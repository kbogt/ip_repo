# IP Core ComBlock

## Files structure

* **README.md:** this file
* **ip_repo:** own made components Vivado's repository
* **vivado:** Tcl files to recreate Vivado's projects (`BOAR_COMPONENT_VIVADOVERSION.tcl`)
* **sdk:** C files to use with the Vivado's projects (`COMPONENT.c`)

## Instructions

* Create a project for the desired board
* Add ip_repo path to the project
* Run in the *Tcl console* the command `source REL_OR_ABS_PATH/BOAD_COMPONENT_VIVADOVERSION.tcl`
* Create a wrapper for the design
* Generate Bitstream
* Export Hardware (include the Bitstream)
* Launch the SDK
* Create a new baremetal app
* Add the needed `sdk/COMPONENT.c` file
* Program the FPGA, connect the UART and run the app
* Enjoy :-)

## Others instructions

* You can create the Vivado's projects inside a directory called *ignore* (included in *.gitignore* file)
* **Do not** add a Vivado's project directory under the **Control Version System!!! (GIT)**
* When a block design is changed, run *File -> Export -> Export Block Design*
* When a C file is changed, overwrite the content in the **sdk** directory

## ComBlock

### Default Generics

Report Cell Usage: 
+------+-----------+------+
|      |Cell       |Count |
+------+-----------+------+
|1     |CARRY4     |    37|
|2     |LUT1       |     9|
|3     |LUT2       |   154|
|4     |LUT3       |    85|
|5     |LUT4       |    81|
|6     |LUT5       |    64|
|7     |LUT6       |   133|
|8     |MUXF7      |    32|
|9     |RAMB18E1   |     1|
|10    |RAMB36E1   |    32|
|11    |RAMB36E1_1 |    32|
|12    |FDRE       |   531|
|13    |FDSE       |     5|
|14    |LDC        |    16|
+------+-----------+------+

Report Instance Areas: 
+------+-------------------------------+----------------------+------+
|      |Instance                       |Module                |Cells |
+------+-------------------------------+----------------------+------+
|1     |top                            |                      |  1212|
|2     |  U0                           |comblock_v1_0         |  1212|
|3     |    comblock_i                 |comblock              |   267|
|4     |      \dram_g.truedualram_i    |TrueDualPortRAM       |    64|
|5     |      \fifo_ftp_g.fifo1_i      |FIFO                  |   203|
|6     |        \g_async.i_sync_rd2wr  |Gray_Sync             |    58|
|7     |          i_sync               |FFchain_1             |    43|
|8     |        \g_async.i_sync_wr2rd  |Gray_Sync_0           |    54|
|9     |          i_sync               |FFchain               |    33|
|10    |        i_memory               |SimpleDualPortRAM     |     6|
|11    |    comblock_v1_0_S00_AXI_inst |comblock_v1_0_S00_AXI |   513|
|12    |    comblock_v1_0_S01_AXI_inst |comblock_v1_0_S01_AXI |   348|
|13    |    comblock_v1_0_S02_AXI_inst |comblock_v1_0_S02_AXI |    84|
+------+-------------------------------+----------------------+------+

### Changed Generics

Report Cell Usage: 
+------+---------+------+
|      |Cell     |Count |
+------+---------+------+
|1     |CARRY4   |    11|
|2     |LUT1     |     8|
|3     |LUT2     |    53|
|4     |LUT3     |    28|
|5     |LUT4     |    70|
|6     |LUT5     |    40|
|7     |LUT6     |    87|
|8     |MUXF7    |    16|
|9     |RAM32M   |     2|
|10    |RAMB18E1 |     1|
|11    |FDRE     |   305|
|12    |FDSE     |     5|
|13    |LDC      |     8|
+------+---------+------+

Report Instance Areas: 
+------+-------------------------------+----------------------+------+
|      |Instance                       |Module                |Cells |
+------+-------------------------------+----------------------+------+
|1     |top                            |                      |   634|
|2     |  U0                           |comblock_v1_0         |   634|
|3     |    comblock_i                 |comblock              |    76|
|4     |      \dram_g.truedualram_i    |TrueDualPortRAM       |     1|
|5     |      \fifo_ftp_g.fifo1_i      |FIFO                  |    75|
|6     |        \g_async.i_sync_rd2wr  |Gray_Sync             |    16|
|7     |          i_sync               |FFchain_1             |    16|
|8     |        \g_async.i_sync_wr2rd  |Gray_Sync_0           |    22|
|9     |          i_sync               |FFchain               |    22|
|10    |        i_memory               |SimpleDualPortRAM     |    15|
|11    |    comblock_v1_0_S00_AXI_inst |comblock_v1_0_S00_AXI |   273|
|12    |    comblock_v1_0_S01_AXI_inst |comblock_v1_0_S01_AXI |   206|
|13    |    comblock_v1_0_S02_AXI_inst |comblock_v1_0_S02_AXI |    79|
+------+-------------------------------+----------------------+------+

## Notes

* Bursts are created by changing the MMU/translation table settings for the PL address range by marking the region as a normal memory type, usually to enable the cache: `Xil_SetTlbAttributes(X_Y_Z_BASEADDR, 0x15de6);`

## Notes about Cortex-A9

* Cortex-A9 processors can only issue INCR (BURST = 01) incrementing bursts. In the case of writes from the ACP, the burst type can also be FIXED (BURST = 00) or WRAP (BURST = 10) and these values can be forwarded onto the AXI Master0 port.
* AXI 3.
