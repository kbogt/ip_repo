#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xil_io.h"

int main()
{
    int val[100];
    int i, avg;
    init_platform();

    xil_printf("Enabling Counter\n");
    Xil_Out32(XPAR_AXIPERF_0_S_AXIL_BASEADDR,0);
    Xil_Out32(XPAR_AXIPERF_0_S_AXIL_BASEADDR,1);
    //
    avg = 0;
    for (i=0; i<100 ; i++){
        val[i] = Xil_In32(XPAR_AXIPERF_0_S_AXIL_BASEADDR);
    }
    //xil_printf("AXI LITE READ -> val1 = %d\n",val[0]);
    for (i=1; i<100 ; i++){
        //xil_printf("AXI LITE READ -> val%d = %d (diff = %d)\n",i,val[i],val[i]-val[i-1]);
        avg += val[i]-val[i-1];
    }
    xil_printf("AVG = %d\n",avg/99);
    //
    avg = 0;
    for (i=0; i<100 ; i++){
        val[i] = Xil_In32(XPAR_AXIPERF_0_S_AXIF_BASEADDR);
    }
    //xil_printf("AXI FULL READ -> val1 = %d\n",val[0]);
    for (i=1; i<100 ; i++){
        //xil_printf("AXI FULL READ -> val%d = %d (diff = %d)\n",i,val[i],val[i]-val[i-1]);
        avg += val[i]-val[i-1];
    }
    xil_printf("AVG = %d\n",avg/99);

    cleanup_platform();
    return 0;
}
