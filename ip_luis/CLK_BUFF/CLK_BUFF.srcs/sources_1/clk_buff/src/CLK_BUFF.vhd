----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/14/2018 06:07:27 PM
-- Design Name: 
-- Module Name: CLK_BUFF - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity CLK_BUFF is
    Port ( S : in STD_LOGIC;
           I0: in STD_LOGIC;
           I1 : in STD_LOGIC;
           O : out STD_LOGIC);
end CLK_BUFF;

architecture Behavioral of CLK_BUFF is

begin
BUFGMUX_inst : BUFGMUX
port map (
O => O, -- 1-bit output: Clock output
I0 => I0, -- 1-bit input: Clock input (S=0)
I1 => I1, -- 1-bit input: Clock input (S=1)
S => S -- 1-bit input: Clock select
);

end Behavioral;
