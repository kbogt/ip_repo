#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xil_io.h"
#include "xtime_l.h"
#include "xil_cache.h"
#include "xil_mmu.h"

#define NUM 256
#define ENABLE_BURST

int main() {
    int i, run;
    u32 rval[NUM], wval[NUM];
    XTime tStart, tEnd;

    init_platform();

    print("Start of test\n");

#ifdef ENABLE_BURST
    // Set the associated memory cacheable
    Xil_SetTlbAttributes(XPAR_BRAM_0_BASEADDR, 0x15de6);
    // Do a write operation so that the memory is cached
//    *(int *)(XPAR_BRAM_0_BASEADDR) = 0x12345678;
    // Flush the cacheline
//    Xil_DCacheFlushRange(XPAR_BRAM_0_BASEADDR, NUM*4);
#endif

    for (run=1; run <= 10; run++) {
        printf("\nRun %d\n",run);
        //
        for (i=0;i<NUM;i++) {
            wval[i] = i*run;
        }
        XTime_GetTime(&tStart);
        for (i=0;i<NUM;i++) {
            Xil_Out32(XPAR_BRAM_0_BASEADDR+i*4,wval[i]);
        }
        XTime_GetTime(&tEnd);
        printf("* Out Simple = %llu\n",tEnd - tStart);
        XTime_GetTime(&tStart);
        for (i=0;i<NUM;i++) {
            rval[i] = Xil_In32(XPAR_BRAM_0_BASEADDR+i*4);
        }
        XTime_GetTime(&tEnd);
        printf("* In Simple == %llu\n",tEnd - tStart);
        for (i=0;i<NUM;i++) {
            if (rval[i]!=wval[i]) print("ERROR\n");
        }
        //
        for (i=0;i<NUM;i++) {
            wval[i] = i*run*2;
        }
        XTime_GetTime(&tStart);
        memcpy((void *)XPAR_BRAM_0_BASEADDR,wval,NUM*4);
        XTime_GetTime(&tEnd);
        printf("* Out Burst == %llu\n",tEnd - tStart);
        XTime_GetTime(&tStart);
        memcpy(rval,(void *)XPAR_BRAM_0_BASEADDR,NUM*4);
        XTime_GetTime(&tEnd);
        printf("* In Burst === %llu\n",tEnd - tStart);
        for (i=0;i<NUM;i++) {
            if (rval[i]!=wval[i]) print("ERROR\n");
        }
    }
    cleanup_platform();
    return 0;
}
