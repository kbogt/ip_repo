# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "addr_bits" -parent ${Page_0}
  ipgui::add_param $IPINST -name "data_bits" -parent ${Page_0}
  ipgui::add_param $IPINST -name "latency_count" -parent ${Page_0}
  ipgui::add_param $IPINST -name "web_bits" -parent ${Page_0}


}

proc update_PARAM_VALUE.addr_bits { PARAM_VALUE.addr_bits } {
	# Procedure called to update addr_bits when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.addr_bits { PARAM_VALUE.addr_bits } {
	# Procedure called to validate addr_bits
	return true
}

proc update_PARAM_VALUE.data_bits { PARAM_VALUE.data_bits } {
	# Procedure called to update data_bits when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.data_bits { PARAM_VALUE.data_bits } {
	# Procedure called to validate data_bits
	return true
}

proc update_PARAM_VALUE.latency_count { PARAM_VALUE.latency_count } {
	# Procedure called to update latency_count when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.latency_count { PARAM_VALUE.latency_count } {
	# Procedure called to validate latency_count
	return true
}

proc update_PARAM_VALUE.web_bits { PARAM_VALUE.web_bits } {
	# Procedure called to update web_bits when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.web_bits { PARAM_VALUE.web_bits } {
	# Procedure called to validate web_bits
	return true
}


proc update_MODELPARAM_VALUE.latency_count { MODELPARAM_VALUE.latency_count PARAM_VALUE.latency_count } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.latency_count}] ${MODELPARAM_VALUE.latency_count}
}

proc update_MODELPARAM_VALUE.web_bits { MODELPARAM_VALUE.web_bits PARAM_VALUE.web_bits } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.web_bits}] ${MODELPARAM_VALUE.web_bits}
}

proc update_MODELPARAM_VALUE.addr_bits { MODELPARAM_VALUE.addr_bits PARAM_VALUE.addr_bits } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.addr_bits}] ${MODELPARAM_VALUE.addr_bits}
}

proc update_MODELPARAM_VALUE.data_bits { MODELPARAM_VALUE.data_bits PARAM_VALUE.data_bits } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.data_bits}] ${MODELPARAM_VALUE.data_bits}
}

