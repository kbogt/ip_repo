--Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
--Date        : Wed Feb  7 11:14:06 2018
--Host        : HP6-MLAB-9 running 64-bit Service Pack 1  (build 7601)
--Command     : generate_target design_1.bd
--Design      : design_1
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1 is
  port (
    Count : out STD_LOGIC_VECTOR ( 15 downto 0 );
    InAddr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    OutAddr : out STD_LOGIC_VECTOR ( 12 downto 0 );
    clk : in STD_LOGIC;
    en : in STD_LOGIC;
    rdy_0 : out STD_LOGIC;
    rsta : in STD_LOGIC;
    rsta_busy_0 : out STD_LOGIC;
    rstb_busy_0 : out STD_LOGIC
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of design_1 : entity is "design_1,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=2,numReposBlks=2,numNonXlnxBlks=1,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,da_axi4_cnt=1,da_ps7_cnt=1,synth_mode=Global}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of design_1 : entity is "design_1.hwdef";
end design_1;

architecture STRUCTURE of design_1 is
  component design_1_blk_mem_gen_0_1 is
  port (
    clka : in STD_LOGIC;
    rsta : in STD_LOGIC;
    ena : in STD_LOGIC;
    wea : in STD_LOGIC_VECTOR ( 0 to 0 );
    addra : in STD_LOGIC_VECTOR ( 12 downto 0 );
    dina : in STD_LOGIC_VECTOR ( 15 downto 0 );
    douta : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clkb : in STD_LOGIC;
    rstb : in STD_LOGIC;
    enb : in STD_LOGIC;
    web : in STD_LOGIC_VECTOR ( 0 to 0 );
    addrb : in STD_LOGIC_VECTOR ( 12 downto 0 );
    dinb : in STD_LOGIC_VECTOR ( 15 downto 0 );
    doutb : out STD_LOGIC_VECTOR ( 15 downto 0 );
    rsta_busy : out STD_LOGIC;
    rstb_busy : out STD_LOGIC
  );
  end component design_1_blk_mem_gen_0_1;
  component design_1_wen_mgr_0_1 is
  port (
    clk : in STD_LOGIC;
    rst : in STD_LOGIC;
    en : in STD_LOGIC;
    in_addr : in STD_LOGIC_VECTOR ( 12 downto 0 );
    adda : out STD_LOGIC_VECTOR ( 12 downto 0 );
    addb : out STD_LOGIC_VECTOR ( 12 downto 0 );
    douta : in STD_LOGIC_VECTOR ( 15 downto 0 );
    doutb : in STD_LOGIC_VECTOR ( 15 downto 0 );
    dina : out STD_LOGIC_VECTOR ( 15 downto 0 );
    dinb : out STD_LOGIC_VECTOR ( 15 downto 0 );
    out_addr : out STD_LOGIC_VECTOR ( 12 downto 0 );
    count : out STD_LOGIC_VECTOR ( 15 downto 0 );
    rdy : out STD_LOGIC;
    ena : out STD_LOGIC;
    enb : out STD_LOGIC;
    wea : out STD_LOGIC;
    web : out STD_LOGIC
  );
  end component design_1_wen_mgr_0_1;
  signal InAddr_1 : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal blk_mem_gen_0_douta : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal blk_mem_gen_0_doutb : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal blk_mem_gen_0_rsta_busy : STD_LOGIC;
  signal blk_mem_gen_0_rstb_busy : STD_LOGIC;
  signal clk_1 : STD_LOGIC;
  signal en_1 : STD_LOGIC;
  signal rsta_1 : STD_LOGIC;
  signal wen_mgr_0_adda : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal wen_mgr_0_addb : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal wen_mgr_0_addr : STD_LOGIC_VECTOR ( 12 downto 0 );
  signal wen_mgr_0_count : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal wen_mgr_0_dina : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal wen_mgr_0_dinb : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal wen_mgr_0_ena : STD_LOGIC;
  signal wen_mgr_0_enb : STD_LOGIC;
  signal wen_mgr_0_rdy : STD_LOGIC;
  signal wen_mgr_0_wea : STD_LOGIC;
  signal wen_mgr_0_web : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clk : signal is "xilinx.com:signal:clock:1.0 CLK.CLK CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clk : signal is "XIL_INTERFACENAME CLK.CLK, ASSOCIATED_RESET rsta, CLK_DOMAIN design_1_CLK_0, FREQ_HZ 100000000, PHASE 0.000";
  attribute X_INTERFACE_INFO of rsta : signal is "xilinx.com:signal:reset:1.0 RST.RSTA RST";
  attribute X_INTERFACE_PARAMETER of rsta : signal is "XIL_INTERFACENAME RST.RSTA, POLARITY ACTIVE_LOW";
  attribute X_INTERFACE_INFO of Count : signal is "xilinx.com:signal:data:1.0 DATA.COUNT DATA";
  attribute X_INTERFACE_PARAMETER of Count : signal is "XIL_INTERFACENAME DATA.COUNT, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type generated dependency signed format bool minimum {} maximum {}} value FALSE}}}} DATA_WIDTH 16}";
  attribute X_INTERFACE_INFO of OutAddr : signal is "xilinx.com:signal:data:1.0 DATA.OUTADDR DATA";
  attribute X_INTERFACE_PARAMETER of OutAddr : signal is "XIL_INTERFACENAME DATA.OUTADDR, LAYERED_METADATA xilinx.com:interface:datatypes:1.0 {DATA {datatype {name {attribs {resolve_type immediate dependency {} format string minimum {} maximum {}} value data} bitwidth {attribs {resolve_type generated dependency bitwidth format long minimum {} maximum {}} value 16} bitoffset {attribs {resolve_type immediate dependency {} format long minimum {} maximum {}} value 0} integer {signed {attribs {resolve_type generated dependency signed format bool minimum {} maximum {}} value FALSE}}}} DATA_WIDTH 16}";
begin
  Count(15 downto 0) <= wen_mgr_0_count(15 downto 0);
  InAddr_1(12 downto 0) <= InAddr(12 downto 0);
  OutAddr(12 downto 0) <= wen_mgr_0_addr(12 downto 0);
  clk_1 <= clk;
  en_1 <= en;
  rdy_0 <= wen_mgr_0_rdy;
  rsta_1 <= rsta;
  rsta_busy_0 <= blk_mem_gen_0_rsta_busy;
  rstb_busy_0 <= blk_mem_gen_0_rstb_busy;
blk_mem_gen_0: component design_1_blk_mem_gen_0_1
     port map (
      addra(12 downto 0) => wen_mgr_0_adda(12 downto 0),
      addrb(12 downto 0) => wen_mgr_0_addb(12 downto 0),
      clka => clk_1,
      clkb => clk_1,
      dina(15 downto 0) => wen_mgr_0_dina(15 downto 0),
      dinb(15 downto 0) => wen_mgr_0_dinb(15 downto 0),
      douta(15 downto 0) => blk_mem_gen_0_douta(15 downto 0),
      doutb(15 downto 0) => blk_mem_gen_0_doutb(15 downto 0),
      ena => wen_mgr_0_ena,
      enb => wen_mgr_0_enb,
      rsta => rsta_1,
      rsta_busy => blk_mem_gen_0_rsta_busy,
      rstb => rsta_1,
      rstb_busy => blk_mem_gen_0_rstb_busy,
      wea(0) => wen_mgr_0_wea,
      web(0) => wen_mgr_0_web
    );
wen_mgr_0: component design_1_wen_mgr_0_1
     port map (
      adda(12 downto 0) => wen_mgr_0_adda(12 downto 0),
      addb(12 downto 0) => wen_mgr_0_addb(12 downto 0),
      clk => clk_1,
      count(15 downto 0) => wen_mgr_0_count(15 downto 0),
      dina(15 downto 0) => wen_mgr_0_dina(15 downto 0),
      dinb(15 downto 0) => wen_mgr_0_dinb(15 downto 0),
      douta(15 downto 0) => blk_mem_gen_0_douta(15 downto 0),
      doutb(15 downto 0) => blk_mem_gen_0_doutb(15 downto 0),
      en => en_1,
      ena => wen_mgr_0_ena,
      enb => wen_mgr_0_enb,
      in_addr(12 downto 0) => InAddr_1(12 downto 0),
      out_addr(12 downto 0) => wen_mgr_0_addr(12 downto 0),
      rdy => wen_mgr_0_rdy,
      rst => rsta_1,
      wea => wen_mgr_0_wea,
      web => wen_mgr_0_web
    );
end STRUCTURE;
