# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "count_bits" -parent ${Page_0}
  ipgui::add_param $IPINST -name "fifo_size" -parent ${Page_0}
  ipgui::add_param $IPINST -name "we_bytes" -parent ${Page_0}


}

proc update_PARAM_VALUE.count_bits { PARAM_VALUE.count_bits } {
	# Procedure called to update count_bits when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.count_bits { PARAM_VALUE.count_bits } {
	# Procedure called to validate count_bits
	return true
}

proc update_PARAM_VALUE.fifo_size { PARAM_VALUE.fifo_size } {
	# Procedure called to update fifo_size when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.fifo_size { PARAM_VALUE.fifo_size } {
	# Procedure called to validate fifo_size
	return true
}

proc update_PARAM_VALUE.we_bytes { PARAM_VALUE.we_bytes } {
	# Procedure called to update we_bytes when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.we_bytes { PARAM_VALUE.we_bytes } {
	# Procedure called to validate we_bytes
	return true
}


proc update_MODELPARAM_VALUE.count_bits { MODELPARAM_VALUE.count_bits PARAM_VALUE.count_bits } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.count_bits}] ${MODELPARAM_VALUE.count_bits}
}

proc update_MODELPARAM_VALUE.fifo_size { MODELPARAM_VALUE.fifo_size PARAM_VALUE.fifo_size } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.fifo_size}] ${MODELPARAM_VALUE.fifo_size}
}

proc update_MODELPARAM_VALUE.we_bytes { MODELPARAM_VALUE.we_bytes PARAM_VALUE.we_bytes } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.we_bytes}] ${MODELPARAM_VALUE.we_bytes}
}

