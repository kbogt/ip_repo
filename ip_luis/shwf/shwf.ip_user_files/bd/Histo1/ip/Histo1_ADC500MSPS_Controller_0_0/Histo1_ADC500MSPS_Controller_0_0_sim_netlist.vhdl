-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
-- Date        : Wed Feb  7 13:10:30 2018
-- Host        : HP6-MLAB-9 running 64-bit Service Pack 1  (build 7601)
-- Command     : write_vhdl -force -mode funcsim
--               c:/Users/infolab/Desktop/my_verilog/histo_w_comblock/project_1/project_1.srcs/sources_1/bd/Histo1/ip/Histo1_ADC500MSPS_Controller_0_0/Histo1_ADC500MSPS_Controller_0_0_sim_netlist.vhdl
-- Design      : Histo1_ADC500MSPS_Controller_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC500MSPS_Controller_0_0_ADC500MHz_Controller_v1_0 is
  port (
    data_valid : out STD_LOGIC;
    ADC_Dout : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ADC_clk : in STD_LOGIC;
    ADC_Din : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ADC_reset : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC500MSPS_Controller_0_0_ADC500MHz_Controller_v1_0 : entity is "ADC500MHz_Controller_v1_0";
end Histo1_ADC500MSPS_Controller_0_0_ADC500MHz_Controller_v1_0;

architecture STRUCTURE of Histo1_ADC500MSPS_Controller_0_0_ADC500MHz_Controller_v1_0 is
  signal data_int_16b_1 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal data_int_16b_2 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^data_valid\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal p_1_in : STD_LOGIC;
begin
  data_valid <= \^data_valid\;
\data_int_16b_1_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => ADC_Din(0),
      Q => data_int_16b_1(0),
      R => p_0_in
    );
\data_int_16b_1_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => ADC_Din(10),
      Q => data_int_16b_1(10),
      R => p_0_in
    );
\data_int_16b_1_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => ADC_Din(11),
      Q => data_int_16b_1(11),
      R => p_0_in
    );
\data_int_16b_1_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => ADC_Din(12),
      Q => data_int_16b_1(12),
      R => p_0_in
    );
\data_int_16b_1_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => ADC_Din(13),
      Q => data_int_16b_1(13),
      R => p_0_in
    );
\data_int_16b_1_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => ADC_Din(14),
      Q => data_int_16b_1(14),
      R => p_0_in
    );
\data_int_16b_1_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => ADC_Din(15),
      Q => data_int_16b_1(15),
      R => p_0_in
    );
\data_int_16b_1_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => ADC_Din(1),
      Q => data_int_16b_1(1),
      R => p_0_in
    );
\data_int_16b_1_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => ADC_Din(2),
      Q => data_int_16b_1(2),
      R => p_0_in
    );
\data_int_16b_1_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => ADC_Din(3),
      Q => data_int_16b_1(3),
      R => p_0_in
    );
\data_int_16b_1_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => ADC_Din(4),
      Q => data_int_16b_1(4),
      R => p_0_in
    );
\data_int_16b_1_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => ADC_Din(5),
      Q => data_int_16b_1(5),
      R => p_0_in
    );
\data_int_16b_1_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => ADC_Din(6),
      Q => data_int_16b_1(6),
      R => p_0_in
    );
\data_int_16b_1_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => ADC_Din(7),
      Q => data_int_16b_1(7),
      R => p_0_in
    );
\data_int_16b_1_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => ADC_Din(8),
      Q => data_int_16b_1(8),
      R => p_0_in
    );
\data_int_16b_1_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => ADC_Din(9),
      Q => data_int_16b_1(9),
      R => p_0_in
    );
\data_int_16b_2_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => data_int_16b_1(0),
      Q => data_int_16b_2(0),
      R => p_0_in
    );
\data_int_16b_2_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => data_int_16b_1(10),
      Q => data_int_16b_2(10),
      R => p_0_in
    );
\data_int_16b_2_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => data_int_16b_1(11),
      Q => data_int_16b_2(11),
      R => p_0_in
    );
\data_int_16b_2_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => data_int_16b_1(12),
      Q => data_int_16b_2(12),
      R => p_0_in
    );
\data_int_16b_2_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => data_int_16b_1(13),
      Q => data_int_16b_2(13),
      R => p_0_in
    );
\data_int_16b_2_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => data_int_16b_1(14),
      Q => data_int_16b_2(14),
      R => p_0_in
    );
\data_int_16b_2_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => data_int_16b_1(15),
      Q => data_int_16b_2(15),
      R => p_0_in
    );
\data_int_16b_2_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => data_int_16b_1(1),
      Q => data_int_16b_2(1),
      R => p_0_in
    );
\data_int_16b_2_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => data_int_16b_1(2),
      Q => data_int_16b_2(2),
      R => p_0_in
    );
\data_int_16b_2_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => data_int_16b_1(3),
      Q => data_int_16b_2(3),
      R => p_0_in
    );
\data_int_16b_2_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => data_int_16b_1(4),
      Q => data_int_16b_2(4),
      R => p_0_in
    );
\data_int_16b_2_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => data_int_16b_1(5),
      Q => data_int_16b_2(5),
      R => p_0_in
    );
\data_int_16b_2_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => data_int_16b_1(6),
      Q => data_int_16b_2(6),
      R => p_0_in
    );
\data_int_16b_2_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => data_int_16b_1(7),
      Q => data_int_16b_2(7),
      R => p_0_in
    );
\data_int_16b_2_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => data_int_16b_1(8),
      Q => data_int_16b_2(8),
      R => p_0_in
    );
\data_int_16b_2_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => '1',
      D => data_int_16b_1(9),
      Q => data_int_16b_2(9),
      R => p_0_in
    );
\data_int_32b[31]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => ADC_reset,
      O => p_0_in
    );
\data_int_32b_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_1(0),
      Q => ADC_Dout(0),
      R => p_0_in
    );
\data_int_32b_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_1(10),
      Q => ADC_Dout(10),
      R => p_0_in
    );
\data_int_32b_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_1(11),
      Q => ADC_Dout(11),
      R => p_0_in
    );
\data_int_32b_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_1(12),
      Q => ADC_Dout(12),
      R => p_0_in
    );
\data_int_32b_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_1(13),
      Q => ADC_Dout(13),
      R => p_0_in
    );
\data_int_32b_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_1(14),
      Q => ADC_Dout(14),
      R => p_0_in
    );
\data_int_32b_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_1(15),
      Q => ADC_Dout(15),
      R => p_0_in
    );
\data_int_32b_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_2(0),
      Q => ADC_Dout(16),
      R => p_0_in
    );
\data_int_32b_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_2(1),
      Q => ADC_Dout(17),
      R => p_0_in
    );
\data_int_32b_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_2(2),
      Q => ADC_Dout(18),
      R => p_0_in
    );
\data_int_32b_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_2(3),
      Q => ADC_Dout(19),
      R => p_0_in
    );
\data_int_32b_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_1(1),
      Q => ADC_Dout(1),
      R => p_0_in
    );
\data_int_32b_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_2(4),
      Q => ADC_Dout(20),
      R => p_0_in
    );
\data_int_32b_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_2(5),
      Q => ADC_Dout(21),
      R => p_0_in
    );
\data_int_32b_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_2(6),
      Q => ADC_Dout(22),
      R => p_0_in
    );
\data_int_32b_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_2(7),
      Q => ADC_Dout(23),
      R => p_0_in
    );
\data_int_32b_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_2(8),
      Q => ADC_Dout(24),
      R => p_0_in
    );
\data_int_32b_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_2(9),
      Q => ADC_Dout(25),
      R => p_0_in
    );
\data_int_32b_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_2(10),
      Q => ADC_Dout(26),
      R => p_0_in
    );
\data_int_32b_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_2(11),
      Q => ADC_Dout(27),
      R => p_0_in
    );
\data_int_32b_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_2(12),
      Q => ADC_Dout(28),
      R => p_0_in
    );
\data_int_32b_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_2(13),
      Q => ADC_Dout(29),
      R => p_0_in
    );
\data_int_32b_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_1(2),
      Q => ADC_Dout(2),
      R => p_0_in
    );
\data_int_32b_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_2(14),
      Q => ADC_Dout(30),
      R => p_0_in
    );
\data_int_32b_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_2(15),
      Q => ADC_Dout(31),
      R => p_0_in
    );
\data_int_32b_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_1(3),
      Q => ADC_Dout(3),
      R => p_0_in
    );
\data_int_32b_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_1(4),
      Q => ADC_Dout(4),
      R => p_0_in
    );
\data_int_32b_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_1(5),
      Q => ADC_Dout(5),
      R => p_0_in
    );
\data_int_32b_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_1(6),
      Q => ADC_Dout(6),
      R => p_0_in
    );
\data_int_32b_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_1(7),
      Q => ADC_Dout(7),
      R => p_0_in
    );
\data_int_32b_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_1(8),
      Q => ADC_Dout(8),
      R => p_0_in
    );
\data_int_32b_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => ADC_clk,
      CE => \^data_valid\,
      D => data_int_16b_1(9),
      Q => ADC_Dout(9),
      R => p_0_in
    );
data_to_fifo_clock_enable_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^data_valid\,
      O => p_1_in
    );
data_to_fifo_clock_enable_reg: unisim.vcomponents.FDSE
     port map (
      C => ADC_clk,
      CE => '1',
      D => p_1_in,
      Q => \^data_valid\,
      S => p_0_in
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC500MSPS_Controller_0_0_cdc_sync is
  port (
    lpf_asr_reg : out STD_LOGIC;
    scndry_out : out STD_LOGIC;
    lpf_asr : in STD_LOGIC;
    asr_lpf : in STD_LOGIC_VECTOR ( 0 to 0 );
    p_1_in : in STD_LOGIC;
    p_2_in : in STD_LOGIC;
    aux_reset_in : in STD_LOGIC;
    slowest_sync_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC500MSPS_Controller_0_0_cdc_sync : entity is "cdc_sync";
end Histo1_ADC500MSPS_Controller_0_0_cdc_sync;

architecture STRUCTURE of Histo1_ADC500MSPS_Controller_0_0_cdc_sync is
  signal asr_d1 : STD_LOGIC;
  signal s_level_out_d1_cdc_to : STD_LOGIC;
  signal s_level_out_d2 : STD_LOGIC;
  signal s_level_out_d3 : STD_LOGIC;
  signal \^scndry_out\ : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to\ : label is std.standard.true;
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to\ : label is "FDR";
  attribute box_type : string;
  attribute box_type of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to\ : label is "PRIMITIVE";
  attribute ASYNC_REG of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d2\ : label is std.standard.true;
  attribute XILINX_LEGACY_PRIM of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d2\ : label is "FDR";
  attribute box_type of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d2\ : label is "PRIMITIVE";
  attribute ASYNC_REG of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d3\ : label is std.standard.true;
  attribute XILINX_LEGACY_PRIM of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d3\ : label is "FDR";
  attribute box_type of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d3\ : label is "PRIMITIVE";
  attribute ASYNC_REG of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d4\ : label is std.standard.true;
  attribute XILINX_LEGACY_PRIM of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d4\ : label is "FDR";
  attribute box_type of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d4\ : label is "PRIMITIVE";
begin
  scndry_out <= \^scndry_out\;
\GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => asr_d1,
      Q => s_level_out_d1_cdc_to,
      R => '0'
    );
\GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => aux_reset_in,
      O => asr_d1
    );
\GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => s_level_out_d1_cdc_to,
      Q => s_level_out_d2,
      R => '0'
    );
\GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d3\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => s_level_out_d2,
      Q => s_level_out_d3,
      R => '0'
    );
\GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d4\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => s_level_out_d3,
      Q => \^scndry_out\,
      R => '0'
    );
lpf_asr_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EAAAAAA8"
    )
        port map (
      I0 => lpf_asr,
      I1 => asr_lpf(0),
      I2 => \^scndry_out\,
      I3 => p_1_in,
      I4 => p_2_in,
      O => lpf_asr_reg
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC500MSPS_Controller_0_0_cdc_sync_0 is
  port (
    lpf_exr_reg : out STD_LOGIC;
    scndry_out : out STD_LOGIC;
    lpf_exr : in STD_LOGIC;
    p_3_out : in STD_LOGIC_VECTOR ( 2 downto 0 );
    mb_debug_sys_rst : in STD_LOGIC;
    ext_reset_in : in STD_LOGIC;
    slowest_sync_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC500MSPS_Controller_0_0_cdc_sync_0 : entity is "cdc_sync";
end Histo1_ADC500MSPS_Controller_0_0_cdc_sync_0;

architecture STRUCTURE of Histo1_ADC500MSPS_Controller_0_0_cdc_sync_0 is
  signal \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to_i_1__0_n_0\ : STD_LOGIC;
  signal s_level_out_d1_cdc_to : STD_LOGIC;
  signal s_level_out_d2 : STD_LOGIC;
  signal s_level_out_d3 : STD_LOGIC;
  signal \^scndry_out\ : STD_LOGIC;
  attribute ASYNC_REG : boolean;
  attribute ASYNC_REG of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to\ : label is std.standard.true;
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to\ : label is "FDR";
  attribute box_type : string;
  attribute box_type of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to\ : label is "PRIMITIVE";
  attribute ASYNC_REG of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d2\ : label is std.standard.true;
  attribute XILINX_LEGACY_PRIM of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d2\ : label is "FDR";
  attribute box_type of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d2\ : label is "PRIMITIVE";
  attribute ASYNC_REG of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d3\ : label is std.standard.true;
  attribute XILINX_LEGACY_PRIM of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d3\ : label is "FDR";
  attribute box_type of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d3\ : label is "PRIMITIVE";
  attribute ASYNC_REG of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d4\ : label is std.standard.true;
  attribute XILINX_LEGACY_PRIM of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d4\ : label is "FDR";
  attribute box_type of \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d4\ : label is "PRIMITIVE";
begin
  scndry_out <= \^scndry_out\;
\GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to_i_1__0_n_0\,
      Q => s_level_out_d1_cdc_to,
      R => '0'
    );
\GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to_i_1__0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => mb_debug_sys_rst,
      I1 => ext_reset_in,
      O => \GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_IN_cdc_to_i_1__0_n_0\
    );
\GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d2\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => s_level_out_d1_cdc_to,
      Q => s_level_out_d2,
      R => '0'
    );
\GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d3\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => s_level_out_d2,
      Q => s_level_out_d3,
      R => '0'
    );
\GENERATE_LEVEL_P_S_CDC.SINGLE_BIT.CROSS_PLEVEL_IN2SCNDRY_s_level_out_d4\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => s_level_out_d3,
      Q => \^scndry_out\,
      R => '0'
    );
lpf_exr_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"EAAAAAA8"
    )
        port map (
      I0 => lpf_exr,
      I1 => p_3_out(0),
      I2 => \^scndry_out\,
      I3 => p_3_out(1),
      I4 => p_3_out(2),
      O => lpf_exr_reg
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC500MSPS_Controller_0_0_design_1_clk_wiz_0_0_clk_wiz is
  port (
    clk_out1 : out STD_LOGIC;
    resetn : in STD_LOGIC;
    clk_in1 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC500MSPS_Controller_0_0_design_1_clk_wiz_0_0_clk_wiz : entity is "design_1_clk_wiz_0_0_clk_wiz";
end Histo1_ADC500MSPS_Controller_0_0_design_1_clk_wiz_0_0_clk_wiz;

architecture STRUCTURE of Histo1_ADC500MSPS_Controller_0_0_design_1_clk_wiz_0_0_clk_wiz is
  signal clk_in1_design_1_clk_wiz_0_0 : STD_LOGIC;
  signal clk_out1_design_1_clk_wiz_0_0 : STD_LOGIC;
  signal clkfbout_buf_design_1_clk_wiz_0_0 : STD_LOGIC;
  signal clkfbout_design_1_clk_wiz_0_0 : STD_LOGIC;
  signal reset_high : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT1_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DRDY_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_LOCKED_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_PSDONE_UNCONNECTED : STD_LOGIC;
  signal NLW_mmcm_adv_inst_DO_UNCONNECTED : STD_LOGIC_VECTOR ( 15 downto 0 );
  attribute box_type : string;
  attribute box_type of clkf_buf : label is "PRIMITIVE";
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of clkin1_ibufg : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of clkin1_ibufg : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of clkin1_ibufg : label is "AUTO";
  attribute box_type of clkin1_ibufg : label is "PRIMITIVE";
  attribute box_type of clkout1_buf : label is "PRIMITIVE";
  attribute box_type of mmcm_adv_inst : label is "PRIMITIVE";
begin
clkf_buf: unisim.vcomponents.BUFG
     port map (
      I => clkfbout_design_1_clk_wiz_0_0,
      O => clkfbout_buf_design_1_clk_wiz_0_0
    );
clkin1_ibufg: unisim.vcomponents.IBUF
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => clk_in1,
      O => clk_in1_design_1_clk_wiz_0_0
    );
clkout1_buf: unisim.vcomponents.BUFG
     port map (
      I => clk_out1_design_1_clk_wiz_0_0,
      O => clk_out1
    );
mmcm_adv_inst: unisim.vcomponents.MMCME2_ADV
    generic map(
      BANDWIDTH => "OPTIMIZED",
      CLKFBOUT_MULT_F => 10.125000,
      CLKFBOUT_PHASE => 0.000000,
      CLKFBOUT_USE_FINE_PS => false,
      CLKIN1_PERIOD => 10.000000,
      CLKIN2_PERIOD => 0.000000,
      CLKOUT0_DIVIDE_F => 3.375000,
      CLKOUT0_DUTY_CYCLE => 0.500000,
      CLKOUT0_PHASE => 0.000000,
      CLKOUT0_USE_FINE_PS => false,
      CLKOUT1_DIVIDE => 1,
      CLKOUT1_DUTY_CYCLE => 0.500000,
      CLKOUT1_PHASE => 0.000000,
      CLKOUT1_USE_FINE_PS => false,
      CLKOUT2_DIVIDE => 1,
      CLKOUT2_DUTY_CYCLE => 0.500000,
      CLKOUT2_PHASE => 0.000000,
      CLKOUT2_USE_FINE_PS => false,
      CLKOUT3_DIVIDE => 1,
      CLKOUT3_DUTY_CYCLE => 0.500000,
      CLKOUT3_PHASE => 0.000000,
      CLKOUT3_USE_FINE_PS => false,
      CLKOUT4_CASCADE => false,
      CLKOUT4_DIVIDE => 1,
      CLKOUT4_DUTY_CYCLE => 0.500000,
      CLKOUT4_PHASE => 0.000000,
      CLKOUT4_USE_FINE_PS => false,
      CLKOUT5_DIVIDE => 1,
      CLKOUT5_DUTY_CYCLE => 0.500000,
      CLKOUT5_PHASE => 0.000000,
      CLKOUT5_USE_FINE_PS => false,
      CLKOUT6_DIVIDE => 1,
      CLKOUT6_DUTY_CYCLE => 0.500000,
      CLKOUT6_PHASE => 0.000000,
      CLKOUT6_USE_FINE_PS => false,
      COMPENSATION => "ZHOLD",
      DIVCLK_DIVIDE => 1,
      IS_CLKINSEL_INVERTED => '0',
      IS_PSEN_INVERTED => '0',
      IS_PSINCDEC_INVERTED => '0',
      IS_PWRDWN_INVERTED => '0',
      IS_RST_INVERTED => '0',
      REF_JITTER1 => 0.010000,
      REF_JITTER2 => 0.010000,
      SS_EN => "FALSE",
      SS_MODE => "CENTER_HIGH",
      SS_MOD_PERIOD => 10000,
      STARTUP_WAIT => false
    )
        port map (
      CLKFBIN => clkfbout_buf_design_1_clk_wiz_0_0,
      CLKFBOUT => clkfbout_design_1_clk_wiz_0_0,
      CLKFBOUTB => NLW_mmcm_adv_inst_CLKFBOUTB_UNCONNECTED,
      CLKFBSTOPPED => NLW_mmcm_adv_inst_CLKFBSTOPPED_UNCONNECTED,
      CLKIN1 => clk_in1_design_1_clk_wiz_0_0,
      CLKIN2 => '0',
      CLKINSEL => '1',
      CLKINSTOPPED => NLW_mmcm_adv_inst_CLKINSTOPPED_UNCONNECTED,
      CLKOUT0 => clk_out1_design_1_clk_wiz_0_0,
      CLKOUT0B => NLW_mmcm_adv_inst_CLKOUT0B_UNCONNECTED,
      CLKOUT1 => NLW_mmcm_adv_inst_CLKOUT1_UNCONNECTED,
      CLKOUT1B => NLW_mmcm_adv_inst_CLKOUT1B_UNCONNECTED,
      CLKOUT2 => NLW_mmcm_adv_inst_CLKOUT2_UNCONNECTED,
      CLKOUT2B => NLW_mmcm_adv_inst_CLKOUT2B_UNCONNECTED,
      CLKOUT3 => NLW_mmcm_adv_inst_CLKOUT3_UNCONNECTED,
      CLKOUT3B => NLW_mmcm_adv_inst_CLKOUT3B_UNCONNECTED,
      CLKOUT4 => NLW_mmcm_adv_inst_CLKOUT4_UNCONNECTED,
      CLKOUT5 => NLW_mmcm_adv_inst_CLKOUT5_UNCONNECTED,
      CLKOUT6 => NLW_mmcm_adv_inst_CLKOUT6_UNCONNECTED,
      DADDR(6 downto 0) => B"0000000",
      DCLK => '0',
      DEN => '0',
      DI(15 downto 0) => B"0000000000000000",
      DO(15 downto 0) => NLW_mmcm_adv_inst_DO_UNCONNECTED(15 downto 0),
      DRDY => NLW_mmcm_adv_inst_DRDY_UNCONNECTED,
      DWE => '0',
      LOCKED => NLW_mmcm_adv_inst_LOCKED_UNCONNECTED,
      PSCLK => '0',
      PSDONE => NLW_mmcm_adv_inst_PSDONE_UNCONNECTED,
      PSEN => '0',
      PSINCDEC => '0',
      PWRDWN => '0',
      RST => reset_high
    );
mmcm_adv_inst_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => resetn,
      O => reset_high
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC500MSPS_Controller_0_0_upcnt_n is
  port (
    Q : out STD_LOGIC_VECTOR ( 5 downto 0 );
    seq_clr : in STD_LOGIC;
    seq_cnt_en : in STD_LOGIC;
    slowest_sync_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC500MSPS_Controller_0_0_upcnt_n : entity is "upcnt_n";
end Histo1_ADC500MSPS_Controller_0_0_upcnt_n;

architecture STRUCTURE of Histo1_ADC500MSPS_Controller_0_0_upcnt_n is
  signal \^q\ : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal clear : STD_LOGIC;
  signal q_int0 : STD_LOGIC_VECTOR ( 5 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \q_int[1]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \q_int[2]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \q_int[3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \q_int[4]_i_1\ : label is "soft_lutpair0";
begin
  Q(5 downto 0) <= \^q\(5 downto 0);
\q_int[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^q\(0),
      O => q_int0(0)
    );
\q_int[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => q_int0(1)
    );
\q_int[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      I2 => \^q\(2),
      O => q_int0(2)
    );
\q_int[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => \^q\(1),
      I1 => \^q\(0),
      I2 => \^q\(2),
      I3 => \^q\(3),
      O => q_int0(3)
    );
\q_int[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(0),
      I2 => \^q\(1),
      I3 => \^q\(3),
      I4 => \^q\(4),
      O => q_int0(4)
    );
\q_int[5]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => seq_clr,
      O => clear
    );
\q_int[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => \^q\(3),
      I1 => \^q\(1),
      I2 => \^q\(0),
      I3 => \^q\(2),
      I4 => \^q\(4),
      I5 => \^q\(5),
      O => q_int0(5)
    );
\q_int_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => slowest_sync_clk,
      CE => seq_cnt_en,
      D => q_int0(0),
      Q => \^q\(0),
      R => clear
    );
\q_int_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => slowest_sync_clk,
      CE => seq_cnt_en,
      D => q_int0(1),
      Q => \^q\(1),
      R => clear
    );
\q_int_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => slowest_sync_clk,
      CE => seq_cnt_en,
      D => q_int0(2),
      Q => \^q\(2),
      R => clear
    );
\q_int_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => slowest_sync_clk,
      CE => seq_cnt_en,
      D => q_int0(3),
      Q => \^q\(3),
      R => clear
    );
\q_int_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => slowest_sync_clk,
      CE => seq_cnt_en,
      D => q_int0(4),
      Q => \^q\(4),
      R => clear
    );
\q_int_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => slowest_sync_clk,
      CE => seq_cnt_en,
      D => q_int0(5),
      Q => \^q\(5),
      R => clear
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC500MSPS_Controller_0_0_util_ds_buf is
  port (
    BUFG_O : out STD_LOGIC_VECTOR ( 0 to 0 );
    BUFG_I : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC500MSPS_Controller_0_0_util_ds_buf : entity is "util_ds_buf";
end Histo1_ADC500MSPS_Controller_0_0_util_ds_buf;

architecture STRUCTURE of Histo1_ADC500MSPS_Controller_0_0_util_ds_buf is
  attribute box_type : string;
  attribute box_type of \USE_BUFG.GEN_BUFG[0].BUFG_U\ : label is "PRIMITIVE";
begin
\USE_BUFG.GEN_BUFG[0].BUFG_U\: unisim.vcomponents.BUFG
     port map (
      I => BUFG_I(0),
      O => BUFG_O(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized1\ is
  port (
    IBUF_OUT : out STD_LOGIC_VECTOR ( 0 to 0 );
    IBUF_DS_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    IBUF_DS_N : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized1\ : entity is "util_ds_buf";
end \Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized1\;

architecture STRUCTURE of \Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized1\ is
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of \USE_IBUFDS.GEN_IBUFDS[0].IBUFDS_I\ : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[0].IBUFDS_I\ : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[0].IBUFDS_I\ : label is "AUTO";
  attribute box_type : string;
  attribute box_type of \USE_IBUFDS.GEN_IBUFDS[0].IBUFDS_I\ : label is "PRIMITIVE";
begin
\USE_IBUFDS.GEN_IBUFDS[0].IBUFDS_I\: unisim.vcomponents.IBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => IBUF_DS_P(0),
      IB => IBUF_DS_N(0),
      O => IBUF_OUT(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized1_1\ is
  port (
    IBUF_OUT : out STD_LOGIC_VECTOR ( 0 to 0 );
    IBUF_DS_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    IBUF_DS_N : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized1_1\ : entity is "util_ds_buf";
end \Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized1_1\;

architecture STRUCTURE of \Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized1_1\ is
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of \USE_IBUFDS.GEN_IBUFDS[0].IBUFDS_I\ : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[0].IBUFDS_I\ : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[0].IBUFDS_I\ : label is "AUTO";
  attribute box_type : string;
  attribute box_type of \USE_IBUFDS.GEN_IBUFDS[0].IBUFDS_I\ : label is "PRIMITIVE";
begin
\USE_IBUFDS.GEN_IBUFDS[0].IBUFDS_I\: unisim.vcomponents.IBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => IBUF_DS_P(0),
      IB => IBUF_DS_N(0),
      O => IBUF_OUT(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized3\ is
  port (
    IBUF_OUT : out STD_LOGIC_VECTOR ( 15 downto 0 );
    IBUF_DS_P : in STD_LOGIC_VECTOR ( 15 downto 0 );
    IBUF_DS_N : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized3\ : entity is "util_ds_buf";
end \Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized3\;

architecture STRUCTURE of \Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized3\ is
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of \USE_IBUFDS.GEN_IBUFDS[0].IBUFDS_I\ : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE : string;
  attribute IBUF_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[0].IBUFDS_I\ : label is "0";
  attribute IFD_DELAY_VALUE : string;
  attribute IFD_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[0].IBUFDS_I\ : label is "AUTO";
  attribute box_type : string;
  attribute box_type of \USE_IBUFDS.GEN_IBUFDS[0].IBUFDS_I\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \USE_IBUFDS.GEN_IBUFDS[10].IBUFDS_I\ : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[10].IBUFDS_I\ : label is "0";
  attribute IFD_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[10].IBUFDS_I\ : label is "AUTO";
  attribute box_type of \USE_IBUFDS.GEN_IBUFDS[10].IBUFDS_I\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \USE_IBUFDS.GEN_IBUFDS[11].IBUFDS_I\ : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[11].IBUFDS_I\ : label is "0";
  attribute IFD_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[11].IBUFDS_I\ : label is "AUTO";
  attribute box_type of \USE_IBUFDS.GEN_IBUFDS[11].IBUFDS_I\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \USE_IBUFDS.GEN_IBUFDS[12].IBUFDS_I\ : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[12].IBUFDS_I\ : label is "0";
  attribute IFD_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[12].IBUFDS_I\ : label is "AUTO";
  attribute box_type of \USE_IBUFDS.GEN_IBUFDS[12].IBUFDS_I\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \USE_IBUFDS.GEN_IBUFDS[13].IBUFDS_I\ : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[13].IBUFDS_I\ : label is "0";
  attribute IFD_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[13].IBUFDS_I\ : label is "AUTO";
  attribute box_type of \USE_IBUFDS.GEN_IBUFDS[13].IBUFDS_I\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \USE_IBUFDS.GEN_IBUFDS[14].IBUFDS_I\ : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[14].IBUFDS_I\ : label is "0";
  attribute IFD_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[14].IBUFDS_I\ : label is "AUTO";
  attribute box_type of \USE_IBUFDS.GEN_IBUFDS[14].IBUFDS_I\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \USE_IBUFDS.GEN_IBUFDS[15].IBUFDS_I\ : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[15].IBUFDS_I\ : label is "0";
  attribute IFD_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[15].IBUFDS_I\ : label is "AUTO";
  attribute box_type of \USE_IBUFDS.GEN_IBUFDS[15].IBUFDS_I\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \USE_IBUFDS.GEN_IBUFDS[1].IBUFDS_I\ : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[1].IBUFDS_I\ : label is "0";
  attribute IFD_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[1].IBUFDS_I\ : label is "AUTO";
  attribute box_type of \USE_IBUFDS.GEN_IBUFDS[1].IBUFDS_I\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \USE_IBUFDS.GEN_IBUFDS[2].IBUFDS_I\ : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[2].IBUFDS_I\ : label is "0";
  attribute IFD_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[2].IBUFDS_I\ : label is "AUTO";
  attribute box_type of \USE_IBUFDS.GEN_IBUFDS[2].IBUFDS_I\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \USE_IBUFDS.GEN_IBUFDS[3].IBUFDS_I\ : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[3].IBUFDS_I\ : label is "0";
  attribute IFD_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[3].IBUFDS_I\ : label is "AUTO";
  attribute box_type of \USE_IBUFDS.GEN_IBUFDS[3].IBUFDS_I\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \USE_IBUFDS.GEN_IBUFDS[4].IBUFDS_I\ : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[4].IBUFDS_I\ : label is "0";
  attribute IFD_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[4].IBUFDS_I\ : label is "AUTO";
  attribute box_type of \USE_IBUFDS.GEN_IBUFDS[4].IBUFDS_I\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \USE_IBUFDS.GEN_IBUFDS[5].IBUFDS_I\ : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[5].IBUFDS_I\ : label is "0";
  attribute IFD_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[5].IBUFDS_I\ : label is "AUTO";
  attribute box_type of \USE_IBUFDS.GEN_IBUFDS[5].IBUFDS_I\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \USE_IBUFDS.GEN_IBUFDS[6].IBUFDS_I\ : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[6].IBUFDS_I\ : label is "0";
  attribute IFD_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[6].IBUFDS_I\ : label is "AUTO";
  attribute box_type of \USE_IBUFDS.GEN_IBUFDS[6].IBUFDS_I\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \USE_IBUFDS.GEN_IBUFDS[7].IBUFDS_I\ : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[7].IBUFDS_I\ : label is "0";
  attribute IFD_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[7].IBUFDS_I\ : label is "AUTO";
  attribute box_type of \USE_IBUFDS.GEN_IBUFDS[7].IBUFDS_I\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \USE_IBUFDS.GEN_IBUFDS[8].IBUFDS_I\ : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[8].IBUFDS_I\ : label is "0";
  attribute IFD_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[8].IBUFDS_I\ : label is "AUTO";
  attribute box_type of \USE_IBUFDS.GEN_IBUFDS[8].IBUFDS_I\ : label is "PRIMITIVE";
  attribute CAPACITANCE of \USE_IBUFDS.GEN_IBUFDS[9].IBUFDS_I\ : label is "DONT_CARE";
  attribute IBUF_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[9].IBUFDS_I\ : label is "0";
  attribute IFD_DELAY_VALUE of \USE_IBUFDS.GEN_IBUFDS[9].IBUFDS_I\ : label is "AUTO";
  attribute box_type of \USE_IBUFDS.GEN_IBUFDS[9].IBUFDS_I\ : label is "PRIMITIVE";
begin
\USE_IBUFDS.GEN_IBUFDS[0].IBUFDS_I\: unisim.vcomponents.IBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => IBUF_DS_P(0),
      IB => IBUF_DS_N(0),
      O => IBUF_OUT(0)
    );
\USE_IBUFDS.GEN_IBUFDS[10].IBUFDS_I\: unisim.vcomponents.IBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => IBUF_DS_P(10),
      IB => IBUF_DS_N(10),
      O => IBUF_OUT(10)
    );
\USE_IBUFDS.GEN_IBUFDS[11].IBUFDS_I\: unisim.vcomponents.IBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => IBUF_DS_P(11),
      IB => IBUF_DS_N(11),
      O => IBUF_OUT(11)
    );
\USE_IBUFDS.GEN_IBUFDS[12].IBUFDS_I\: unisim.vcomponents.IBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => IBUF_DS_P(12),
      IB => IBUF_DS_N(12),
      O => IBUF_OUT(12)
    );
\USE_IBUFDS.GEN_IBUFDS[13].IBUFDS_I\: unisim.vcomponents.IBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => IBUF_DS_P(13),
      IB => IBUF_DS_N(13),
      O => IBUF_OUT(13)
    );
\USE_IBUFDS.GEN_IBUFDS[14].IBUFDS_I\: unisim.vcomponents.IBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => IBUF_DS_P(14),
      IB => IBUF_DS_N(14),
      O => IBUF_OUT(14)
    );
\USE_IBUFDS.GEN_IBUFDS[15].IBUFDS_I\: unisim.vcomponents.IBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => IBUF_DS_P(15),
      IB => IBUF_DS_N(15),
      O => IBUF_OUT(15)
    );
\USE_IBUFDS.GEN_IBUFDS[1].IBUFDS_I\: unisim.vcomponents.IBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => IBUF_DS_P(1),
      IB => IBUF_DS_N(1),
      O => IBUF_OUT(1)
    );
\USE_IBUFDS.GEN_IBUFDS[2].IBUFDS_I\: unisim.vcomponents.IBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => IBUF_DS_P(2),
      IB => IBUF_DS_N(2),
      O => IBUF_OUT(2)
    );
\USE_IBUFDS.GEN_IBUFDS[3].IBUFDS_I\: unisim.vcomponents.IBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => IBUF_DS_P(3),
      IB => IBUF_DS_N(3),
      O => IBUF_OUT(3)
    );
\USE_IBUFDS.GEN_IBUFDS[4].IBUFDS_I\: unisim.vcomponents.IBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => IBUF_DS_P(4),
      IB => IBUF_DS_N(4),
      O => IBUF_OUT(4)
    );
\USE_IBUFDS.GEN_IBUFDS[5].IBUFDS_I\: unisim.vcomponents.IBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => IBUF_DS_P(5),
      IB => IBUF_DS_N(5),
      O => IBUF_OUT(5)
    );
\USE_IBUFDS.GEN_IBUFDS[6].IBUFDS_I\: unisim.vcomponents.IBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => IBUF_DS_P(6),
      IB => IBUF_DS_N(6),
      O => IBUF_OUT(6)
    );
\USE_IBUFDS.GEN_IBUFDS[7].IBUFDS_I\: unisim.vcomponents.IBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => IBUF_DS_P(7),
      IB => IBUF_DS_N(7),
      O => IBUF_OUT(7)
    );
\USE_IBUFDS.GEN_IBUFDS[8].IBUFDS_I\: unisim.vcomponents.IBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => IBUF_DS_P(8),
      IB => IBUF_DS_N(8),
      O => IBUF_OUT(8)
    );
\USE_IBUFDS.GEN_IBUFDS[9].IBUFDS_I\: unisim.vcomponents.IBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => IBUF_DS_P(9),
      IB => IBUF_DS_N(9),
      O => IBUF_OUT(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity \Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized5\ is
  port (
    OBUF_DS_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    OBUF_DS_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    OBUF_IN : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of \Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized5\ : entity is "util_ds_buf";
end \Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized5\;

architecture STRUCTURE of \Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized5\ is
  attribute CAPACITANCE : string;
  attribute CAPACITANCE of \USE_OBUFDS.GEN_OBUFDS[0].OBUFDS_I\ : label is "DONT_CARE";
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \USE_OBUFDS.GEN_OBUFDS[0].OBUFDS_I\ : label is "OBUFDS";
  attribute box_type : string;
  attribute box_type of \USE_OBUFDS.GEN_OBUFDS[0].OBUFDS_I\ : label is "PRIMITIVE";
begin
\USE_OBUFDS.GEN_OBUFDS[0].OBUFDS_I\: unisim.vcomponents.OBUFDS
    generic map(
      IOSTANDARD => "DEFAULT"
    )
        port map (
      I => OBUF_IN(0),
      O => OBUF_DS_P(0),
      OB => OBUF_DS_N(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC500MSPS_Controller_0_0_design_1_ADC500MHz_Controller_v1_0_0_0 is
  port (
    ADC_clk : in STD_LOGIC;
    ADC_reset : in STD_LOGIC;
    ADC_Din : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ADC_Dout : out STD_LOGIC_VECTOR ( 31 downto 0 );
    data_valid : out STD_LOGIC;
    Ctrl_reg0_in : in STD_LOGIC_VECTOR ( 8 downto 0 );
    Ctrl_reg1_out : out STD_LOGIC_VECTOR ( 1 downto 0 );
    to_adc_cal_fmc : out STD_LOGIC;
    to_adc_caldly_nscs_fmc : out STD_LOGIC;
    to_adc_fsr_ece_fmc : out STD_LOGIC;
    to_adc_outv_slck_fmc : out STD_LOGIC;
    to_adc_outedge_ddr_sdata_fmc : out STD_LOGIC;
    to_adc_dclk_rst_fmc : out STD_LOGIC;
    to_adc_pd_fmc : out STD_LOGIC;
    to_adc_led_0 : out STD_LOGIC;
    to_adc_led_1 : out STD_LOGIC;
    from_adc_calrun_fmc : in STD_LOGIC;
    from_adc_or : in STD_LOGIC
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of Histo1_ADC500MSPS_Controller_0_0_design_1_ADC500MHz_Controller_v1_0_0_0 : entity is "design_1_ADC500MHz_Controller_v1_0_0_0,ADC500MHz_Controller_v1_0,{}";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC500MSPS_Controller_0_0_design_1_ADC500MHz_Controller_v1_0_0_0 : entity is "design_1_ADC500MHz_Controller_v1_0_0_0";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of Histo1_ADC500MSPS_Controller_0_0_design_1_ADC500MHz_Controller_v1_0_0_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of Histo1_ADC500MSPS_Controller_0_0_design_1_ADC500MHz_Controller_v1_0_0_0 : entity is "ADC500MHz_Controller_v1_0,Vivado 2017.4";
end Histo1_ADC500MSPS_Controller_0_0_design_1_ADC500MHz_Controller_v1_0_0_0;

architecture STRUCTURE of Histo1_ADC500MSPS_Controller_0_0_design_1_ADC500MHz_Controller_v1_0_0_0 is
  signal \^ctrl_reg0_in\ : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal \^from_adc_calrun_fmc\ : STD_LOGIC;
  signal \^from_adc_or\ : STD_LOGIC;
  attribute x_interface_info : string;
  attribute x_interface_info of ADC_clk : signal is "xilinx.com:signal:clock:1.0 ADC_clk CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of ADC_clk : signal is "XIL_INTERFACENAME ADC_clk, ASSOCIATED_RESET ADC_reset, FREQ_HZ 100000000, PHASE 0.000";
  attribute x_interface_info of ADC_reset : signal is "xilinx.com:signal:reset:1.0 ADC_reset RST";
  attribute x_interface_parameter of ADC_reset : signal is "XIL_INTERFACENAME ADC_reset, POLARITY ACTIVE_LOW";
begin
  Ctrl_reg1_out(1) <= \^from_adc_or\;
  Ctrl_reg1_out(0) <= \^from_adc_calrun_fmc\;
  \^ctrl_reg0_in\(8 downto 0) <= Ctrl_reg0_in(8 downto 0);
  \^from_adc_calrun_fmc\ <= from_adc_calrun_fmc;
  \^from_adc_or\ <= from_adc_or;
  to_adc_cal_fmc <= \^ctrl_reg0_in\(0);
  to_adc_caldly_nscs_fmc <= \^ctrl_reg0_in\(1);
  to_adc_dclk_rst_fmc <= \^ctrl_reg0_in\(5);
  to_adc_fsr_ece_fmc <= \^ctrl_reg0_in\(2);
  to_adc_led_0 <= \^ctrl_reg0_in\(7);
  to_adc_led_1 <= \^ctrl_reg0_in\(8);
  to_adc_outedge_ddr_sdata_fmc <= \^ctrl_reg0_in\(4);
  to_adc_outv_slck_fmc <= \^ctrl_reg0_in\(3);
  to_adc_pd_fmc <= \^ctrl_reg0_in\(6);
U0: entity work.Histo1_ADC500MSPS_Controller_0_0_ADC500MHz_Controller_v1_0
     port map (
      ADC_Din(15 downto 0) => ADC_Din(15 downto 0),
      ADC_Dout(31 downto 0) => ADC_Dout(31 downto 0),
      ADC_clk => ADC_clk,
      ADC_reset => ADC_reset,
      data_valid => data_valid
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC500MSPS_Controller_0_0_design_1_clk_wiz_0_0 is
  port (
    clk_out1 : out STD_LOGIC;
    resetn : in STD_LOGIC;
    clk_in1 : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC500MSPS_Controller_0_0_design_1_clk_wiz_0_0 : entity is "design_1_clk_wiz_0_0";
end Histo1_ADC500MSPS_Controller_0_0_design_1_clk_wiz_0_0;

architecture STRUCTURE of Histo1_ADC500MSPS_Controller_0_0_design_1_clk_wiz_0_0 is
begin
inst: entity work.Histo1_ADC500MSPS_Controller_0_0_design_1_clk_wiz_0_0_clk_wiz
     port map (
      clk_in1 => clk_in1,
      clk_out1 => clk_out1,
      resetn => resetn
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_0 is
  port (
    OBUF_IN : in STD_LOGIC_VECTOR ( 0 to 0 );
    OBUF_DS_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    OBUF_DS_N : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_0 : entity is "design_1_util_ds_buf_0_0,util_ds_buf,{}";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_0 : entity is "design_1_util_ds_buf_0_0";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_0 : entity is "util_ds_buf,Vivado 2017.4";
end Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_0;

architecture STRUCTURE of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_0 is
  attribute x_interface_info : string;
  attribute x_interface_info of OBUF_DS_N : signal is "xilinx.com:signal:clock:1.0 OBUF_DS_N CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of OBUF_DS_N : signal is "XIL_INTERFACENAME OBUF_DS_N, FREQ_HZ 100000000, PHASE 0.000";
  attribute x_interface_info of OBUF_DS_P : signal is "xilinx.com:signal:clock:1.0 OBUF_DS_P CLK";
  attribute x_interface_parameter of OBUF_DS_P : signal is "XIL_INTERFACENAME OBUF_DS_P, FREQ_HZ 100000000, PHASE 0.000";
  attribute x_interface_info of OBUF_IN : signal is "xilinx.com:signal:clock:1.0 OBUF_IN CLK";
  attribute x_interface_parameter of OBUF_IN : signal is "XIL_INTERFACENAME OBUF_IN, FREQ_HZ 100000000, PHASE 0.000";
begin
U0: entity work.\Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized5\
     port map (
      OBUF_DS_N(0) => OBUF_DS_N(0),
      OBUF_DS_P(0) => OBUF_DS_P(0),
      OBUF_IN(0) => OBUF_IN(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_1 is
  port (
    IBUF_DS_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    IBUF_DS_N : in STD_LOGIC_VECTOR ( 0 to 0 );
    IBUF_OUT : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_1 : entity is "design_1_util_ds_buf_0_1,util_ds_buf,{}";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_1 : entity is "design_1_util_ds_buf_0_1";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_1 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_1 : entity is "util_ds_buf,Vivado 2017.4";
end Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_1;

architecture STRUCTURE of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_1 is
  attribute x_interface_info : string;
  attribute x_interface_info of IBUF_DS_N : signal is "xilinx.com:interface:diff_clock:1.0 CLK_IN_D CLK_N";
  attribute x_interface_info of IBUF_DS_P : signal is "xilinx.com:interface:diff_clock:1.0 CLK_IN_D CLK_P";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of IBUF_DS_P : signal is "XIL_INTERFACENAME CLK_IN_D, BOARD.ASSOCIATED_PARAM DIFF_CLK_IN_BOARD_INTERFACE, CAN_DEBUG false, FREQ_HZ 100000000";
  attribute x_interface_info of IBUF_OUT : signal is "xilinx.com:signal:clock:1.0 IBUF_OUT CLK";
  attribute x_interface_parameter of IBUF_OUT : signal is "XIL_INTERFACENAME IBUF_OUT, FREQ_HZ 100000000, PHASE 0.000";
begin
U0: entity work.\Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized1_1\
     port map (
      IBUF_DS_N(0) => IBUF_DS_N(0),
      IBUF_DS_P(0) => IBUF_DS_P(0),
      IBUF_OUT(0) => IBUF_OUT(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_1_0 is
  port (
    IBUF_DS_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    IBUF_DS_N : in STD_LOGIC_VECTOR ( 0 to 0 );
    IBUF_OUT : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_1_0 : entity is "design_1_util_ds_buf_1_0,util_ds_buf,{}";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_1_0 : entity is "design_1_util_ds_buf_1_0";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_1_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_1_0 : entity is "util_ds_buf,Vivado 2017.4";
end Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_1_0;

architecture STRUCTURE of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_1_0 is
  attribute x_interface_info : string;
  attribute x_interface_info of IBUF_DS_N : signal is "xilinx.com:interface:diff_clock:1.0 CLK_IN_D CLK_N";
  attribute x_interface_info of IBUF_DS_P : signal is "xilinx.com:interface:diff_clock:1.0 CLK_IN_D CLK_P";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of IBUF_DS_P : signal is "XIL_INTERFACENAME CLK_IN_D, BOARD.ASSOCIATED_PARAM DIFF_CLK_IN_BOARD_INTERFACE, CAN_DEBUG false, FREQ_HZ 100000000";
  attribute x_interface_info of IBUF_OUT : signal is "xilinx.com:signal:clock:1.0 IBUF_OUT CLK";
  attribute x_interface_parameter of IBUF_OUT : signal is "XIL_INTERFACENAME IBUF_OUT, FREQ_HZ 100000000, PHASE 0.000";
begin
U0: entity work.\Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized1\
     port map (
      IBUF_DS_N(0) => IBUF_DS_N(0),
      IBUF_DS_P(0) => IBUF_DS_P(0),
      IBUF_OUT(0) => IBUF_OUT(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_2_0 is
  port (
    BUFG_I : in STD_LOGIC_VECTOR ( 0 to 0 );
    BUFG_O : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_2_0 : entity is "design_1_util_ds_buf_2_0,util_ds_buf,{}";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_2_0 : entity is "design_1_util_ds_buf_2_0";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_2_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_2_0 : entity is "util_ds_buf,Vivado 2017.4";
end Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_2_0;

architecture STRUCTURE of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_2_0 is
  attribute x_interface_info : string;
  attribute x_interface_info of BUFG_I : signal is "xilinx.com:signal:clock:1.0 BUFG_I CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of BUFG_I : signal is "XIL_INTERFACENAME BUFG_I, FREQ_HZ 100000000, PHASE 0.000";
  attribute x_interface_info of BUFG_O : signal is "xilinx.com:signal:clock:1.0 BUFG_O CLK";
  attribute x_interface_parameter of BUFG_O : signal is "XIL_INTERFACENAME BUFG_O, FREQ_HZ 100000000, PHASE 0.000";
begin
U0: entity work.Histo1_ADC500MSPS_Controller_0_0_util_ds_buf
     port map (
      BUFG_I(0) => BUFG_I(0),
      BUFG_O(0) => BUFG_O(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_3_0 is
  port (
    IBUF_DS_P : in STD_LOGIC_VECTOR ( 15 downto 0 );
    IBUF_DS_N : in STD_LOGIC_VECTOR ( 15 downto 0 );
    IBUF_OUT : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_3_0 : entity is "design_1_util_ds_buf_3_0,util_ds_buf,{}";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_3_0 : entity is "design_1_util_ds_buf_3_0";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_3_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_3_0 : entity is "util_ds_buf,Vivado 2017.4";
end Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_3_0;

architecture STRUCTURE of Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_3_0 is
  attribute x_interface_info : string;
  attribute x_interface_info of IBUF_DS_N : signal is "xilinx.com:interface:diff_clock:1.0 CLK_IN_D CLK_N";
  attribute x_interface_info of IBUF_DS_P : signal is "xilinx.com:interface:diff_clock:1.0 CLK_IN_D CLK_P";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of IBUF_DS_P : signal is "XIL_INTERFACENAME CLK_IN_D, BOARD.ASSOCIATED_PARAM DIFF_CLK_IN_BOARD_INTERFACE, CAN_DEBUG false, FREQ_HZ 100000000";
  attribute x_interface_info of IBUF_OUT : signal is "xilinx.com:signal:clock:1.0 IBUF_OUT CLK";
  attribute x_interface_parameter of IBUF_OUT : signal is "XIL_INTERFACENAME IBUF_OUT, FREQ_HZ 100000000, PHASE 0.000";
begin
U0: entity work.\Histo1_ADC500MSPS_Controller_0_0_util_ds_buf__parameterized3\
     port map (
      IBUF_DS_N(15 downto 0) => IBUF_DS_N(15 downto 0),
      IBUF_DS_P(15 downto 0) => IBUF_DS_P(15 downto 0),
      IBUF_OUT(15 downto 0) => IBUF_OUT(15 downto 0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC500MSPS_Controller_0_0_lpf is
  port (
    lpf_int : out STD_LOGIC;
    slowest_sync_clk : in STD_LOGIC;
    dcm_locked : in STD_LOGIC;
    aux_reset_in : in STD_LOGIC;
    mb_debug_sys_rst : in STD_LOGIC;
    ext_reset_in : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC500MSPS_Controller_0_0_lpf : entity is "lpf";
end Histo1_ADC500MSPS_Controller_0_0_lpf;

architecture STRUCTURE of Histo1_ADC500MSPS_Controller_0_0_lpf is
  signal \ACTIVE_LOW_AUX.ACT_LO_AUX_n_0\ : STD_LOGIC;
  signal \ACTIVE_LOW_EXT.ACT_LO_EXT_n_0\ : STD_LOGIC;
  signal Q : STD_LOGIC;
  signal asr_lpf : STD_LOGIC_VECTOR ( 0 to 0 );
  signal lpf_asr : STD_LOGIC;
  signal lpf_exr : STD_LOGIC;
  signal \lpf_int0__0\ : STD_LOGIC;
  signal p_1_in : STD_LOGIC;
  signal p_2_in : STD_LOGIC;
  signal p_3_in1_in : STD_LOGIC;
  signal p_3_out : STD_LOGIC_VECTOR ( 3 downto 0 );
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of POR_SRL_I : label is "SRL16";
  attribute box_type : string;
  attribute box_type of POR_SRL_I : label is "PRIMITIVE";
  attribute srl_name : string;
  attribute srl_name of POR_SRL_I : label is "\U0/reset_generator /U0/\EXT_LPF/POR_SRL_I ";
begin
\ACTIVE_LOW_AUX.ACT_LO_AUX\: entity work.Histo1_ADC500MSPS_Controller_0_0_cdc_sync
     port map (
      asr_lpf(0) => asr_lpf(0),
      aux_reset_in => aux_reset_in,
      lpf_asr => lpf_asr,
      lpf_asr_reg => \ACTIVE_LOW_AUX.ACT_LO_AUX_n_0\,
      p_1_in => p_1_in,
      p_2_in => p_2_in,
      scndry_out => p_3_in1_in,
      slowest_sync_clk => slowest_sync_clk
    );
\ACTIVE_LOW_EXT.ACT_LO_EXT\: entity work.Histo1_ADC500MSPS_Controller_0_0_cdc_sync_0
     port map (
      ext_reset_in => ext_reset_in,
      lpf_exr => lpf_exr,
      lpf_exr_reg => \ACTIVE_LOW_EXT.ACT_LO_EXT_n_0\,
      mb_debug_sys_rst => mb_debug_sys_rst,
      p_3_out(2 downto 0) => p_3_out(2 downto 0),
      scndry_out => p_3_out(3),
      slowest_sync_clk => slowest_sync_clk
    );
\AUX_LPF[1].asr_lpf_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => p_3_in1_in,
      Q => p_2_in,
      R => '0'
    );
\AUX_LPF[2].asr_lpf_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => p_2_in,
      Q => p_1_in,
      R => '0'
    );
\AUX_LPF[3].asr_lpf_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => p_1_in,
      Q => asr_lpf(0),
      R => '0'
    );
\EXT_LPF[1].exr_lpf_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => p_3_out(3),
      Q => p_3_out(2),
      R => '0'
    );
\EXT_LPF[2].exr_lpf_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => p_3_out(2),
      Q => p_3_out(1),
      R => '0'
    );
\EXT_LPF[3].exr_lpf_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => p_3_out(1),
      Q => p_3_out(0),
      R => '0'
    );
POR_SRL_I: unisim.vcomponents.SRL16E
    generic map(
      INIT => X"FFFF"
    )
        port map (
      A0 => '1',
      A1 => '1',
      A2 => '1',
      A3 => '1',
      CE => '1',
      CLK => slowest_sync_clk,
      D => '0',
      Q => Q
    );
lpf_asr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => \ACTIVE_LOW_AUX.ACT_LO_AUX_n_0\,
      Q => lpf_asr,
      R => '0'
    );
lpf_exr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => \ACTIVE_LOW_EXT.ACT_LO_EXT_n_0\,
      Q => lpf_exr,
      R => '0'
    );
lpf_int0: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => dcm_locked,
      I1 => Q,
      I2 => lpf_exr,
      I3 => lpf_asr,
      O => \lpf_int0__0\
    );
lpf_int_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => \lpf_int0__0\,
      Q => lpf_int,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC500MSPS_Controller_0_0_sequence_psr is
  port (
    MB_out : out STD_LOGIC;
    Bsr_out : out STD_LOGIC;
    Pr_out : out STD_LOGIC;
    \ACTIVE_LOW_BSR_OUT_DFF[0].FDRE_BSR_N\ : out STD_LOGIC;
    \ACTIVE_LOW_PR_OUT_DFF[0].FDRE_PER_N\ : out STD_LOGIC;
    lpf_int : in STD_LOGIC;
    slowest_sync_clk : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC500MSPS_Controller_0_0_sequence_psr : entity is "sequence_psr";
end Histo1_ADC500MSPS_Controller_0_0_sequence_psr;

architecture STRUCTURE of Histo1_ADC500MSPS_Controller_0_0_sequence_psr is
  signal \^bsr_out\ : STD_LOGIC;
  signal Core_i_1_n_0 : STD_LOGIC;
  signal \^mb_out\ : STD_LOGIC;
  signal \^pr_out\ : STD_LOGIC;
  signal \bsr_dec_reg_n_0_[0]\ : STD_LOGIC;
  signal \bsr_dec_reg_n_0_[2]\ : STD_LOGIC;
  signal bsr_i_1_n_0 : STD_LOGIC;
  signal \core_dec[0]_i_1_n_0\ : STD_LOGIC;
  signal \core_dec[2]_i_1_n_0\ : STD_LOGIC;
  signal \core_dec_reg_n_0_[0]\ : STD_LOGIC;
  signal \core_dec_reg_n_0_[1]\ : STD_LOGIC;
  signal from_sys_i_1_n_0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal p_3_out : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal p_5_out : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \pr_dec0__0\ : STD_LOGIC;
  signal \pr_dec_reg_n_0_[0]\ : STD_LOGIC;
  signal \pr_dec_reg_n_0_[2]\ : STD_LOGIC;
  signal pr_i_1_n_0 : STD_LOGIC;
  signal seq_clr : STD_LOGIC;
  signal seq_cnt : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal seq_cnt_en : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \ACTIVE_LOW_BSR_OUT_DFF[0].FDRE_BSR_N_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \ACTIVE_LOW_PR_OUT_DFF[0].FDRE_PER_N_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of Core_i_1 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \bsr_dec[2]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of bsr_i_1 : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \core_dec[0]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \core_dec[2]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of from_sys_i_1 : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \pr_dec[0]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of pr_i_1 : label is "soft_lutpair4";
begin
  Bsr_out <= \^bsr_out\;
  MB_out <= \^mb_out\;
  Pr_out <= \^pr_out\;
\ACTIVE_LOW_BSR_OUT_DFF[0].FDRE_BSR_N_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^bsr_out\,
      O => \ACTIVE_LOW_BSR_OUT_DFF[0].FDRE_BSR_N\
    );
\ACTIVE_LOW_PR_OUT_DFF[0].FDRE_PER_N_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^pr_out\,
      O => \ACTIVE_LOW_PR_OUT_DFF[0].FDRE_PER_N\
    );
Core_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^mb_out\,
      I1 => p_0_in,
      O => Core_i_1_n_0
    );
Core_reg: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => Core_i_1_n_0,
      Q => \^mb_out\,
      S => lpf_int
    );
SEQ_COUNTER: entity work.Histo1_ADC500MSPS_Controller_0_0_upcnt_n
     port map (
      Q(5 downto 0) => seq_cnt(5 downto 0),
      seq_clr => seq_clr,
      seq_cnt_en => seq_cnt_en,
      slowest_sync_clk => slowest_sync_clk
    );
\bsr_dec[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0804"
    )
        port map (
      I0 => seq_cnt_en,
      I1 => seq_cnt(3),
      I2 => seq_cnt(5),
      I3 => seq_cnt(4),
      O => p_5_out(0)
    );
\bsr_dec[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \core_dec_reg_n_0_[1]\,
      I1 => \bsr_dec_reg_n_0_[0]\,
      O => p_5_out(2)
    );
\bsr_dec_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => p_5_out(0),
      Q => \bsr_dec_reg_n_0_[0]\,
      R => '0'
    );
\bsr_dec_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => p_5_out(2),
      Q => \bsr_dec_reg_n_0_[2]\,
      R => '0'
    );
bsr_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^bsr_out\,
      I1 => \bsr_dec_reg_n_0_[2]\,
      O => bsr_i_1_n_0
    );
bsr_reg: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => bsr_i_1_n_0,
      Q => \^bsr_out\,
      S => lpf_int
    );
\core_dec[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"8040"
    )
        port map (
      I0 => seq_cnt(4),
      I1 => seq_cnt(3),
      I2 => seq_cnt(5),
      I3 => seq_cnt_en,
      O => \core_dec[0]_i_1_n_0\
    );
\core_dec[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \core_dec_reg_n_0_[1]\,
      I1 => \core_dec_reg_n_0_[0]\,
      O => \core_dec[2]_i_1_n_0\
    );
\core_dec_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => \core_dec[0]_i_1_n_0\,
      Q => \core_dec_reg_n_0_[0]\,
      R => '0'
    );
\core_dec_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => \pr_dec0__0\,
      Q => \core_dec_reg_n_0_[1]\,
      R => '0'
    );
\core_dec_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => \core_dec[2]_i_1_n_0\,
      Q => p_0_in,
      R => '0'
    );
from_sys_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^mb_out\,
      I1 => seq_cnt_en,
      O => from_sys_i_1_n_0
    );
from_sys_reg: unisim.vcomponents.FDSE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => from_sys_i_1_n_0,
      Q => seq_cnt_en,
      S => lpf_int
    );
pr_dec0: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0210"
    )
        port map (
      I0 => seq_cnt(0),
      I1 => seq_cnt(1),
      I2 => seq_cnt(2),
      I3 => seq_cnt_en,
      O => \pr_dec0__0\
    );
\pr_dec[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1080"
    )
        port map (
      I0 => seq_cnt_en,
      I1 => seq_cnt(5),
      I2 => seq_cnt(3),
      I3 => seq_cnt(4),
      O => p_3_out(0)
    );
\pr_dec[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \core_dec_reg_n_0_[1]\,
      I1 => \pr_dec_reg_n_0_[0]\,
      O => p_3_out(2)
    );
\pr_dec_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => p_3_out(0),
      Q => \pr_dec_reg_n_0_[0]\,
      R => '0'
    );
\pr_dec_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => p_3_out(2),
      Q => \pr_dec_reg_n_0_[2]\,
      R => '0'
    );
pr_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \^pr_out\,
      I1 => \pr_dec_reg_n_0_[2]\,
      O => pr_i_1_n_0
    );
pr_reg: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => pr_i_1_n_0,
      Q => \^pr_out\,
      S => lpf_int
    );
seq_clr_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => '1',
      Q => seq_clr,
      R => lpf_int
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC500MSPS_Controller_0_0_proc_sys_reset is
  port (
    slowest_sync_clk : in STD_LOGIC;
    ext_reset_in : in STD_LOGIC;
    aux_reset_in : in STD_LOGIC;
    mb_debug_sys_rst : in STD_LOGIC;
    dcm_locked : in STD_LOGIC;
    mb_reset : out STD_LOGIC;
    bus_struct_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    interconnect_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute C_AUX_RESET_HIGH : string;
  attribute C_AUX_RESET_HIGH of Histo1_ADC500MSPS_Controller_0_0_proc_sys_reset : entity is "1'b0";
  attribute C_AUX_RST_WIDTH : integer;
  attribute C_AUX_RST_WIDTH of Histo1_ADC500MSPS_Controller_0_0_proc_sys_reset : entity is 4;
  attribute C_EXT_RESET_HIGH : string;
  attribute C_EXT_RESET_HIGH of Histo1_ADC500MSPS_Controller_0_0_proc_sys_reset : entity is "1'b0";
  attribute C_EXT_RST_WIDTH : integer;
  attribute C_EXT_RST_WIDTH of Histo1_ADC500MSPS_Controller_0_0_proc_sys_reset : entity is 4;
  attribute C_FAMILY : string;
  attribute C_FAMILY of Histo1_ADC500MSPS_Controller_0_0_proc_sys_reset : entity is "zynq";
  attribute C_NUM_BUS_RST : integer;
  attribute C_NUM_BUS_RST of Histo1_ADC500MSPS_Controller_0_0_proc_sys_reset : entity is 1;
  attribute C_NUM_INTERCONNECT_ARESETN : integer;
  attribute C_NUM_INTERCONNECT_ARESETN of Histo1_ADC500MSPS_Controller_0_0_proc_sys_reset : entity is 1;
  attribute C_NUM_PERP_ARESETN : integer;
  attribute C_NUM_PERP_ARESETN of Histo1_ADC500MSPS_Controller_0_0_proc_sys_reset : entity is 1;
  attribute C_NUM_PERP_RST : integer;
  attribute C_NUM_PERP_RST of Histo1_ADC500MSPS_Controller_0_0_proc_sys_reset : entity is 1;
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC500MSPS_Controller_0_0_proc_sys_reset : entity is "proc_sys_reset";
end Histo1_ADC500MSPS_Controller_0_0_proc_sys_reset;

architecture STRUCTURE of Histo1_ADC500MSPS_Controller_0_0_proc_sys_reset is
  signal Bsr_out : STD_LOGIC;
  signal MB_out : STD_LOGIC;
  signal Pr_out : STD_LOGIC;
  signal SEQ_n_3 : STD_LOGIC;
  signal SEQ_n_4 : STD_LOGIC;
  signal lpf_int : STD_LOGIC;
  attribute box_type : string;
  attribute box_type of \ACTIVE_LOW_BSR_OUT_DFF[0].FDRE_BSR_N\ : label is "PRIMITIVE";
  attribute box_type of \ACTIVE_LOW_PR_OUT_DFF[0].FDRE_PER_N\ : label is "PRIMITIVE";
  attribute box_type of \BSR_OUT_DFF[0].FDRE_BSR\ : label is "PRIMITIVE";
  attribute box_type of FDRE_inst : label is "PRIMITIVE";
  attribute box_type of \PR_OUT_DFF[0].FDRE_PER\ : label is "PRIMITIVE";
  attribute equivalent_register_removal : string;
  attribute equivalent_register_removal of bus_struct_reset : signal is "no";
  attribute equivalent_register_removal of interconnect_aresetn : signal is "no";
  attribute equivalent_register_removal of peripheral_aresetn : signal is "no";
  attribute equivalent_register_removal of peripheral_reset : signal is "no";
begin
\ACTIVE_LOW_BSR_OUT_DFF[0].FDRE_BSR_N\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '0',
      IS_D_INVERTED => '0',
      IS_R_INVERTED => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => SEQ_n_3,
      Q => interconnect_aresetn(0),
      R => '0'
    );
\ACTIVE_LOW_PR_OUT_DFF[0].FDRE_PER_N\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '0',
      IS_D_INVERTED => '0',
      IS_R_INVERTED => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => SEQ_n_4,
      Q => peripheral_aresetn(0),
      R => '0'
    );
\BSR_OUT_DFF[0].FDRE_BSR\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1',
      IS_C_INVERTED => '0',
      IS_D_INVERTED => '0',
      IS_R_INVERTED => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => Bsr_out,
      Q => bus_struct_reset(0),
      R => '0'
    );
EXT_LPF: entity work.Histo1_ADC500MSPS_Controller_0_0_lpf
     port map (
      aux_reset_in => aux_reset_in,
      dcm_locked => dcm_locked,
      ext_reset_in => ext_reset_in,
      lpf_int => lpf_int,
      mb_debug_sys_rst => mb_debug_sys_rst,
      slowest_sync_clk => slowest_sync_clk
    );
FDRE_inst: unisim.vcomponents.FDRE
    generic map(
      INIT => '1',
      IS_C_INVERTED => '0',
      IS_D_INVERTED => '0',
      IS_R_INVERTED => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => MB_out,
      Q => mb_reset,
      R => '0'
    );
\PR_OUT_DFF[0].FDRE_PER\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1',
      IS_C_INVERTED => '0',
      IS_D_INVERTED => '0',
      IS_R_INVERTED => '0'
    )
        port map (
      C => slowest_sync_clk,
      CE => '1',
      D => Pr_out,
      Q => peripheral_reset(0),
      R => '0'
    );
SEQ: entity work.Histo1_ADC500MSPS_Controller_0_0_sequence_psr
     port map (
      \ACTIVE_LOW_BSR_OUT_DFF[0].FDRE_BSR_N\ => SEQ_n_3,
      \ACTIVE_LOW_PR_OUT_DFF[0].FDRE_PER_N\ => SEQ_n_4,
      Bsr_out => Bsr_out,
      MB_out => MB_out,
      Pr_out => Pr_out,
      lpf_int => lpf_int,
      slowest_sync_clk => slowest_sync_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC500MSPS_Controller_0_0_design_1_proc_sys_reset_0_0 is
  port (
    slowest_sync_clk : in STD_LOGIC;
    ext_reset_in : in STD_LOGIC;
    aux_reset_in : in STD_LOGIC;
    mb_debug_sys_rst : in STD_LOGIC;
    dcm_locked : in STD_LOGIC;
    mb_reset : out STD_LOGIC;
    bus_struct_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    interconnect_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of Histo1_ADC500MSPS_Controller_0_0_design_1_proc_sys_reset_0_0 : entity is "design_1_proc_sys_reset_0_0,proc_sys_reset,{}";
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC500MSPS_Controller_0_0_design_1_proc_sys_reset_0_0 : entity is "design_1_proc_sys_reset_0_0";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of Histo1_ADC500MSPS_Controller_0_0_design_1_proc_sys_reset_0_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of Histo1_ADC500MSPS_Controller_0_0_design_1_proc_sys_reset_0_0 : entity is "proc_sys_reset,Vivado 2017.4";
end Histo1_ADC500MSPS_Controller_0_0_design_1_proc_sys_reset_0_0;

architecture STRUCTURE of Histo1_ADC500MSPS_Controller_0_0_design_1_proc_sys_reset_0_0 is
  attribute C_AUX_RESET_HIGH : string;
  attribute C_AUX_RESET_HIGH of U0 : label is "1'b0";
  attribute C_AUX_RST_WIDTH : integer;
  attribute C_AUX_RST_WIDTH of U0 : label is 4;
  attribute C_EXT_RESET_HIGH : string;
  attribute C_EXT_RESET_HIGH of U0 : label is "1'b0";
  attribute C_EXT_RST_WIDTH : integer;
  attribute C_EXT_RST_WIDTH of U0 : label is 4;
  attribute C_FAMILY : string;
  attribute C_FAMILY of U0 : label is "zynq";
  attribute C_NUM_BUS_RST : integer;
  attribute C_NUM_BUS_RST of U0 : label is 1;
  attribute C_NUM_INTERCONNECT_ARESETN : integer;
  attribute C_NUM_INTERCONNECT_ARESETN of U0 : label is 1;
  attribute C_NUM_PERP_ARESETN : integer;
  attribute C_NUM_PERP_ARESETN of U0 : label is 1;
  attribute C_NUM_PERP_RST : integer;
  attribute C_NUM_PERP_RST of U0 : label is 1;
  attribute x_interface_info : string;
  attribute x_interface_info of aux_reset_in : signal is "xilinx.com:signal:reset:1.0 aux_reset RST";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of aux_reset_in : signal is "XIL_INTERFACENAME aux_reset, POLARITY ACTIVE_LOW";
  attribute x_interface_info of ext_reset_in : signal is "xilinx.com:signal:reset:1.0 ext_reset RST";
  attribute x_interface_parameter of ext_reset_in : signal is "XIL_INTERFACENAME ext_reset, BOARD.ASSOCIATED_PARAM RESET_BOARD_INTERFACE, POLARITY ACTIVE_LOW";
  attribute x_interface_info of mb_debug_sys_rst : signal is "xilinx.com:signal:reset:1.0 dbg_reset RST";
  attribute x_interface_parameter of mb_debug_sys_rst : signal is "XIL_INTERFACENAME dbg_reset, POLARITY ACTIVE_HIGH";
  attribute x_interface_info of mb_reset : signal is "xilinx.com:signal:reset:1.0 mb_rst RST";
  attribute x_interface_parameter of mb_reset : signal is "XIL_INTERFACENAME mb_rst, POLARITY ACTIVE_HIGH, TYPE PROCESSOR";
  attribute x_interface_info of slowest_sync_clk : signal is "xilinx.com:signal:clock:1.0 clock CLK";
  attribute x_interface_parameter of slowest_sync_clk : signal is "XIL_INTERFACENAME clock, ASSOCIATED_RESET mb_reset:bus_struct_reset:interconnect_aresetn:peripheral_aresetn:peripheral_reset, FREQ_HZ 100000000, PHASE 0.000";
  attribute x_interface_info of bus_struct_reset : signal is "xilinx.com:signal:reset:1.0 bus_struct_reset RST";
  attribute x_interface_parameter of bus_struct_reset : signal is "XIL_INTERFACENAME bus_struct_reset, POLARITY ACTIVE_HIGH, TYPE INTERCONNECT";
  attribute x_interface_info of interconnect_aresetn : signal is "xilinx.com:signal:reset:1.0 interconnect_low_rst RST";
  attribute x_interface_parameter of interconnect_aresetn : signal is "XIL_INTERFACENAME interconnect_low_rst, POLARITY ACTIVE_LOW, TYPE INTERCONNECT";
  attribute x_interface_info of peripheral_aresetn : signal is "xilinx.com:signal:reset:1.0 peripheral_low_rst RST";
  attribute x_interface_parameter of peripheral_aresetn : signal is "XIL_INTERFACENAME peripheral_low_rst, POLARITY ACTIVE_LOW, TYPE PERIPHERAL";
  attribute x_interface_info of peripheral_reset : signal is "xilinx.com:signal:reset:1.0 peripheral_high_rst RST";
  attribute x_interface_parameter of peripheral_reset : signal is "XIL_INTERFACENAME peripheral_high_rst, POLARITY ACTIVE_HIGH, TYPE PERIPHERAL";
begin
U0: entity work.Histo1_ADC500MSPS_Controller_0_0_proc_sys_reset
     port map (
      aux_reset_in => aux_reset_in,
      bus_struct_reset(0) => bus_struct_reset(0),
      dcm_locked => dcm_locked,
      ext_reset_in => ext_reset_in,
      interconnect_aresetn(0) => interconnect_aresetn(0),
      mb_debug_sys_rst => mb_debug_sys_rst,
      mb_reset => mb_reset,
      peripheral_aresetn(0) => peripheral_aresetn(0),
      peripheral_reset(0) => peripheral_reset(0),
      slowest_sync_clk => slowest_sync_clk
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC500MSPS_Controller_0_0_design_1_adc is
  port (
    ADC_Dout_for_PL : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ADC_Dout_valid : out STD_LOGIC;
    Ctrl_reg1_out : out STD_LOGIC_VECTOR ( 1 downto 0 );
    to_adc_cal_fmc : out STD_LOGIC;
    to_adc_caldly_nscs_fmc : out STD_LOGIC;
    to_adc_fsr_ece_fmc : out STD_LOGIC;
    to_adc_outv_slck_fmc : out STD_LOGIC;
    to_adc_outedge_ddr_sdata_fmc : out STD_LOGIC;
    to_adc_dclk_rst_fmc : out STD_LOGIC;
    to_adc_pd_fmc : out STD_LOGIC;
    to_adc_led_0 : out STD_LOGIC;
    to_adc_led_1 : out STD_LOGIC;
    ADC_clk_for_PL : out STD_LOGIC_VECTOR ( 0 to 0 );
    ADC_Dout_2Sample : out STD_LOGIC_VECTOR ( 15 downto 0 );
    clk_to_ADC_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_to_ADC_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    Ctrl_reg0_in : in STD_LOGIC_VECTOR ( 8 downto 0 );
    from_adc_calrun_fmc : in STD_LOGIC;
    from_ADC_OR_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    from_ADC_OR_N : in STD_LOGIC_VECTOR ( 0 to 0 );
    Data_from_ADC_P : in STD_LOGIC_VECTOR ( 15 downto 0 );
    Data_from_ADC_N : in STD_LOGIC_VECTOR ( 15 downto 0 );
    clk_from_ADC_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk_from_ADC_N : in STD_LOGIC_VECTOR ( 0 to 0 );
    clock : in STD_LOGIC;
    reset : in STD_LOGIC
  );
  attribute ORIG_REF_NAME : string;
  attribute ORIG_REF_NAME of Histo1_ADC500MSPS_Controller_0_0_design_1_adc : entity is "design_1_adc";
end Histo1_ADC500MSPS_Controller_0_0_design_1_adc;

architecture STRUCTURE of Histo1_ADC500MSPS_Controller_0_0_design_1_adc is
  signal \^adc_dout_2sample\ : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \^adc_clk_for_pl\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal DS_ADC_OR_Buffer_IBUF_OUT : STD_LOGIC;
  signal clk_wiz_0_clk_out1 : STD_LOGIC;
  signal reset_generator_peripheral_aresetn_0 : STD_LOGIC;
  signal util_ds_buf_1_IBUF_OUT1 : STD_LOGIC;
  signal NLW_reset_generator_mb_reset_UNCONNECTED : STD_LOGIC;
  signal NLW_reset_generator_bus_struct_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_reset_generator_interconnect_aresetn_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_reset_generator_peripheral_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of ADC500MHz_Controller_v1_0_0 : label is "design_1_ADC500MHz_Controller_v1_0_0_0,ADC500MHz_Controller_v1_0,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of ADC500MHz_Controller_v1_0_0 : label is "yes";
  attribute x_core_info : string;
  attribute x_core_info of ADC500MHz_Controller_v1_0_0 : label is "ADC500MHz_Controller_v1_0,Vivado 2017.4";
  attribute CHECK_LICENSE_TYPE of BUFG_clk : label is "design_1_util_ds_buf_2_0,util_ds_buf,{}";
  attribute downgradeipidentifiedwarnings of BUFG_clk : label is "yes";
  attribute x_core_info of BUFG_clk : label is "util_ds_buf,Vivado 2017.4";
  attribute CHECK_LICENSE_TYPE of DS_ADC_OR_Buffer : label is "design_1_util_ds_buf_0_1,util_ds_buf,{}";
  attribute downgradeipidentifiedwarnings of DS_ADC_OR_Buffer : label is "yes";
  attribute x_core_info of DS_ADC_OR_Buffer : label is "util_ds_buf,Vivado 2017.4";
  attribute CHECK_LICENSE_TYPE of DS_Data_Buffer : label is "design_1_util_ds_buf_3_0,util_ds_buf,{}";
  attribute downgradeipidentifiedwarnings of DS_Data_Buffer : label is "yes";
  attribute x_core_info of DS_Data_Buffer : label is "util_ds_buf,Vivado 2017.4";
  attribute CHECK_LICENSE_TYPE of DS_clk_Buffer : label is "design_1_util_ds_buf_1_0,util_ds_buf,{}";
  attribute downgradeipidentifiedwarnings of DS_clk_Buffer : label is "yes";
  attribute x_core_info of DS_clk_Buffer : label is "util_ds_buf,Vivado 2017.4";
  attribute CHECK_LICENSE_TYPE of SD_clk_Buffer : label is "design_1_util_ds_buf_0_0,util_ds_buf,{}";
  attribute downgradeipidentifiedwarnings of SD_clk_Buffer : label is "yes";
  attribute x_core_info of SD_clk_Buffer : label is "util_ds_buf,Vivado 2017.4";
  attribute CHECK_LICENSE_TYPE of reset_generator : label is "design_1_proc_sys_reset_0_0,proc_sys_reset,{}";
  attribute downgradeipidentifiedwarnings of reset_generator : label is "yes";
  attribute x_core_info of reset_generator : label is "proc_sys_reset,Vivado 2017.4";
begin
  ADC_Dout_2Sample(15 downto 0) <= \^adc_dout_2sample\(15 downto 0);
  ADC_clk_for_PL(0) <= \^adc_clk_for_pl\(0);
ADC500MHz_Controller_v1_0_0: entity work.Histo1_ADC500MSPS_Controller_0_0_design_1_ADC500MHz_Controller_v1_0_0_0
     port map (
      ADC_Din(15 downto 0) => \^adc_dout_2sample\(15 downto 0),
      ADC_Dout(31 downto 0) => ADC_Dout_for_PL(31 downto 0),
      ADC_clk => \^adc_clk_for_pl\(0),
      ADC_reset => reset_generator_peripheral_aresetn_0,
      Ctrl_reg0_in(8 downto 0) => Ctrl_reg0_in(8 downto 0),
      Ctrl_reg1_out(1 downto 0) => Ctrl_reg1_out(1 downto 0),
      data_valid => ADC_Dout_valid,
      from_adc_calrun_fmc => from_adc_calrun_fmc,
      from_adc_or => DS_ADC_OR_Buffer_IBUF_OUT,
      to_adc_cal_fmc => to_adc_cal_fmc,
      to_adc_caldly_nscs_fmc => to_adc_caldly_nscs_fmc,
      to_adc_dclk_rst_fmc => to_adc_dclk_rst_fmc,
      to_adc_fsr_ece_fmc => to_adc_fsr_ece_fmc,
      to_adc_led_0 => to_adc_led_0,
      to_adc_led_1 => to_adc_led_1,
      to_adc_outedge_ddr_sdata_fmc => to_adc_outedge_ddr_sdata_fmc,
      to_adc_outv_slck_fmc => to_adc_outv_slck_fmc,
      to_adc_pd_fmc => to_adc_pd_fmc
    );
BUFG_clk: entity work.Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_2_0
     port map (
      BUFG_I(0) => util_ds_buf_1_IBUF_OUT1,
      BUFG_O(0) => \^adc_clk_for_pl\(0)
    );
DS_ADC_OR_Buffer: entity work.Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_1
     port map (
      IBUF_DS_N(0) => from_ADC_OR_N(0),
      IBUF_DS_P(0) => from_ADC_OR_P(0),
      IBUF_OUT(0) => DS_ADC_OR_Buffer_IBUF_OUT
    );
DS_Data_Buffer: entity work.Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_3_0
     port map (
      IBUF_DS_N(15 downto 0) => Data_from_ADC_N(15 downto 0),
      IBUF_DS_P(15 downto 0) => Data_from_ADC_P(15 downto 0),
      IBUF_OUT(15 downto 0) => \^adc_dout_2sample\(15 downto 0)
    );
DS_clk_Buffer: entity work.Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_1_0
     port map (
      IBUF_DS_N(0) => clk_from_ADC_N(0),
      IBUF_DS_P(0) => clk_from_ADC_P(0),
      IBUF_OUT(0) => util_ds_buf_1_IBUF_OUT1
    );
SD_clk_Buffer: entity work.Histo1_ADC500MSPS_Controller_0_0_design_1_util_ds_buf_0_0
     port map (
      OBUF_DS_N(0) => clk_to_ADC_N(0),
      OBUF_DS_P(0) => clk_to_ADC_P(0),
      OBUF_IN(0) => clk_wiz_0_clk_out1
    );
clk_wiz_for_ADC: entity work.Histo1_ADC500MSPS_Controller_0_0_design_1_clk_wiz_0_0
     port map (
      clk_in1 => clock,
      clk_out1 => clk_wiz_0_clk_out1,
      resetn => reset_generator_peripheral_aresetn_0
    );
reset_generator: entity work.Histo1_ADC500MSPS_Controller_0_0_design_1_proc_sys_reset_0_0
     port map (
      aux_reset_in => '1',
      bus_struct_reset(0) => NLW_reset_generator_bus_struct_reset_UNCONNECTED(0),
      dcm_locked => '1',
      ext_reset_in => reset,
      interconnect_aresetn(0) => NLW_reset_generator_interconnect_aresetn_UNCONNECTED(0),
      mb_debug_sys_rst => '0',
      mb_reset => NLW_reset_generator_mb_reset_UNCONNECTED,
      peripheral_aresetn(0) => reset_generator_peripheral_aresetn_0,
      peripheral_reset(0) => NLW_reset_generator_peripheral_reset_UNCONNECTED(0),
      slowest_sync_clk => \^adc_clk_for_pl\(0)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_ADC500MSPS_Controller_0_0 is
  port (
    ADC_Dout_2Sample : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ADC_Dout_for_PL : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ADC_Dout_valid : out STD_LOGIC;
    ADC_clk_for_PL : out STD_LOGIC_VECTOR ( 0 to 0 );
    Ctrl_reg0_in : in STD_LOGIC_VECTOR ( 8 downto 0 );
    Ctrl_reg1_out : out STD_LOGIC_VECTOR ( 1 downto 0 );
    Data_from_ADC_N : in STD_LOGIC_VECTOR ( 15 downto 0 );
    Data_from_ADC_P : in STD_LOGIC_VECTOR ( 15 downto 0 );
    clk_from_ADC_N : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk_from_ADC_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk_to_ADC_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_to_ADC_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    clock : in STD_LOGIC;
    from_ADC_OR_N : in STD_LOGIC_VECTOR ( 0 to 0 );
    from_ADC_OR_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    from_adc_calrun_fmc : in STD_LOGIC;
    reset : in STD_LOGIC;
    to_adc_cal_fmc : out STD_LOGIC;
    to_adc_caldly_nscs_fmc : out STD_LOGIC;
    to_adc_dclk_rst_fmc : out STD_LOGIC;
    to_adc_fsr_ece_fmc : out STD_LOGIC;
    to_adc_led_0 : out STD_LOGIC;
    to_adc_led_1 : out STD_LOGIC;
    to_adc_outedge_ddr_sdata_fmc : out STD_LOGIC;
    to_adc_outv_slck_fmc : out STD_LOGIC;
    to_adc_pd_fmc : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of Histo1_ADC500MSPS_Controller_0_0 : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of Histo1_ADC500MSPS_Controller_0_0 : entity is "Histo1_ADC500MSPS_Controller_0_0,design_1_adc,{}";
  attribute downgradeipidentifiedwarnings : string;
  attribute downgradeipidentifiedwarnings of Histo1_ADC500MSPS_Controller_0_0 : entity is "yes";
  attribute x_core_info : string;
  attribute x_core_info of Histo1_ADC500MSPS_Controller_0_0 : entity is "design_1_adc,Vivado 2017.4";
end Histo1_ADC500MSPS_Controller_0_0;

architecture STRUCTURE of Histo1_ADC500MSPS_Controller_0_0 is
  attribute x_interface_info : string;
  attribute x_interface_info of clock : signal is "xilinx.com:signal:clock:1.0 CLK.CLOCK CLK";
  attribute x_interface_parameter : string;
  attribute x_interface_parameter of clock : signal is "XIL_INTERFACENAME CLK.CLOCK, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN Histo1_clock_0";
  attribute x_interface_info of reset : signal is "xilinx.com:signal:reset:1.0 RST.RESET RST";
  attribute x_interface_parameter of reset : signal is "XIL_INTERFACENAME RST.RESET, POLARITY ACTIVE_LOW";
begin
U0: entity work.Histo1_ADC500MSPS_Controller_0_0_design_1_adc
     port map (
      ADC_Dout_2Sample(15 downto 0) => ADC_Dout_2Sample(15 downto 0),
      ADC_Dout_for_PL(31 downto 0) => ADC_Dout_for_PL(31 downto 0),
      ADC_Dout_valid => ADC_Dout_valid,
      ADC_clk_for_PL(0) => ADC_clk_for_PL(0),
      Ctrl_reg0_in(8 downto 0) => Ctrl_reg0_in(8 downto 0),
      Ctrl_reg1_out(1 downto 0) => Ctrl_reg1_out(1 downto 0),
      Data_from_ADC_N(15 downto 0) => Data_from_ADC_N(15 downto 0),
      Data_from_ADC_P(15 downto 0) => Data_from_ADC_P(15 downto 0),
      clk_from_ADC_N(0) => clk_from_ADC_N(0),
      clk_from_ADC_P(0) => clk_from_ADC_P(0),
      clk_to_ADC_N(0) => clk_to_ADC_N(0),
      clk_to_ADC_P(0) => clk_to_ADC_P(0),
      clock => clock,
      from_ADC_OR_N(0) => from_ADC_OR_N(0),
      from_ADC_OR_P(0) => from_ADC_OR_P(0),
      from_adc_calrun_fmc => from_adc_calrun_fmc,
      reset => reset,
      to_adc_cal_fmc => to_adc_cal_fmc,
      to_adc_caldly_nscs_fmc => to_adc_caldly_nscs_fmc,
      to_adc_dclk_rst_fmc => to_adc_dclk_rst_fmc,
      to_adc_fsr_ece_fmc => to_adc_fsr_ece_fmc,
      to_adc_led_0 => to_adc_led_0,
      to_adc_led_1 => to_adc_led_1,
      to_adc_outedge_ddr_sdata_fmc => to_adc_outedge_ddr_sdata_fmc,
      to_adc_outv_slck_fmc => to_adc_outv_slck_fmc,
      to_adc_pd_fmc => to_adc_pd_fmc
    );
end STRUCTURE;
