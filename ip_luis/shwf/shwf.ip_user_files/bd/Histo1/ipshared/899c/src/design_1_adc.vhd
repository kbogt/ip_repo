--Copyright 1986-2016 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2016.1 (win64) Build 1538259 Fri Apr  8 15:45:27 MDT 2016
--Date        : Wed Nov 08 13:13:34 2017
--Host        : HP6-MLAB-10 running 64-bit Service Pack 1  (build 7601)
--Command     : generate_target design_1.bd
--Design      : design_1
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_adc is
  port (
    ADC_Dout_2Sample : out STD_LOGIC_VECTOR ( 15 downto 0 );
    ADC_Dout_for_PL : out STD_LOGIC_VECTOR ( 31 downto 0 );
    ADC_Dout_valid : out STD_LOGIC;
    ADC_clk_for_PL : out STD_LOGIC_VECTOR ( 0 to 0 );
    Ctrl_reg0_in : in STD_LOGIC_VECTOR ( 8 downto 0 );
    Ctrl_reg1_out : out STD_LOGIC_VECTOR ( 1 downto 0 );
    Data_from_ADC_N : in STD_LOGIC_VECTOR ( 15 downto 0 );
    Data_from_ADC_P : in STD_LOGIC_VECTOR ( 15 downto 0 );
    clk_from_ADC_N : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk_from_ADC_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk_to_ADC_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_to_ADC_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    clock : in STD_LOGIC;
    from_ADC_OR_N : in STD_LOGIC_VECTOR ( 0 to 0 );
    from_ADC_OR_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    from_adc_calrun_fmc : in STD_LOGIC;
    reset : in STD_LOGIC;
    to_adc_cal_fmc : out STD_LOGIC;
    to_adc_caldly_nscs_fmc : out STD_LOGIC;
    to_adc_dclk_rst_fmc : out STD_LOGIC;
    to_adc_fsr_ece_fmc : out STD_LOGIC;
    to_adc_led_0 : out STD_LOGIC;
    to_adc_led_1 : out STD_LOGIC;
    to_adc_outedge_ddr_sdata_fmc : out STD_LOGIC;
    to_adc_outv_slck_fmc : out STD_LOGIC;
    to_adc_pd_fmc : out STD_LOGIC
  );
  attribute CORE_GENERATION_INFO : string;
  attribute CORE_GENERATION_INFO of design_1_adc : entity is "design_1_adc,IP_Integrator,{x_ipVendor=xilinx.com,x_ipLibrary=BlockDiagram,x_ipName=design_1_adc,x_ipVersion=1.00.a,x_ipLanguage=VHDL,numBlks=8,numReposBlks=8,numNonXlnxBlks=1,numHierBlks=0,maxHierDepth=0,numSysgenBlks=0,numHlsBlks=0,numHdlrefBlks=0,numPkgbdBlks=0,bdsource=USER,da_board_cnt=2,synth_mode=Global}";
  attribute HW_HANDOFF : string;
  attribute HW_HANDOFF of design_1_adc : entity is "design_1_adc.hwdef";
end design_1_adc;

architecture STRUCTURE of design_1_adc is
  component design_1_ADC500MHz_Controller_v1_0_0_0 is
  port (
    ADC_clk : in STD_LOGIC;
    ADC_reset : in STD_LOGIC;
    ADC_Din : in STD_LOGIC_VECTOR ( 15 downto 0 );
    ADC_Dout : out STD_LOGIC_VECTOR ( 31 downto 0 );
    data_valid : out STD_LOGIC;
    Ctrl_reg0_in : in STD_LOGIC_VECTOR ( 8 downto 0 );
    Ctrl_reg1_out : out STD_LOGIC_VECTOR ( 1 downto 0 );
    to_adc_cal_fmc : out STD_LOGIC;
    to_adc_caldly_nscs_fmc : out STD_LOGIC;
    to_adc_fsr_ece_fmc : out STD_LOGIC;
    to_adc_outv_slck_fmc : out STD_LOGIC;
    to_adc_outedge_ddr_sdata_fmc : out STD_LOGIC;
    to_adc_dclk_rst_fmc : out STD_LOGIC;
    to_adc_pd_fmc : out STD_LOGIC;
    to_adc_led_0 : out STD_LOGIC;
    to_adc_led_1 : out STD_LOGIC;
    from_adc_calrun_fmc : in STD_LOGIC;
    from_adc_or : in STD_LOGIC
  );
  end component design_1_ADC500MHz_Controller_v1_0_0_0;
  component design_1_clk_wiz_0_0 is
  port (
    resetn : in STD_LOGIC;
    clk_in1 : in STD_LOGIC;
    clk_out1 : out STD_LOGIC
  );
  end component design_1_clk_wiz_0_0;
  component design_1_util_ds_buf_0_0 is
  port (
    OBUF_IN : in STD_LOGIC_VECTOR ( 0 to 0 );
    OBUF_DS_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    OBUF_DS_N : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_util_ds_buf_0_0;
  component design_1_util_ds_buf_1_0 is
  port (
    IBUF_DS_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    IBUF_DS_N : in STD_LOGIC_VECTOR ( 0 to 0 );
    IBUF_OUT : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_util_ds_buf_1_0;
  component design_1_util_ds_buf_2_0 is
  port (
    BUFG_I : in STD_LOGIC_VECTOR ( 0 to 0 );
    BUFG_O : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_util_ds_buf_2_0;
  component design_1_util_ds_buf_3_0 is
  port (
    IBUF_DS_P : in STD_LOGIC_VECTOR ( 15 downto 0 );
    IBUF_DS_N : in STD_LOGIC_VECTOR ( 15 downto 0 );
    IBUF_OUT : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );
  end component design_1_util_ds_buf_3_0;
  component design_1_proc_sys_reset_0_0 is
  port (
    slowest_sync_clk : in STD_LOGIC;
    ext_reset_in : in STD_LOGIC;
    aux_reset_in : in STD_LOGIC;
    mb_debug_sys_rst : in STD_LOGIC;
    dcm_locked : in STD_LOGIC;
    mb_reset : out STD_LOGIC;
    bus_struct_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_reset : out STD_LOGIC_VECTOR ( 0 to 0 );
    interconnect_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 );
    peripheral_aresetn : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_proc_sys_reset_0_0;
  component design_1_util_ds_buf_0_1 is
  port (
    IBUF_DS_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    IBUF_DS_N : in STD_LOGIC_VECTOR ( 0 to 0 );
    IBUF_OUT : out STD_LOGIC_VECTOR ( 0 to 0 )
  );
  end component design_1_util_ds_buf_0_1;
  signal ADC500MHz_Controller_v1_0_0_ADC_Dout : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal ADC500MHz_Controller_v1_0_0_Ctrl_reg1_out : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal ADC500MHz_Controller_v1_0_0_data_valid : STD_LOGIC;
  signal ADC500MHz_Controller_v1_0_0_to_adc_cal_fmc : STD_LOGIC;
  signal ADC500MHz_Controller_v1_0_0_to_adc_caldly_nscs_fmc : STD_LOGIC;
  signal ADC500MHz_Controller_v1_0_0_to_adc_dclk_rst_fmc : STD_LOGIC;
  signal ADC500MHz_Controller_v1_0_0_to_adc_fsr_ece_fmc : STD_LOGIC;
  signal ADC500MHz_Controller_v1_0_0_to_adc_led_0 : STD_LOGIC;
  signal ADC500MHz_Controller_v1_0_0_to_adc_led_1 : STD_LOGIC;
  signal ADC500MHz_Controller_v1_0_0_to_adc_outedge_ddr_sdata_fmc : STD_LOGIC;
  signal ADC500MHz_Controller_v1_0_0_to_adc_outv_slck_fmc : STD_LOGIC;
  signal ADC500MHz_Controller_v1_0_0_to_adc_pd_fmc : STD_LOGIC;
  signal Ctrl_reg0_in_1 : STD_LOGIC_VECTOR ( 8 downto 0 );
  signal DS_ADC_OR_Buffer_IBUF_OUT : STD_LOGIC_VECTOR ( 0 to 0 );
  signal DS_Data_Buffer_IBUF_OUT : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal IBUF_DS_N_1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal IBUF_DS_N_2 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal IBUF_DS_N_3 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal IBUF_DS_P_1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal IBUF_DS_P_2 : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal IBUF_DS_P_3 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal clk_wiz_0_clk_out1 : STD_LOGIC;
  signal from_adc_calrun_fmc_1 : STD_LOGIC;
  signal reset_1 : STD_LOGIC;
  signal reset_generator_peripheral_aresetn : STD_LOGIC_VECTOR ( 0 to 0 );
  signal sys_clock_1 : STD_LOGIC;
  signal util_ds_buf_0_OBUF_DS_N : STD_LOGIC_VECTOR ( 0 to 0 );
  signal util_ds_buf_0_OBUF_DS_P : STD_LOGIC_VECTOR ( 0 to 0 );
  signal util_ds_buf_1_IBUF_OUT : STD_LOGIC_VECTOR ( 0 to 0 );
  signal util_ds_buf_1_IBUF_OUT1 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_reset_generator_mb_reset_UNCONNECTED : STD_LOGIC;
  signal NLW_reset_generator_bus_struct_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_reset_generator_interconnect_aresetn_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
  signal NLW_reset_generator_peripheral_reset_UNCONNECTED : STD_LOGIC_VECTOR ( 0 to 0 );
begin
  ADC_Dout_2Sample(15 downto 0) <= DS_Data_Buffer_IBUF_OUT(15 downto 0);
  ADC_Dout_for_PL(31 downto 0) <= ADC500MHz_Controller_v1_0_0_ADC_Dout(31 downto 0);
  ADC_Dout_valid <= ADC500MHz_Controller_v1_0_0_data_valid;
  ADC_clk_for_PL(0) <= util_ds_buf_1_IBUF_OUT(0);
  Ctrl_reg0_in_1(8 downto 0) <= Ctrl_reg0_in(8 downto 0);
  Ctrl_reg1_out(1 downto 0) <= ADC500MHz_Controller_v1_0_0_Ctrl_reg1_out(1 downto 0);
  IBUF_DS_N_1(0) <= clk_from_ADC_N(0);
  IBUF_DS_N_2(15 downto 0) <= Data_from_ADC_N(15 downto 0);
  IBUF_DS_N_3(0) <= from_ADC_OR_N(0);
  IBUF_DS_P_1(0) <= clk_from_ADC_P(0);
  IBUF_DS_P_2(15 downto 0) <= Data_from_ADC_P(15 downto 0);
  IBUF_DS_P_3(0) <= from_ADC_OR_P(0);
  clk_to_ADC_N(0) <= util_ds_buf_0_OBUF_DS_N(0);
  clk_to_ADC_P(0) <= util_ds_buf_0_OBUF_DS_P(0);
  from_adc_calrun_fmc_1 <= from_adc_calrun_fmc;
  reset_1 <= reset;
  sys_clock_1 <= clock;
  to_adc_cal_fmc <= ADC500MHz_Controller_v1_0_0_to_adc_cal_fmc;
  to_adc_caldly_nscs_fmc <= ADC500MHz_Controller_v1_0_0_to_adc_caldly_nscs_fmc;
  to_adc_dclk_rst_fmc <= ADC500MHz_Controller_v1_0_0_to_adc_dclk_rst_fmc;
  to_adc_fsr_ece_fmc <= ADC500MHz_Controller_v1_0_0_to_adc_fsr_ece_fmc;
  to_adc_led_0 <= ADC500MHz_Controller_v1_0_0_to_adc_led_0;
  to_adc_led_1 <= ADC500MHz_Controller_v1_0_0_to_adc_led_1;
  to_adc_outedge_ddr_sdata_fmc <= ADC500MHz_Controller_v1_0_0_to_adc_outedge_ddr_sdata_fmc;
  to_adc_outv_slck_fmc <= ADC500MHz_Controller_v1_0_0_to_adc_outv_slck_fmc;
  to_adc_pd_fmc <= ADC500MHz_Controller_v1_0_0_to_adc_pd_fmc;
ADC500MHz_Controller_v1_0_0: component design_1_ADC500MHz_Controller_v1_0_0_0
     port map (
      ADC_Din(15 downto 0) => DS_Data_Buffer_IBUF_OUT(15 downto 0),
      ADC_Dout(31 downto 0) => ADC500MHz_Controller_v1_0_0_ADC_Dout(31 downto 0),
      ADC_clk => util_ds_buf_1_IBUF_OUT(0),
      ADC_reset => reset_generator_peripheral_aresetn(0),
      Ctrl_reg0_in(8 downto 0) => Ctrl_reg0_in_1(8 downto 0),
      Ctrl_reg1_out(1 downto 0) => ADC500MHz_Controller_v1_0_0_Ctrl_reg1_out(1 downto 0),
      data_valid => ADC500MHz_Controller_v1_0_0_data_valid,
      from_adc_calrun_fmc => from_adc_calrun_fmc_1,
      from_adc_or => DS_ADC_OR_Buffer_IBUF_OUT(0),
      to_adc_cal_fmc => ADC500MHz_Controller_v1_0_0_to_adc_cal_fmc,
      to_adc_caldly_nscs_fmc => ADC500MHz_Controller_v1_0_0_to_adc_caldly_nscs_fmc,
      to_adc_dclk_rst_fmc => ADC500MHz_Controller_v1_0_0_to_adc_dclk_rst_fmc,
      to_adc_fsr_ece_fmc => ADC500MHz_Controller_v1_0_0_to_adc_fsr_ece_fmc,
      to_adc_led_0 => ADC500MHz_Controller_v1_0_0_to_adc_led_0,
      to_adc_led_1 => ADC500MHz_Controller_v1_0_0_to_adc_led_1,
      to_adc_outedge_ddr_sdata_fmc => ADC500MHz_Controller_v1_0_0_to_adc_outedge_ddr_sdata_fmc,
      to_adc_outv_slck_fmc => ADC500MHz_Controller_v1_0_0_to_adc_outv_slck_fmc,
      to_adc_pd_fmc => ADC500MHz_Controller_v1_0_0_to_adc_pd_fmc
    );
BUFG_clk: component design_1_util_ds_buf_2_0
     port map (
      BUFG_I(0) => util_ds_buf_1_IBUF_OUT1(0),
      BUFG_O(0) => util_ds_buf_1_IBUF_OUT(0)
    );
DS_ADC_OR_Buffer: component design_1_util_ds_buf_0_1
     port map (
      IBUF_DS_N(0) => IBUF_DS_N_3(0),
      IBUF_DS_P(0) => IBUF_DS_P_3(0),
      IBUF_OUT(0) => DS_ADC_OR_Buffer_IBUF_OUT(0)
    );
DS_Data_Buffer: component design_1_util_ds_buf_3_0
     port map (
      IBUF_DS_N(15 downto 0) => IBUF_DS_N_2(15 downto 0),
      IBUF_DS_P(15 downto 0) => IBUF_DS_P_2(15 downto 0),
      IBUF_OUT(15 downto 0) => DS_Data_Buffer_IBUF_OUT(15 downto 0)
    );
DS_clk_Buffer: component design_1_util_ds_buf_1_0
     port map (
      IBUF_DS_N(0) => IBUF_DS_N_1(0),
      IBUF_DS_P(0) => IBUF_DS_P_1(0),
      IBUF_OUT(0) => util_ds_buf_1_IBUF_OUT1(0)
    );
SD_clk_Buffer: component design_1_util_ds_buf_0_0
     port map (
      OBUF_DS_N(0) => util_ds_buf_0_OBUF_DS_N(0),
      OBUF_DS_P(0) => util_ds_buf_0_OBUF_DS_P(0),
      OBUF_IN(0) => clk_wiz_0_clk_out1
    );
clk_wiz_for_ADC: component design_1_clk_wiz_0_0
     port map (
      clk_in1 => sys_clock_1,
      clk_out1 => clk_wiz_0_clk_out1,
      resetn => reset_generator_peripheral_aresetn(0)
    );
reset_generator: component design_1_proc_sys_reset_0_0
     port map (
      aux_reset_in => '1',
      bus_struct_reset(0) => NLW_reset_generator_bus_struct_reset_UNCONNECTED(0),
      dcm_locked => '1',
      ext_reset_in => reset_1,
      interconnect_aresetn(0) => NLW_reset_generator_interconnect_aresetn_UNCONNECTED(0),
      mb_debug_sys_rst => '0',
      mb_reset => NLW_reset_generator_mb_reset_UNCONNECTED,
      peripheral_aresetn(0) => reset_generator_peripheral_aresetn(0),
      peripheral_reset(0) => NLW_reset_generator_peripheral_reset_UNCONNECTED(0),
      slowest_sync_clk => util_ds_buf_1_IBUF_OUT(0)
    );
end STRUCTURE;
