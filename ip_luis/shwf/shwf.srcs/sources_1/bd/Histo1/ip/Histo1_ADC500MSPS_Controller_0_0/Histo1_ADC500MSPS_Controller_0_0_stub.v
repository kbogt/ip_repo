// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Wed Feb  7 13:10:30 2018
// Host        : HP6-MLAB-9 running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode synth_stub
//               c:/Users/infolab/Desktop/my_verilog/histo_w_comblock/project_1/project_1.srcs/sources_1/bd/Histo1/ip/Histo1_ADC500MSPS_Controller_0_0/Histo1_ADC500MSPS_Controller_0_0_stub.v
// Design      : Histo1_ADC500MSPS_Controller_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "design_1_adc,Vivado 2017.4" *)
module Histo1_ADC500MSPS_Controller_0_0(ADC_Dout_2Sample, ADC_Dout_for_PL, 
  ADC_Dout_valid, ADC_clk_for_PL, Ctrl_reg0_in, Ctrl_reg1_out, Data_from_ADC_N, 
  Data_from_ADC_P, clk_from_ADC_N, clk_from_ADC_P, clk_to_ADC_N, clk_to_ADC_P, clock, 
  from_ADC_OR_N, from_ADC_OR_P, from_adc_calrun_fmc, reset, to_adc_cal_fmc, 
  to_adc_caldly_nscs_fmc, to_adc_dclk_rst_fmc, to_adc_fsr_ece_fmc, to_adc_led_0, 
  to_adc_led_1, to_adc_outedge_ddr_sdata_fmc, to_adc_outv_slck_fmc, to_adc_pd_fmc)
/* synthesis syn_black_box black_box_pad_pin="ADC_Dout_2Sample[15:0],ADC_Dout_for_PL[31:0],ADC_Dout_valid,ADC_clk_for_PL[0:0],Ctrl_reg0_in[8:0],Ctrl_reg1_out[1:0],Data_from_ADC_N[15:0],Data_from_ADC_P[15:0],clk_from_ADC_N[0:0],clk_from_ADC_P[0:0],clk_to_ADC_N[0:0],clk_to_ADC_P[0:0],clock,from_ADC_OR_N[0:0],from_ADC_OR_P[0:0],from_adc_calrun_fmc,reset,to_adc_cal_fmc,to_adc_caldly_nscs_fmc,to_adc_dclk_rst_fmc,to_adc_fsr_ece_fmc,to_adc_led_0,to_adc_led_1,to_adc_outedge_ddr_sdata_fmc,to_adc_outv_slck_fmc,to_adc_pd_fmc" */;
  output [15:0]ADC_Dout_2Sample;
  output [31:0]ADC_Dout_for_PL;
  output ADC_Dout_valid;
  output [0:0]ADC_clk_for_PL;
  input [8:0]Ctrl_reg0_in;
  output [1:0]Ctrl_reg1_out;
  input [15:0]Data_from_ADC_N;
  input [15:0]Data_from_ADC_P;
  input [0:0]clk_from_ADC_N;
  input [0:0]clk_from_ADC_P;
  output [0:0]clk_to_ADC_N;
  output [0:0]clk_to_ADC_P;
  input clock;
  input [0:0]from_ADC_OR_N;
  input [0:0]from_ADC_OR_P;
  input from_adc_calrun_fmc;
  input reset;
  output to_adc_cal_fmc;
  output to_adc_caldly_nscs_fmc;
  output to_adc_dclk_rst_fmc;
  output to_adc_fsr_ece_fmc;
  output to_adc_led_0;
  output to_adc_led_1;
  output to_adc_outedge_ddr_sdata_fmc;
  output to_adc_outv_slck_fmc;
  output to_adc_pd_fmc;
endmodule
