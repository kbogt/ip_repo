-- (c) Copyright 1995-2018 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: ictp.it:user:ADC500MHz_Controller_v1_0:1.0
-- IP Revision: 2

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY design_1_ADC500MHz_Controller_v1_0_0_0 IS
  PORT (
    ADC_clk : IN STD_LOGIC;
    ADC_reset : IN STD_LOGIC;
    ADC_Din : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ADC_Dout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    data_valid : OUT STD_LOGIC;
    Ctrl_reg0_in : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    Ctrl_reg1_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    to_adc_cal_fmc : OUT STD_LOGIC;
    to_adc_caldly_nscs_fmc : OUT STD_LOGIC;
    to_adc_fsr_ece_fmc : OUT STD_LOGIC;
    to_adc_outv_slck_fmc : OUT STD_LOGIC;
    to_adc_outedge_ddr_sdata_fmc : OUT STD_LOGIC;
    to_adc_dclk_rst_fmc : OUT STD_LOGIC;
    to_adc_pd_fmc : OUT STD_LOGIC;
    to_adc_led_0 : OUT STD_LOGIC;
    to_adc_led_1 : OUT STD_LOGIC;
    from_adc_calrun_fmc : IN STD_LOGIC;
    from_adc_or : IN STD_LOGIC
  );
END design_1_ADC500MHz_Controller_v1_0_0_0;

ARCHITECTURE design_1_ADC500MHz_Controller_v1_0_0_0_arch OF design_1_ADC500MHz_Controller_v1_0_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF design_1_ADC500MHz_Controller_v1_0_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT ADC500MHz_Controller_v1_0 IS
    GENERIC (
      din_width : INTEGER
    );
    PORT (
      ADC_clk : IN STD_LOGIC;
      ADC_reset : IN STD_LOGIC;
      ADC_Din : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      ADC_Dout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      data_valid : OUT STD_LOGIC;
      Ctrl_reg0_in : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
      Ctrl_reg1_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      to_adc_cal_fmc : OUT STD_LOGIC;
      to_adc_caldly_nscs_fmc : OUT STD_LOGIC;
      to_adc_fsr_ece_fmc : OUT STD_LOGIC;
      to_adc_outv_slck_fmc : OUT STD_LOGIC;
      to_adc_outedge_ddr_sdata_fmc : OUT STD_LOGIC;
      to_adc_dclk_rst_fmc : OUT STD_LOGIC;
      to_adc_pd_fmc : OUT STD_LOGIC;
      to_adc_led_0 : OUT STD_LOGIC;
      to_adc_led_1 : OUT STD_LOGIC;
      from_adc_calrun_fmc : IN STD_LOGIC;
      from_adc_or : IN STD_LOGIC
    );
  END COMPONENT ADC500MHz_Controller_v1_0;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF design_1_ADC500MHz_Controller_v1_0_0_0_arch: ARCHITECTURE IS "ADC500MHz_Controller_v1_0,Vivado 2017.4";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF design_1_ADC500MHz_Controller_v1_0_0_0_arch : ARCHITECTURE IS "design_1_ADC500MHz_Controller_v1_0_0_0,ADC500MHz_Controller_v1_0,{}";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF ADC_reset: SIGNAL IS "XIL_INTERFACENAME ADC_reset, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF ADC_reset: SIGNAL IS "xilinx.com:signal:reset:1.0 ADC_reset RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF ADC_clk: SIGNAL IS "XIL_INTERFACENAME ADC_clk, ASSOCIATED_RESET ADC_reset, FREQ_HZ 100000000, PHASE 0.000";
  ATTRIBUTE X_INTERFACE_INFO OF ADC_clk: SIGNAL IS "xilinx.com:signal:clock:1.0 ADC_clk CLK";
BEGIN
  U0 : ADC500MHz_Controller_v1_0
    GENERIC MAP (
      din_width => 16
    )
    PORT MAP (
      ADC_clk => ADC_clk,
      ADC_reset => ADC_reset,
      ADC_Din => ADC_Din,
      ADC_Dout => ADC_Dout,
      data_valid => data_valid,
      Ctrl_reg0_in => Ctrl_reg0_in,
      Ctrl_reg1_out => Ctrl_reg1_out,
      to_adc_cal_fmc => to_adc_cal_fmc,
      to_adc_caldly_nscs_fmc => to_adc_caldly_nscs_fmc,
      to_adc_fsr_ece_fmc => to_adc_fsr_ece_fmc,
      to_adc_outv_slck_fmc => to_adc_outv_slck_fmc,
      to_adc_outedge_ddr_sdata_fmc => to_adc_outedge_ddr_sdata_fmc,
      to_adc_dclk_rst_fmc => to_adc_dclk_rst_fmc,
      to_adc_pd_fmc => to_adc_pd_fmc,
      to_adc_led_0 => to_adc_led_0,
      to_adc_led_1 => to_adc_led_1,
      from_adc_calrun_fmc => from_adc_calrun_fmc,
      from_adc_or => from_adc_or
    );
END design_1_ADC500MHz_Controller_v1_0_0_0_arch;
