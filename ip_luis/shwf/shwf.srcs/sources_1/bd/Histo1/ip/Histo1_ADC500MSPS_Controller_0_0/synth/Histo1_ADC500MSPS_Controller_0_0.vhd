-- (c) Copyright 1995-2018 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES.
-- 
-- DO NOT MODIFY THIS FILE.

-- IP VLNV: ictp.it:user:ADC500MSPS_Controller:1.0
-- IP Revision: 10

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY Histo1_ADC500MSPS_Controller_0_0 IS
  PORT (
    ADC_Dout_2Sample : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    ADC_Dout_for_PL : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    ADC_Dout_valid : OUT STD_LOGIC;
    ADC_clk_for_PL : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    Ctrl_reg0_in : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
    Ctrl_reg1_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    Data_from_ADC_N : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    Data_from_ADC_P : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    clk_from_ADC_N : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    clk_from_ADC_P : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    clk_to_ADC_N : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    clk_to_ADC_P : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    clock : IN STD_LOGIC;
    from_ADC_OR_N : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    from_ADC_OR_P : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    from_adc_calrun_fmc : IN STD_LOGIC;
    reset : IN STD_LOGIC;
    to_adc_cal_fmc : OUT STD_LOGIC;
    to_adc_caldly_nscs_fmc : OUT STD_LOGIC;
    to_adc_dclk_rst_fmc : OUT STD_LOGIC;
    to_adc_fsr_ece_fmc : OUT STD_LOGIC;
    to_adc_led_0 : OUT STD_LOGIC;
    to_adc_led_1 : OUT STD_LOGIC;
    to_adc_outedge_ddr_sdata_fmc : OUT STD_LOGIC;
    to_adc_outv_slck_fmc : OUT STD_LOGIC;
    to_adc_pd_fmc : OUT STD_LOGIC
  );
END Histo1_ADC500MSPS_Controller_0_0;

ARCHITECTURE Histo1_ADC500MSPS_Controller_0_0_arch OF Histo1_ADC500MSPS_Controller_0_0 IS
  ATTRIBUTE DowngradeIPIdentifiedWarnings : STRING;
  ATTRIBUTE DowngradeIPIdentifiedWarnings OF Histo1_ADC500MSPS_Controller_0_0_arch: ARCHITECTURE IS "yes";
  COMPONENT design_1_adc IS
    PORT (
      ADC_Dout_2Sample : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
      ADC_Dout_for_PL : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      ADC_Dout_valid : OUT STD_LOGIC;
      ADC_clk_for_PL : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      Ctrl_reg0_in : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
      Ctrl_reg1_out : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
      Data_from_ADC_N : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      Data_from_ADC_P : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
      clk_from_ADC_N : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      clk_from_ADC_P : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      clk_to_ADC_N : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      clk_to_ADC_P : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
      clock : IN STD_LOGIC;
      from_ADC_OR_N : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      from_ADC_OR_P : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
      from_adc_calrun_fmc : IN STD_LOGIC;
      reset : IN STD_LOGIC;
      to_adc_cal_fmc : OUT STD_LOGIC;
      to_adc_caldly_nscs_fmc : OUT STD_LOGIC;
      to_adc_dclk_rst_fmc : OUT STD_LOGIC;
      to_adc_fsr_ece_fmc : OUT STD_LOGIC;
      to_adc_led_0 : OUT STD_LOGIC;
      to_adc_led_1 : OUT STD_LOGIC;
      to_adc_outedge_ddr_sdata_fmc : OUT STD_LOGIC;
      to_adc_outv_slck_fmc : OUT STD_LOGIC;
      to_adc_pd_fmc : OUT STD_LOGIC
    );
  END COMPONENT design_1_adc;
  ATTRIBUTE X_CORE_INFO : STRING;
  ATTRIBUTE X_CORE_INFO OF Histo1_ADC500MSPS_Controller_0_0_arch: ARCHITECTURE IS "design_1_adc,Vivado 2017.4";
  ATTRIBUTE CHECK_LICENSE_TYPE : STRING;
  ATTRIBUTE CHECK_LICENSE_TYPE OF Histo1_ADC500MSPS_Controller_0_0_arch : ARCHITECTURE IS "Histo1_ADC500MSPS_Controller_0_0,design_1_adc,{}";
  ATTRIBUTE X_INTERFACE_INFO : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER : STRING;
  ATTRIBUTE X_INTERFACE_PARAMETER OF reset: SIGNAL IS "XIL_INTERFACENAME RST.RESET, POLARITY ACTIVE_LOW";
  ATTRIBUTE X_INTERFACE_INFO OF reset: SIGNAL IS "xilinx.com:signal:reset:1.0 RST.RESET RST";
  ATTRIBUTE X_INTERFACE_PARAMETER OF clock: SIGNAL IS "XIL_INTERFACENAME CLK.CLOCK, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN Histo1_clock_0";
  ATTRIBUTE X_INTERFACE_INFO OF clock: SIGNAL IS "xilinx.com:signal:clock:1.0 CLK.CLOCK CLK";
BEGIN
  U0 : design_1_adc
    PORT MAP (
      ADC_Dout_2Sample => ADC_Dout_2Sample,
      ADC_Dout_for_PL => ADC_Dout_for_PL,
      ADC_Dout_valid => ADC_Dout_valid,
      ADC_clk_for_PL => ADC_clk_for_PL,
      Ctrl_reg0_in => Ctrl_reg0_in,
      Ctrl_reg1_out => Ctrl_reg1_out,
      Data_from_ADC_N => Data_from_ADC_N,
      Data_from_ADC_P => Data_from_ADC_P,
      clk_from_ADC_N => clk_from_ADC_N,
      clk_from_ADC_P => clk_from_ADC_P,
      clk_to_ADC_N => clk_to_ADC_N,
      clk_to_ADC_P => clk_to_ADC_P,
      clock => clock,
      from_ADC_OR_N => from_ADC_OR_N,
      from_ADC_OR_P => from_ADC_OR_P,
      from_adc_calrun_fmc => from_adc_calrun_fmc,
      reset => reset,
      to_adc_cal_fmc => to_adc_cal_fmc,
      to_adc_caldly_nscs_fmc => to_adc_caldly_nscs_fmc,
      to_adc_dclk_rst_fmc => to_adc_dclk_rst_fmc,
      to_adc_fsr_ece_fmc => to_adc_fsr_ece_fmc,
      to_adc_led_0 => to_adc_led_0,
      to_adc_led_1 => to_adc_led_1,
      to_adc_outedge_ddr_sdata_fmc => to_adc_outedge_ddr_sdata_fmc,
      to_adc_outv_slck_fmc => to_adc_outv_slck_fmc,
      to_adc_pd_fmc => to_adc_pd_fmc
    );
END Histo1_ADC500MSPS_Controller_0_0_arch;
