// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Wed Feb  7 13:10:30 2018
// Host        : HP6-MLAB-9 running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode funcsim
//               c:/Users/infolab/Desktop/my_verilog/histo_w_comblock/project_1/project_1.srcs/sources_1/bd/Histo1/ip/Histo1_ADC_Decimator_0_0/Histo1_ADC_Decimator_0_0_sim_netlist.v
// Design      : Histo1_ADC_Decimator_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "Histo1_ADC_Decimator_0_0,ADC_Decimator,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "ADC_Decimator,Vivado 2017.4" *) 
(* NotValidForBitStream *)
module Histo1_ADC_Decimator_0_0
   (clk_in,
    resetn,
    Din,
    N,
    clk_out,
    Dout,
    Decim_data_valid,
    D_sum);
  input clk_in;
  (* x_interface_info = "xilinx.com:signal:reset:1.0 reset RST" *) (* x_interface_parameter = "XIL_INTERFACENAME reset, POLARITY ACTIVE_LOW" *) input resetn;
  input [15:0]Din;
  input [15:0]N;
  output clk_out;
  output [15:0]Dout;
  output Decim_data_valid;
  output [15:0]D_sum;

  wire \<const0> ;
  wire [15:0]D_sum;
  wire Decim_data_valid;
  wire [15:0]Din;
  wire [15:0]Dout;
  wire [15:0]N;
  wire clk_in;
  wire resetn;

  assign clk_out = \<const0> ;
  GND GND
       (.G(\<const0> ));
  Histo1_ADC_Decimator_0_0_ADC_Decimator U0
       (.D_sum(D_sum),
        .Decim_data_valid(Decim_data_valid),
        .Din(Din),
        .Dout(Dout),
        .N(N[15:1]),
        .clk_in(clk_in),
        .resetn(resetn));
endmodule

(* ORIG_REF_NAME = "ADC_Decimator" *) 
module Histo1_ADC_Decimator_0_0_ADC_Decimator
   (Dout,
    D_sum,
    Decim_data_valid,
    resetn,
    clk_in,
    Din,
    N);
  output [15:0]Dout;
  output [15:0]D_sum;
  output Decim_data_valid;
  input resetn;
  input clk_in;
  input [15:0]Din;
  input [14:0]N;

  wire [15:0]D_sum;
  wire Decim_data_valid;
  wire [15:0]Din;
  wire [8:0]Din_sig;
  wire [15:0]Dout;
  wire [14:0]N;
  wire clk_in;
  wire p_0_in;
  wire resetn;

  Histo1_ADC_Decimator_0_0_Decimate_N Decimate_N_init
       (.D_sum(D_sum),
        .Decim_data_valid(Decim_data_valid),
        .Dout(Dout),
        .N(N),
        .Q(Din_sig),
        .SR(p_0_in),
        .clk_in(clk_in),
        .resetn(resetn));
  Histo1_ADC_Decimator_0_0_Decimate_by2 Decimate_by2_init
       (.Din(Din),
        .Q(Din_sig),
        .SR(p_0_in),
        .clk_in(clk_in));
endmodule

(* ORIG_REF_NAME = "Decimate_N" *) 
module Histo1_ADC_Decimator_0_0_Decimate_N
   (Decim_data_valid,
    Dout,
    SR,
    D_sum,
    clk_in,
    resetn,
    Q,
    N);
  output Decim_data_valid;
  output [15:0]Dout;
  output [0:0]SR;
  output [15:0]D_sum;
  input clk_in;
  input resetn;
  input [8:0]Q;
  input [14:0]N;

  wire [15:0]D_sum;
  wire \D_sum[15]_i_2_n_0 ;
  wire Decim_data_valid;
  wire Decim_data_valid_i_1_n_0;
  wire [15:0]Dout;
  wire [14:0]N;
  wire [8:0]Q;
  wire [0:0]SR;
  wire clk_in;
  wire \cnt[0]_i_1_n_0 ;
  wire \cnt[0]_i_3_n_0 ;
  wire [15:0]cnt_reg;
  wire \cnt_reg[0]_i_2_n_0 ;
  wire \cnt_reg[0]_i_2_n_1 ;
  wire \cnt_reg[0]_i_2_n_2 ;
  wire \cnt_reg[0]_i_2_n_3 ;
  wire \cnt_reg[0]_i_2_n_4 ;
  wire \cnt_reg[0]_i_2_n_5 ;
  wire \cnt_reg[0]_i_2_n_6 ;
  wire \cnt_reg[0]_i_2_n_7 ;
  wire \cnt_reg[12]_i_1_n_1 ;
  wire \cnt_reg[12]_i_1_n_2 ;
  wire \cnt_reg[12]_i_1_n_3 ;
  wire \cnt_reg[12]_i_1_n_4 ;
  wire \cnt_reg[12]_i_1_n_5 ;
  wire \cnt_reg[12]_i_1_n_6 ;
  wire \cnt_reg[12]_i_1_n_7 ;
  wire \cnt_reg[4]_i_1_n_0 ;
  wire \cnt_reg[4]_i_1_n_1 ;
  wire \cnt_reg[4]_i_1_n_2 ;
  wire \cnt_reg[4]_i_1_n_3 ;
  wire \cnt_reg[4]_i_1_n_4 ;
  wire \cnt_reg[4]_i_1_n_5 ;
  wire \cnt_reg[4]_i_1_n_6 ;
  wire \cnt_reg[4]_i_1_n_7 ;
  wire \cnt_reg[8]_i_1_n_0 ;
  wire \cnt_reg[8]_i_1_n_1 ;
  wire \cnt_reg[8]_i_1_n_2 ;
  wire \cnt_reg[8]_i_1_n_3 ;
  wire \cnt_reg[8]_i_1_n_4 ;
  wire \cnt_reg[8]_i_1_n_5 ;
  wire \cnt_reg[8]_i_1_n_6 ;
  wire \cnt_reg[8]_i_1_n_7 ;
  wire eqOp;
  wire eqOp_carry__0_i_1_n_0;
  wire eqOp_carry__0_i_2_n_0;
  wire eqOp_carry__0_n_2;
  wire eqOp_carry__0_n_3;
  wire eqOp_carry_i_1_n_0;
  wire eqOp_carry_i_2_n_0;
  wire eqOp_carry_i_3_n_0;
  wire eqOp_carry_i_4_n_0;
  wire eqOp_carry_n_0;
  wire eqOp_carry_n_1;
  wire eqOp_carry_n_2;
  wire eqOp_carry_n_3;
  wire [14:1]minusOp;
  wire minusOp_carry__0_i_1_n_0;
  wire minusOp_carry__0_i_2_n_0;
  wire minusOp_carry__0_i_3_n_0;
  wire minusOp_carry__0_i_4_n_0;
  wire minusOp_carry__0_n_0;
  wire minusOp_carry__0_n_1;
  wire minusOp_carry__0_n_2;
  wire minusOp_carry__0_n_3;
  wire minusOp_carry__1_i_1_n_0;
  wire minusOp_carry__1_i_2_n_0;
  wire minusOp_carry__1_i_3_n_0;
  wire minusOp_carry__1_i_4_n_0;
  wire minusOp_carry__1_n_0;
  wire minusOp_carry__1_n_1;
  wire minusOp_carry__1_n_2;
  wire minusOp_carry__1_n_3;
  wire minusOp_carry__2_i_1_n_0;
  wire minusOp_carry__2_i_2_n_0;
  wire minusOp_carry__2_n_1;
  wire minusOp_carry__2_n_3;
  wire minusOp_carry_i_1_n_0;
  wire minusOp_carry_i_2_n_0;
  wire minusOp_carry_i_3_n_0;
  wire minusOp_carry_i_4_n_0;
  wire minusOp_carry_n_0;
  wire minusOp_carry_n_1;
  wire minusOp_carry_n_2;
  wire minusOp_carry_n_3;
  wire resetn;
  wire \tmp_sum[11]_i_2_n_0 ;
  wire \tmp_sum[11]_i_3_n_0 ;
  wire \tmp_sum[11]_i_4_n_0 ;
  wire \tmp_sum[11]_i_5_n_0 ;
  wire \tmp_sum[11]_i_6_n_0 ;
  wire \tmp_sum[15]_i_10_n_0 ;
  wire \tmp_sum[15]_i_1_n_0 ;
  wire \tmp_sum[15]_i_3_n_0 ;
  wire \tmp_sum[15]_i_4_n_0 ;
  wire \tmp_sum[15]_i_5_n_0 ;
  wire \tmp_sum[15]_i_6_n_0 ;
  wire \tmp_sum[15]_i_7_n_0 ;
  wire \tmp_sum[15]_i_8_n_0 ;
  wire \tmp_sum[15]_i_9_n_0 ;
  wire \tmp_sum[3]_i_2_n_0 ;
  wire \tmp_sum[3]_i_3_n_0 ;
  wire \tmp_sum[3]_i_4_n_0 ;
  wire \tmp_sum[3]_i_5_n_0 ;
  wire \tmp_sum[3]_i_6_n_0 ;
  wire \tmp_sum[3]_i_7_n_0 ;
  wire \tmp_sum[3]_i_8_n_0 ;
  wire \tmp_sum[3]_i_9_n_0 ;
  wire \tmp_sum[7]_i_2_n_0 ;
  wire \tmp_sum[7]_i_3_n_0 ;
  wire \tmp_sum[7]_i_4_n_0 ;
  wire \tmp_sum[7]_i_5_n_0 ;
  wire \tmp_sum[7]_i_6_n_0 ;
  wire \tmp_sum[7]_i_7_n_0 ;
  wire \tmp_sum[7]_i_8_n_0 ;
  wire \tmp_sum[7]_i_9_n_0 ;
  wire \tmp_sum_reg[11]_i_1_n_0 ;
  wire \tmp_sum_reg[11]_i_1_n_1 ;
  wire \tmp_sum_reg[11]_i_1_n_2 ;
  wire \tmp_sum_reg[11]_i_1_n_3 ;
  wire \tmp_sum_reg[11]_i_1_n_4 ;
  wire \tmp_sum_reg[11]_i_1_n_5 ;
  wire \tmp_sum_reg[11]_i_1_n_6 ;
  wire \tmp_sum_reg[11]_i_1_n_7 ;
  wire \tmp_sum_reg[15]_i_2_n_1 ;
  wire \tmp_sum_reg[15]_i_2_n_2 ;
  wire \tmp_sum_reg[15]_i_2_n_3 ;
  wire \tmp_sum_reg[15]_i_2_n_4 ;
  wire \tmp_sum_reg[15]_i_2_n_5 ;
  wire \tmp_sum_reg[15]_i_2_n_6 ;
  wire \tmp_sum_reg[15]_i_2_n_7 ;
  wire \tmp_sum_reg[3]_i_1_n_0 ;
  wire \tmp_sum_reg[3]_i_1_n_1 ;
  wire \tmp_sum_reg[3]_i_1_n_2 ;
  wire \tmp_sum_reg[3]_i_1_n_3 ;
  wire \tmp_sum_reg[3]_i_1_n_4 ;
  wire \tmp_sum_reg[3]_i_1_n_5 ;
  wire \tmp_sum_reg[3]_i_1_n_6 ;
  wire \tmp_sum_reg[3]_i_1_n_7 ;
  wire \tmp_sum_reg[7]_i_1_n_0 ;
  wire \tmp_sum_reg[7]_i_1_n_1 ;
  wire \tmp_sum_reg[7]_i_1_n_2 ;
  wire \tmp_sum_reg[7]_i_1_n_3 ;
  wire \tmp_sum_reg[7]_i_1_n_4 ;
  wire \tmp_sum_reg[7]_i_1_n_5 ;
  wire \tmp_sum_reg[7]_i_1_n_6 ;
  wire \tmp_sum_reg[7]_i_1_n_7 ;
  wire [3:3]\NLW_cnt_reg[12]_i_1_CO_UNCONNECTED ;
  wire [3:0]NLW_eqOp_carry_O_UNCONNECTED;
  wire [3:2]NLW_eqOp_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_eqOp_carry__0_O_UNCONNECTED;
  wire [3:1]NLW_minusOp_carry__2_CO_UNCONNECTED;
  wire [3:2]NLW_minusOp_carry__2_O_UNCONNECTED;
  wire [3:3]\NLW_tmp_sum_reg[15]_i_2_CO_UNCONNECTED ;

  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \D_sum[15]_i_1 
       (.I0(\tmp_sum[15]_i_6_n_0 ),
        .I1(cnt_reg[14]),
        .I2(cnt_reg[15]),
        .I3(cnt_reg[13]),
        .I4(cnt_reg[12]),
        .I5(\D_sum[15]_i_2_n_0 ),
        .O(eqOp));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFEFFFF)) 
    \D_sum[15]_i_2 
       (.I0(cnt_reg[0]),
        .I1(cnt_reg[1]),
        .I2(cnt_reg[2]),
        .I3(cnt_reg[3]),
        .I4(\tmp_sum[15]_i_4_n_0 ),
        .O(\D_sum[15]_i_2_n_0 ));
  FDRE \D_sum_reg[0] 
       (.C(clk_in),
        .CE(eqOp),
        .D(Dout[0]),
        .Q(D_sum[0]),
        .R(SR));
  FDRE \D_sum_reg[10] 
       (.C(clk_in),
        .CE(eqOp),
        .D(Dout[10]),
        .Q(D_sum[10]),
        .R(SR));
  FDRE \D_sum_reg[11] 
       (.C(clk_in),
        .CE(eqOp),
        .D(Dout[11]),
        .Q(D_sum[11]),
        .R(SR));
  FDRE \D_sum_reg[12] 
       (.C(clk_in),
        .CE(eqOp),
        .D(Dout[12]),
        .Q(D_sum[12]),
        .R(SR));
  FDRE \D_sum_reg[13] 
       (.C(clk_in),
        .CE(eqOp),
        .D(Dout[13]),
        .Q(D_sum[13]),
        .R(SR));
  FDRE \D_sum_reg[14] 
       (.C(clk_in),
        .CE(eqOp),
        .D(Dout[14]),
        .Q(D_sum[14]),
        .R(SR));
  FDRE \D_sum_reg[15] 
       (.C(clk_in),
        .CE(eqOp),
        .D(Dout[15]),
        .Q(D_sum[15]),
        .R(SR));
  FDRE \D_sum_reg[1] 
       (.C(clk_in),
        .CE(eqOp),
        .D(Dout[1]),
        .Q(D_sum[1]),
        .R(SR));
  FDRE \D_sum_reg[2] 
       (.C(clk_in),
        .CE(eqOp),
        .D(Dout[2]),
        .Q(D_sum[2]),
        .R(SR));
  FDRE \D_sum_reg[3] 
       (.C(clk_in),
        .CE(eqOp),
        .D(Dout[3]),
        .Q(D_sum[3]),
        .R(SR));
  FDRE \D_sum_reg[4] 
       (.C(clk_in),
        .CE(eqOp),
        .D(Dout[4]),
        .Q(D_sum[4]),
        .R(SR));
  FDRE \D_sum_reg[5] 
       (.C(clk_in),
        .CE(eqOp),
        .D(Dout[5]),
        .Q(D_sum[5]),
        .R(SR));
  FDRE \D_sum_reg[6] 
       (.C(clk_in),
        .CE(eqOp),
        .D(Dout[6]),
        .Q(D_sum[6]),
        .R(SR));
  FDRE \D_sum_reg[7] 
       (.C(clk_in),
        .CE(eqOp),
        .D(Dout[7]),
        .Q(D_sum[7]),
        .R(SR));
  FDRE \D_sum_reg[8] 
       (.C(clk_in),
        .CE(eqOp),
        .D(Dout[8]),
        .Q(D_sum[8]),
        .R(SR));
  FDRE \D_sum_reg[9] 
       (.C(clk_in),
        .CE(eqOp),
        .D(Dout[9]),
        .Q(D_sum[9]),
        .R(SR));
  LUT6 #(
    .INIT(64'h4000FFFF40000000)) 
    Decim_data_valid_i_1
       (.I0(\tmp_sum[15]_i_6_n_0 ),
        .I1(\tmp_sum[15]_i_5_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_3_n_0 ),
        .I4(resetn),
        .I5(Decim_data_valid),
        .O(Decim_data_valid_i_1_n_0));
  FDRE Decim_data_valid_reg
       (.C(clk_in),
        .CE(1'b1),
        .D(Decim_data_valid_i_1_n_0),
        .Q(Decim_data_valid),
        .R(1'b0));
  LUT2 #(
    .INIT(4'hB)) 
    \cnt[0]_i_1 
       (.I0(eqOp_carry__0_n_2),
        .I1(resetn),
        .O(\cnt[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \cnt[0]_i_3 
       (.I0(cnt_reg[0]),
        .O(\cnt[0]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[0] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_7 ),
        .Q(cnt_reg[0]),
        .R(\cnt[0]_i_1_n_0 ));
  CARRY4 \cnt_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\cnt_reg[0]_i_2_n_0 ,\cnt_reg[0]_i_2_n_1 ,\cnt_reg[0]_i_2_n_2 ,\cnt_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\cnt_reg[0]_i_2_n_4 ,\cnt_reg[0]_i_2_n_5 ,\cnt_reg[0]_i_2_n_6 ,\cnt_reg[0]_i_2_n_7 }),
        .S({cnt_reg[3:1],\cnt[0]_i_3_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[10] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_5 ),
        .Q(cnt_reg[10]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[11] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_4 ),
        .Q(cnt_reg[11]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[12] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_7 ),
        .Q(cnt_reg[12]),
        .R(\cnt[0]_i_1_n_0 ));
  CARRY4 \cnt_reg[12]_i_1 
       (.CI(\cnt_reg[8]_i_1_n_0 ),
        .CO({\NLW_cnt_reg[12]_i_1_CO_UNCONNECTED [3],\cnt_reg[12]_i_1_n_1 ,\cnt_reg[12]_i_1_n_2 ,\cnt_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[12]_i_1_n_4 ,\cnt_reg[12]_i_1_n_5 ,\cnt_reg[12]_i_1_n_6 ,\cnt_reg[12]_i_1_n_7 }),
        .S(cnt_reg[15:12]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[13] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_6 ),
        .Q(cnt_reg[13]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[14] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_5 ),
        .Q(cnt_reg[14]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[15] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\cnt_reg[12]_i_1_n_4 ),
        .Q(cnt_reg[15]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[1] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_6 ),
        .Q(cnt_reg[1]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[2] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_5 ),
        .Q(cnt_reg[2]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[3] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\cnt_reg[0]_i_2_n_4 ),
        .Q(cnt_reg[3]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[4] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_7 ),
        .Q(cnt_reg[4]),
        .R(\cnt[0]_i_1_n_0 ));
  CARRY4 \cnt_reg[4]_i_1 
       (.CI(\cnt_reg[0]_i_2_n_0 ),
        .CO({\cnt_reg[4]_i_1_n_0 ,\cnt_reg[4]_i_1_n_1 ,\cnt_reg[4]_i_1_n_2 ,\cnt_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[4]_i_1_n_4 ,\cnt_reg[4]_i_1_n_5 ,\cnt_reg[4]_i_1_n_6 ,\cnt_reg[4]_i_1_n_7 }),
        .S(cnt_reg[7:4]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[5] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_6 ),
        .Q(cnt_reg[5]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[6] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_5 ),
        .Q(cnt_reg[6]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[7] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\cnt_reg[4]_i_1_n_4 ),
        .Q(cnt_reg[7]),
        .R(\cnt[0]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[8] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_7 ),
        .Q(cnt_reg[8]),
        .R(\cnt[0]_i_1_n_0 ));
  CARRY4 \cnt_reg[8]_i_1 
       (.CI(\cnt_reg[4]_i_1_n_0 ),
        .CO({\cnt_reg[8]_i_1_n_0 ,\cnt_reg[8]_i_1_n_1 ,\cnt_reg[8]_i_1_n_2 ,\cnt_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\cnt_reg[8]_i_1_n_4 ,\cnt_reg[8]_i_1_n_5 ,\cnt_reg[8]_i_1_n_6 ,\cnt_reg[8]_i_1_n_7 }),
        .S(cnt_reg[11:8]));
  FDRE #(
    .INIT(1'b0)) 
    \cnt_reg[9] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\cnt_reg[8]_i_1_n_6 ),
        .Q(cnt_reg[9]),
        .R(\cnt[0]_i_1_n_0 ));
  CARRY4 eqOp_carry
       (.CI(1'b0),
        .CO({eqOp_carry_n_0,eqOp_carry_n_1,eqOp_carry_n_2,eqOp_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_eqOp_carry_O_UNCONNECTED[3:0]),
        .S({eqOp_carry_i_1_n_0,eqOp_carry_i_2_n_0,eqOp_carry_i_3_n_0,eqOp_carry_i_4_n_0}));
  CARRY4 eqOp_carry__0
       (.CI(eqOp_carry_n_0),
        .CO({NLW_eqOp_carry__0_CO_UNCONNECTED[3:2],eqOp_carry__0_n_2,eqOp_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_eqOp_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,eqOp_carry__0_i_1_n_0,eqOp_carry__0_i_2_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    eqOp_carry__0_i_1
       (.I0(minusOp_carry__2_n_1),
        .I1(cnt_reg[15]),
        .O(eqOp_carry__0_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    eqOp_carry__0_i_2
       (.I0(cnt_reg[12]),
        .I1(minusOp[12]),
        .I2(minusOp[14]),
        .I3(cnt_reg[14]),
        .I4(minusOp[13]),
        .I5(cnt_reg[13]),
        .O(eqOp_carry__0_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    eqOp_carry_i_1
       (.I0(cnt_reg[9]),
        .I1(minusOp[9]),
        .I2(minusOp[11]),
        .I3(cnt_reg[11]),
        .I4(minusOp[10]),
        .I5(cnt_reg[10]),
        .O(eqOp_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    eqOp_carry_i_2
       (.I0(cnt_reg[6]),
        .I1(minusOp[6]),
        .I2(minusOp[8]),
        .I3(cnt_reg[8]),
        .I4(minusOp[7]),
        .I5(cnt_reg[7]),
        .O(eqOp_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    eqOp_carry_i_3
       (.I0(cnt_reg[3]),
        .I1(minusOp[3]),
        .I2(minusOp[5]),
        .I3(cnt_reg[5]),
        .I4(minusOp[4]),
        .I5(cnt_reg[4]),
        .O(eqOp_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h6006000000006006)) 
    eqOp_carry_i_4
       (.I0(cnt_reg[0]),
        .I1(N[0]),
        .I2(minusOp[2]),
        .I3(cnt_reg[2]),
        .I4(minusOp[1]),
        .I5(cnt_reg[1]),
        .O(eqOp_carry_i_4_n_0));
  CARRY4 minusOp_carry
       (.CI(1'b0),
        .CO({minusOp_carry_n_0,minusOp_carry_n_1,minusOp_carry_n_2,minusOp_carry_n_3}),
        .CYINIT(N[0]),
        .DI(N[4:1]),
        .O(minusOp[4:1]),
        .S({minusOp_carry_i_1_n_0,minusOp_carry_i_2_n_0,minusOp_carry_i_3_n_0,minusOp_carry_i_4_n_0}));
  CARRY4 minusOp_carry__0
       (.CI(minusOp_carry_n_0),
        .CO({minusOp_carry__0_n_0,minusOp_carry__0_n_1,minusOp_carry__0_n_2,minusOp_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI(N[8:5]),
        .O(minusOp[8:5]),
        .S({minusOp_carry__0_i_1_n_0,minusOp_carry__0_i_2_n_0,minusOp_carry__0_i_3_n_0,minusOp_carry__0_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    minusOp_carry__0_i_1
       (.I0(N[8]),
        .O(minusOp_carry__0_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    minusOp_carry__0_i_2
       (.I0(N[7]),
        .O(minusOp_carry__0_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    minusOp_carry__0_i_3
       (.I0(N[6]),
        .O(minusOp_carry__0_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    minusOp_carry__0_i_4
       (.I0(N[5]),
        .O(minusOp_carry__0_i_4_n_0));
  CARRY4 minusOp_carry__1
       (.CI(minusOp_carry__0_n_0),
        .CO({minusOp_carry__1_n_0,minusOp_carry__1_n_1,minusOp_carry__1_n_2,minusOp_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI(N[12:9]),
        .O(minusOp[12:9]),
        .S({minusOp_carry__1_i_1_n_0,minusOp_carry__1_i_2_n_0,minusOp_carry__1_i_3_n_0,minusOp_carry__1_i_4_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    minusOp_carry__1_i_1
       (.I0(N[12]),
        .O(minusOp_carry__1_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    minusOp_carry__1_i_2
       (.I0(N[11]),
        .O(minusOp_carry__1_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    minusOp_carry__1_i_3
       (.I0(N[10]),
        .O(minusOp_carry__1_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    minusOp_carry__1_i_4
       (.I0(N[9]),
        .O(minusOp_carry__1_i_4_n_0));
  CARRY4 minusOp_carry__2
       (.CI(minusOp_carry__1_n_0),
        .CO({NLW_minusOp_carry__2_CO_UNCONNECTED[3],minusOp_carry__2_n_1,NLW_minusOp_carry__2_CO_UNCONNECTED[1],minusOp_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,N[14:13]}),
        .O({NLW_minusOp_carry__2_O_UNCONNECTED[3:2],minusOp[14:13]}),
        .S({1'b0,1'b1,minusOp_carry__2_i_1_n_0,minusOp_carry__2_i_2_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    minusOp_carry__2_i_1
       (.I0(N[14]),
        .O(minusOp_carry__2_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    minusOp_carry__2_i_2
       (.I0(N[13]),
        .O(minusOp_carry__2_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    minusOp_carry_i_1
       (.I0(N[4]),
        .O(minusOp_carry_i_1_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    minusOp_carry_i_2
       (.I0(N[3]),
        .O(minusOp_carry_i_2_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    minusOp_carry_i_3
       (.I0(N[2]),
        .O(minusOp_carry_i_3_n_0));
  LUT1 #(
    .INIT(2'h1)) 
    minusOp_carry_i_4
       (.I0(N[1]),
        .O(minusOp_carry_i_4_n_0));
  LUT5 #(
    .INIT(32'hAAAA2AAA)) 
    \tmp_sum[11]_i_2 
       (.I0(Q[8]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .O(\tmp_sum[11]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA2AAA)) 
    \tmp_sum[11]_i_3 
       (.I0(Dout[11]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .O(\tmp_sum[11]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA2AAA)) 
    \tmp_sum[11]_i_4 
       (.I0(Dout[10]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .O(\tmp_sum[11]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA2AAA)) 
    \tmp_sum[11]_i_5 
       (.I0(Dout[9]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .O(\tmp_sum[11]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h5555D555AAAA2AAA)) 
    \tmp_sum[11]_i_6 
       (.I0(Dout[8]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .I5(Q[8]),
        .O(\tmp_sum[11]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'h0080FFFF)) 
    \tmp_sum[15]_i_1 
       (.I0(\tmp_sum[15]_i_3_n_0 ),
        .I1(\tmp_sum[15]_i_4_n_0 ),
        .I2(\tmp_sum[15]_i_5_n_0 ),
        .I3(\tmp_sum[15]_i_6_n_0 ),
        .I4(resetn),
        .O(\tmp_sum[15]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA2AAA)) 
    \tmp_sum[15]_i_10 
       (.I0(Dout[12]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .O(\tmp_sum[15]_i_10_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \tmp_sum[15]_i_3 
       (.I0(cnt_reg[3]),
        .I1(cnt_reg[2]),
        .I2(cnt_reg[1]),
        .I3(cnt_reg[0]),
        .O(\tmp_sum[15]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \tmp_sum[15]_i_4 
       (.I0(cnt_reg[7]),
        .I1(cnt_reg[6]),
        .I2(cnt_reg[5]),
        .I3(cnt_reg[4]),
        .O(\tmp_sum[15]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \tmp_sum[15]_i_5 
       (.I0(cnt_reg[14]),
        .I1(cnt_reg[15]),
        .I2(cnt_reg[13]),
        .I3(cnt_reg[12]),
        .O(\tmp_sum[15]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \tmp_sum[15]_i_6 
       (.I0(cnt_reg[9]),
        .I1(cnt_reg[8]),
        .I2(cnt_reg[11]),
        .I3(cnt_reg[10]),
        .O(\tmp_sum[15]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA2AAA)) 
    \tmp_sum[15]_i_7 
       (.I0(Dout[15]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .O(\tmp_sum[15]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA2AAA)) 
    \tmp_sum[15]_i_8 
       (.I0(Dout[14]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .O(\tmp_sum[15]_i_8_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA2AAA)) 
    \tmp_sum[15]_i_9 
       (.I0(Dout[13]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .O(\tmp_sum[15]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA2AAA)) 
    \tmp_sum[3]_i_2 
       (.I0(Q[3]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .O(\tmp_sum[3]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA2AAA)) 
    \tmp_sum[3]_i_3 
       (.I0(Q[2]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .O(\tmp_sum[3]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA2AAA)) 
    \tmp_sum[3]_i_4 
       (.I0(Q[1]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .O(\tmp_sum[3]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA2AAA)) 
    \tmp_sum[3]_i_5 
       (.I0(Q[0]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .O(\tmp_sum[3]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h5555D555AAAA2AAA)) 
    \tmp_sum[3]_i_6 
       (.I0(Dout[3]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .I5(Q[3]),
        .O(\tmp_sum[3]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h5555D555AAAA2AAA)) 
    \tmp_sum[3]_i_7 
       (.I0(Dout[2]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .I5(Q[2]),
        .O(\tmp_sum[3]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h5555D555AAAA2AAA)) 
    \tmp_sum[3]_i_8 
       (.I0(Dout[1]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .I5(Q[1]),
        .O(\tmp_sum[3]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h5555D555AAAA2AAA)) 
    \tmp_sum[3]_i_9 
       (.I0(Dout[0]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .I5(Q[0]),
        .O(\tmp_sum[3]_i_9_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA2AAA)) 
    \tmp_sum[7]_i_2 
       (.I0(Q[7]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .O(\tmp_sum[7]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA2AAA)) 
    \tmp_sum[7]_i_3 
       (.I0(Q[6]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .O(\tmp_sum[7]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA2AAA)) 
    \tmp_sum[7]_i_4 
       (.I0(Q[5]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .O(\tmp_sum[7]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hAAAA2AAA)) 
    \tmp_sum[7]_i_5 
       (.I0(Q[4]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .O(\tmp_sum[7]_i_5_n_0 ));
  LUT6 #(
    .INIT(64'h5555D555AAAA2AAA)) 
    \tmp_sum[7]_i_6 
       (.I0(Dout[7]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .I5(Q[7]),
        .O(\tmp_sum[7]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'h5555D555AAAA2AAA)) 
    \tmp_sum[7]_i_7 
       (.I0(Dout[6]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .I5(Q[6]),
        .O(\tmp_sum[7]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h5555D555AAAA2AAA)) 
    \tmp_sum[7]_i_8 
       (.I0(Dout[5]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .I5(Q[5]),
        .O(\tmp_sum[7]_i_8_n_0 ));
  LUT6 #(
    .INIT(64'h5555D555AAAA2AAA)) 
    \tmp_sum[7]_i_9 
       (.I0(Dout[4]),
        .I1(\tmp_sum[15]_i_3_n_0 ),
        .I2(\tmp_sum[15]_i_4_n_0 ),
        .I3(\tmp_sum[15]_i_5_n_0 ),
        .I4(\tmp_sum[15]_i_6_n_0 ),
        .I5(Q[4]),
        .O(\tmp_sum[7]_i_9_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \tmp_sum[8]_i_1 
       (.I0(resetn),
        .O(SR));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_sum_reg[0] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\tmp_sum_reg[3]_i_1_n_7 ),
        .Q(Dout[0]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_sum_reg[10] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\tmp_sum_reg[11]_i_1_n_5 ),
        .Q(Dout[10]),
        .R(\tmp_sum[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_sum_reg[11] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\tmp_sum_reg[11]_i_1_n_4 ),
        .Q(Dout[11]),
        .R(\tmp_sum[15]_i_1_n_0 ));
  CARRY4 \tmp_sum_reg[11]_i_1 
       (.CI(\tmp_sum_reg[7]_i_1_n_0 ),
        .CO({\tmp_sum_reg[11]_i_1_n_0 ,\tmp_sum_reg[11]_i_1_n_1 ,\tmp_sum_reg[11]_i_1_n_2 ,\tmp_sum_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,\tmp_sum[11]_i_2_n_0 }),
        .O({\tmp_sum_reg[11]_i_1_n_4 ,\tmp_sum_reg[11]_i_1_n_5 ,\tmp_sum_reg[11]_i_1_n_6 ,\tmp_sum_reg[11]_i_1_n_7 }),
        .S({\tmp_sum[11]_i_3_n_0 ,\tmp_sum[11]_i_4_n_0 ,\tmp_sum[11]_i_5_n_0 ,\tmp_sum[11]_i_6_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_sum_reg[12] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\tmp_sum_reg[15]_i_2_n_7 ),
        .Q(Dout[12]),
        .R(\tmp_sum[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_sum_reg[13] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\tmp_sum_reg[15]_i_2_n_6 ),
        .Q(Dout[13]),
        .R(\tmp_sum[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_sum_reg[14] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\tmp_sum_reg[15]_i_2_n_5 ),
        .Q(Dout[14]),
        .R(\tmp_sum[15]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_sum_reg[15] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\tmp_sum_reg[15]_i_2_n_4 ),
        .Q(Dout[15]),
        .R(\tmp_sum[15]_i_1_n_0 ));
  CARRY4 \tmp_sum_reg[15]_i_2 
       (.CI(\tmp_sum_reg[11]_i_1_n_0 ),
        .CO({\NLW_tmp_sum_reg[15]_i_2_CO_UNCONNECTED [3],\tmp_sum_reg[15]_i_2_n_1 ,\tmp_sum_reg[15]_i_2_n_2 ,\tmp_sum_reg[15]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\tmp_sum_reg[15]_i_2_n_4 ,\tmp_sum_reg[15]_i_2_n_5 ,\tmp_sum_reg[15]_i_2_n_6 ,\tmp_sum_reg[15]_i_2_n_7 }),
        .S({\tmp_sum[15]_i_7_n_0 ,\tmp_sum[15]_i_8_n_0 ,\tmp_sum[15]_i_9_n_0 ,\tmp_sum[15]_i_10_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_sum_reg[1] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\tmp_sum_reg[3]_i_1_n_6 ),
        .Q(Dout[1]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_sum_reg[2] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\tmp_sum_reg[3]_i_1_n_5 ),
        .Q(Dout[2]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_sum_reg[3] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\tmp_sum_reg[3]_i_1_n_4 ),
        .Q(Dout[3]),
        .R(SR));
  CARRY4 \tmp_sum_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\tmp_sum_reg[3]_i_1_n_0 ,\tmp_sum_reg[3]_i_1_n_1 ,\tmp_sum_reg[3]_i_1_n_2 ,\tmp_sum_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\tmp_sum[3]_i_2_n_0 ,\tmp_sum[3]_i_3_n_0 ,\tmp_sum[3]_i_4_n_0 ,\tmp_sum[3]_i_5_n_0 }),
        .O({\tmp_sum_reg[3]_i_1_n_4 ,\tmp_sum_reg[3]_i_1_n_5 ,\tmp_sum_reg[3]_i_1_n_6 ,\tmp_sum_reg[3]_i_1_n_7 }),
        .S({\tmp_sum[3]_i_6_n_0 ,\tmp_sum[3]_i_7_n_0 ,\tmp_sum[3]_i_8_n_0 ,\tmp_sum[3]_i_9_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_sum_reg[4] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\tmp_sum_reg[7]_i_1_n_7 ),
        .Q(Dout[4]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_sum_reg[5] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\tmp_sum_reg[7]_i_1_n_6 ),
        .Q(Dout[5]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_sum_reg[6] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\tmp_sum_reg[7]_i_1_n_5 ),
        .Q(Dout[6]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_sum_reg[7] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\tmp_sum_reg[7]_i_1_n_4 ),
        .Q(Dout[7]),
        .R(SR));
  CARRY4 \tmp_sum_reg[7]_i_1 
       (.CI(\tmp_sum_reg[3]_i_1_n_0 ),
        .CO({\tmp_sum_reg[7]_i_1_n_0 ,\tmp_sum_reg[7]_i_1_n_1 ,\tmp_sum_reg[7]_i_1_n_2 ,\tmp_sum_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({\tmp_sum[7]_i_2_n_0 ,\tmp_sum[7]_i_3_n_0 ,\tmp_sum[7]_i_4_n_0 ,\tmp_sum[7]_i_5_n_0 }),
        .O({\tmp_sum_reg[7]_i_1_n_4 ,\tmp_sum_reg[7]_i_1_n_5 ,\tmp_sum_reg[7]_i_1_n_6 ,\tmp_sum_reg[7]_i_1_n_7 }),
        .S({\tmp_sum[7]_i_6_n_0 ,\tmp_sum[7]_i_7_n_0 ,\tmp_sum[7]_i_8_n_0 ,\tmp_sum[7]_i_9_n_0 }));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_sum_reg[8] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\tmp_sum_reg[11]_i_1_n_7 ),
        .Q(Dout[8]),
        .R(SR));
  FDRE #(
    .INIT(1'b0)) 
    \tmp_sum_reg[9] 
       (.C(clk_in),
        .CE(1'b1),
        .D(\tmp_sum_reg[11]_i_1_n_6 ),
        .Q(Dout[9]),
        .R(\tmp_sum[15]_i_1_n_0 ));
endmodule

(* ORIG_REF_NAME = "Decimate_by2" *) 
module Histo1_ADC_Decimator_0_0_Decimate_by2
   (Q,
    Din,
    SR,
    clk_in);
  output [8:0]Q;
  input [15:0]Din;
  input [0:0]SR;
  input clk_in;

  wire [15:0]Din;
  wire [8:0]Q;
  wire [0:0]SR;
  wire clk_in;
  wire \dout[3]_i_2_n_0 ;
  wire \dout[3]_i_3_n_0 ;
  wire \dout[3]_i_4_n_0 ;
  wire \dout[3]_i_5_n_0 ;
  wire \dout[7]_i_2_n_0 ;
  wire \dout[7]_i_3_n_0 ;
  wire \dout[7]_i_4_n_0 ;
  wire \dout[7]_i_5_n_0 ;
  wire \dout_reg[3]_i_1_n_0 ;
  wire \dout_reg[3]_i_1_n_1 ;
  wire \dout_reg[3]_i_1_n_2 ;
  wire \dout_reg[3]_i_1_n_3 ;
  wire \dout_reg[7]_i_1_n_0 ;
  wire \dout_reg[7]_i_1_n_1 ;
  wire \dout_reg[7]_i_1_n_2 ;
  wire \dout_reg[7]_i_1_n_3 ;
  wire [8:0]plusOp;
  wire [3:1]\NLW_dout_reg[8]_i_1_CO_UNCONNECTED ;
  wire [3:0]\NLW_dout_reg[8]_i_1_O_UNCONNECTED ;

  LUT2 #(
    .INIT(4'h6)) 
    \dout[3]_i_2 
       (.I0(Din[11]),
        .I1(Din[3]),
        .O(\dout[3]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dout[3]_i_3 
       (.I0(Din[10]),
        .I1(Din[2]),
        .O(\dout[3]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dout[3]_i_4 
       (.I0(Din[9]),
        .I1(Din[1]),
        .O(\dout[3]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dout[3]_i_5 
       (.I0(Din[8]),
        .I1(Din[0]),
        .O(\dout[3]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dout[7]_i_2 
       (.I0(Din[15]),
        .I1(Din[7]),
        .O(\dout[7]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dout[7]_i_3 
       (.I0(Din[14]),
        .I1(Din[6]),
        .O(\dout[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dout[7]_i_4 
       (.I0(Din[13]),
        .I1(Din[5]),
        .O(\dout[7]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    \dout[7]_i_5 
       (.I0(Din[12]),
        .I1(Din[4]),
        .O(\dout[7]_i_5_n_0 ));
  FDRE \dout_reg[0] 
       (.C(clk_in),
        .CE(1'b1),
        .D(plusOp[0]),
        .Q(Q[0]),
        .R(SR));
  FDRE \dout_reg[1] 
       (.C(clk_in),
        .CE(1'b1),
        .D(plusOp[1]),
        .Q(Q[1]),
        .R(SR));
  FDRE \dout_reg[2] 
       (.C(clk_in),
        .CE(1'b1),
        .D(plusOp[2]),
        .Q(Q[2]),
        .R(SR));
  FDRE \dout_reg[3] 
       (.C(clk_in),
        .CE(1'b1),
        .D(plusOp[3]),
        .Q(Q[3]),
        .R(SR));
  CARRY4 \dout_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\dout_reg[3]_i_1_n_0 ,\dout_reg[3]_i_1_n_1 ,\dout_reg[3]_i_1_n_2 ,\dout_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(Din[11:8]),
        .O(plusOp[3:0]),
        .S({\dout[3]_i_2_n_0 ,\dout[3]_i_3_n_0 ,\dout[3]_i_4_n_0 ,\dout[3]_i_5_n_0 }));
  FDRE \dout_reg[4] 
       (.C(clk_in),
        .CE(1'b1),
        .D(plusOp[4]),
        .Q(Q[4]),
        .R(SR));
  FDRE \dout_reg[5] 
       (.C(clk_in),
        .CE(1'b1),
        .D(plusOp[5]),
        .Q(Q[5]),
        .R(SR));
  FDRE \dout_reg[6] 
       (.C(clk_in),
        .CE(1'b1),
        .D(plusOp[6]),
        .Q(Q[6]),
        .R(SR));
  FDRE \dout_reg[7] 
       (.C(clk_in),
        .CE(1'b1),
        .D(plusOp[7]),
        .Q(Q[7]),
        .R(SR));
  CARRY4 \dout_reg[7]_i_1 
       (.CI(\dout_reg[3]_i_1_n_0 ),
        .CO({\dout_reg[7]_i_1_n_0 ,\dout_reg[7]_i_1_n_1 ,\dout_reg[7]_i_1_n_2 ,\dout_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI(Din[15:12]),
        .O(plusOp[7:4]),
        .S({\dout[7]_i_2_n_0 ,\dout[7]_i_3_n_0 ,\dout[7]_i_4_n_0 ,\dout[7]_i_5_n_0 }));
  FDRE \dout_reg[8] 
       (.C(clk_in),
        .CE(1'b1),
        .D(plusOp[8]),
        .Q(Q[8]),
        .R(SR));
  CARRY4 \dout_reg[8]_i_1 
       (.CI(\dout_reg[7]_i_1_n_0 ),
        .CO({\NLW_dout_reg[8]_i_1_CO_UNCONNECTED [3:1],plusOp[8]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(\NLW_dout_reg[8]_i_1_O_UNCONNECTED [3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
