vlib questa_lib/work
vlib questa_lib/msim

vlib questa_lib/msim/xil_defaultlib
vlib questa_lib/msim/xpm
vlib questa_lib/msim/util_ds_buf_v2_01_a
vlib questa_lib/msim/lib_cdc_v1_0_2
vlib questa_lib/msim/proc_sys_reset_v5_0_12
vlib questa_lib/msim/xlslice_v1_0_1
vlib questa_lib/msim/xlconcat_v2_1_1
vlib questa_lib/msim/xlconstant_v1_1_3
vlib questa_lib/msim/axi_infrastructure_v1_1_0
vlib questa_lib/msim/smartconnect_v1_0
vlib questa_lib/msim/axi_protocol_checker_v2_0_1
vlib questa_lib/msim/axi_vip_v1_1_1
vlib questa_lib/msim/processing_system7_vip_v1_0_3
vlib questa_lib/msim/generic_baseblocks_v2_1_0
vlib questa_lib/msim/axi_register_slice_v2_1_15
vlib questa_lib/msim/fifo_generator_v13_2_1
vlib questa_lib/msim/axi_data_fifo_v2_1_14
vlib questa_lib/msim/axi_crossbar_v2_1_16
vlib questa_lib/msim/blk_mem_gen_v8_3_6
vlib questa_lib/msim/axi_bram_ctrl_v4_0_13
vlib questa_lib/msim/blk_mem_gen_v8_4_1
vlib questa_lib/msim/axi_lite_ipif_v3_0_4
vlib questa_lib/msim/lib_pkg_v1_0_2
vlib questa_lib/msim/lib_fifo_v1_0_10
vlib questa_lib/msim/axi_fifo_mm_s_v4_1_12
vlib questa_lib/msim/interrupt_control_v3_1_4
vlib questa_lib/msim/axi_gpio_v2_0_17
vlib questa_lib/msim/util_vector_logic_v2_0_1
vlib questa_lib/msim/axi_protocol_converter_v2_1_15

vmap xil_defaultlib questa_lib/msim/xil_defaultlib
vmap xpm questa_lib/msim/xpm
vmap util_ds_buf_v2_01_a questa_lib/msim/util_ds_buf_v2_01_a
vmap lib_cdc_v1_0_2 questa_lib/msim/lib_cdc_v1_0_2
vmap proc_sys_reset_v5_0_12 questa_lib/msim/proc_sys_reset_v5_0_12
vmap xlslice_v1_0_1 questa_lib/msim/xlslice_v1_0_1
vmap xlconcat_v2_1_1 questa_lib/msim/xlconcat_v2_1_1
vmap xlconstant_v1_1_3 questa_lib/msim/xlconstant_v1_1_3
vmap axi_infrastructure_v1_1_0 questa_lib/msim/axi_infrastructure_v1_1_0
vmap smartconnect_v1_0 questa_lib/msim/smartconnect_v1_0
vmap axi_protocol_checker_v2_0_1 questa_lib/msim/axi_protocol_checker_v2_0_1
vmap axi_vip_v1_1_1 questa_lib/msim/axi_vip_v1_1_1
vmap processing_system7_vip_v1_0_3 questa_lib/msim/processing_system7_vip_v1_0_3
vmap generic_baseblocks_v2_1_0 questa_lib/msim/generic_baseblocks_v2_1_0
vmap axi_register_slice_v2_1_15 questa_lib/msim/axi_register_slice_v2_1_15
vmap fifo_generator_v13_2_1 questa_lib/msim/fifo_generator_v13_2_1
vmap axi_data_fifo_v2_1_14 questa_lib/msim/axi_data_fifo_v2_1_14
vmap axi_crossbar_v2_1_16 questa_lib/msim/axi_crossbar_v2_1_16
vmap blk_mem_gen_v8_3_6 questa_lib/msim/blk_mem_gen_v8_3_6
vmap axi_bram_ctrl_v4_0_13 questa_lib/msim/axi_bram_ctrl_v4_0_13
vmap blk_mem_gen_v8_4_1 questa_lib/msim/blk_mem_gen_v8_4_1
vmap axi_lite_ipif_v3_0_4 questa_lib/msim/axi_lite_ipif_v3_0_4
vmap lib_pkg_v1_0_2 questa_lib/msim/lib_pkg_v1_0_2
vmap lib_fifo_v1_0_10 questa_lib/msim/lib_fifo_v1_0_10
vmap axi_fifo_mm_s_v4_1_12 questa_lib/msim/axi_fifo_mm_s_v4_1_12
vmap interrupt_control_v3_1_4 questa_lib/msim/interrupt_control_v3_1_4
vmap axi_gpio_v2_0_17 questa_lib/msim/axi_gpio_v2_0_17
vmap util_vector_logic_v2_0_1 questa_lib/msim/util_vector_logic_v2_0_1
vmap axi_protocol_converter_v2_1_15 questa_lib/msim/axi_protocol_converter_v2_1_15

vlog -work xil_defaultlib -64 -sv -L smartconnect_v1_0 -L axi_protocol_checker_v2_0_1 -L axi_vip_v1_1_1 -L processing_system7_vip_v1_0_3 -L xilinx_vip "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"C:/Xilinx/Vivado/2017.4/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"C:/Xilinx/Vivado/2017.4/data/ip/xpm/xpm_fifo/hdl/xpm_fifo.sv" \
"C:/Xilinx/Vivado/2017.4/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -64 -93 \
"C:/Xilinx/Vivado/2017.4/data/ip/xpm/xpm_VCOMP.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/Histo1/ip/Histo1_ADC500MSPS_Controller_0_0/ipshared/c094/ADC500MHz_Controller_v1_0.vhd" \
"../../../bd/Histo1/ip/Histo1_ADC500MSPS_Controller_0_0/src/design_1_ADC500MHz_Controller_v1_0_0_0/sim/design_1_ADC500MHz_Controller_v1_0_0_0.vhd" \

vlog -work xil_defaultlib -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../bd/Histo1/ip/Histo1_ADC500MSPS_Controller_0_0/src/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0_clk_wiz.v" \
"../../../bd/Histo1/ip/Histo1_ADC500MSPS_Controller_0_0/src/design_1_clk_wiz_0_0/design_1_clk_wiz_0_0.v" \

vcom -work util_ds_buf_v2_01_a -64 -93 \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/e2ff/hdl/vhdl/util_ds_buf.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/Histo1/ip/Histo1_ADC500MSPS_Controller_0_0/src/design_1_util_ds_buf_0_0/sim/design_1_util_ds_buf_0_0.vhd" \
"../../../bd/Histo1/ip/Histo1_ADC500MSPS_Controller_0_0/src/design_1_util_ds_buf_1_0/sim/design_1_util_ds_buf_1_0.vhd" \
"../../../bd/Histo1/ip/Histo1_ADC500MSPS_Controller_0_0/src/design_1_util_ds_buf_2_0/sim/design_1_util_ds_buf_2_0.vhd" \
"../../../bd/Histo1/ip/Histo1_ADC500MSPS_Controller_0_0/src/design_1_util_ds_buf_3_0/sim/design_1_util_ds_buf_3_0.vhd" \

vcom -work lib_cdc_v1_0_2 -64 -93 \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ef1e/hdl/lib_cdc_v1_0_rfs.vhd" \

vcom -work proc_sys_reset_v5_0_12 -64 -93 \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/f86a/hdl/proc_sys_reset_v5_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/Histo1/ip/Histo1_ADC500MSPS_Controller_0_0/src/design_1_proc_sys_reset_0_0/sim/design_1_proc_sys_reset_0_0.vhd" \
"../../../bd/Histo1/ip/Histo1_ADC500MSPS_Controller_0_0/src/design_1_util_ds_buf_0_1/sim/design_1_util_ds_buf_0_1.vhd" \
"../../../bd/Histo1/ipshared/899c/src/design_1_adc.vhd" \
"../../../bd/Histo1/ip/Histo1_ADC500MSPS_Controller_0_0/sim/Histo1_ADC500MSPS_Controller_0_0.vhd" \
"../../../bd/Histo1/ipshared/a1e0/Decimate_N.vhd" \
"../../../bd/Histo1/ipshared/a1e0/Decimate_by2.vhd" \
"../../../bd/Histo1/ipshared/a1e0/ADC_Decimator.vhd" \
"../../../bd/Histo1/ip/Histo1_ADC_Decimator_0_0/sim/Histo1_ADC_Decimator_0_0.vhd" \
"../../../bd/Histo1/ipshared/b715/hdl/reg_block_v1_0_S00_AXI.vhd" \
"../../../bd/Histo1/ipshared/b715/hdl/reg_block_v1_0.vhd" \
"../../../bd/Histo1/ip/Histo1_reg_block_0_0/sim/Histo1_reg_block_0_0.vhd" \
"../../../bd/Histo1/sim/Histo1.vhd" \
"../../../bd/Histo1/ipshared/b9df/src/histo_counter.vhd" \
"../../../bd/Histo1/ip/Histo1_histo_counter_0_0/sim/Histo1_histo_counter_0_0.vhd" \

vlog -work xlslice_v1_0_1 -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/f3db/hdl/xlslice_v1_0_vl_rfs.v" \

vlog -work xil_defaultlib -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../bd/Histo1/ip/Histo1_xlslice_0_0/sim/Histo1_xlslice_0_0.v" \
"../../../bd/Histo1/ip/Histo1_xlslice_0_1/sim/Histo1_xlslice_0_1.v" \

vlog -work xlconcat_v2_1_1 -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/2f66/hdl/xlconcat_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../bd/Histo1/ip/Histo1_xlconcat_0_0/sim/Histo1_xlconcat_0_0.v" \

vlog -work xlconstant_v1_1_3 -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/0750/hdl/xlconstant_v1_1_vl_rfs.v" \

vlog -work xil_defaultlib -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../bd/Histo1/ip/Histo1_xlconstant_0_0/sim/Histo1_xlconstant_0_0.v" \
"../../../bd/Histo1/ip/Histo1_reg_bit_1_0/sim/Histo1_reg_bit_1_0.v" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/Histo1/ip/Histo1_rst_ps7_0_100M_2/sim/Histo1_rst_ps7_0_100M_2.vhd" \

vlog -work axi_infrastructure_v1_1_0 -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl/axi_infrastructure_v1_1_vl_rfs.v" \

vlog -work smartconnect_v1_0 -64 -sv -L smartconnect_v1_0 -L axi_protocol_checker_v2_0_1 -L axi_vip_v1_1_1 -L processing_system7_vip_v1_0_3 -L xilinx_vip "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/sc_util_v1_0_vl_rfs.sv" \

vlog -work axi_protocol_checker_v2_0_1 -64 -sv -L smartconnect_v1_0 -L axi_protocol_checker_v2_0_1 -L axi_vip_v1_1_1 -L processing_system7_vip_v1_0_3 -L xilinx_vip "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/3b24/hdl/axi_protocol_checker_v2_0_vl_rfs.sv" \

vlog -work axi_vip_v1_1_1 -64 -sv -L smartconnect_v1_0 -L axi_protocol_checker_v2_0_1 -L axi_vip_v1_1_1 -L processing_system7_vip_v1_0_3 -L xilinx_vip "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/a16a/hdl/axi_vip_v1_1_vl_rfs.sv" \

vlog -work processing_system7_vip_v1_0_3 -64 -sv -L smartconnect_v1_0 -L axi_protocol_checker_v2_0_1 -L axi_vip_v1_1_1 -L processing_system7_vip_v1_0_3 -L xilinx_vip "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl/processing_system7_vip_v1_0_vl_rfs.sv" \

vlog -work xil_defaultlib -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../bd/Histo1/ip/Histo1_processing_system7_0_1/sim/Histo1_processing_system7_0_1.v" \

vlog -work generic_baseblocks_v2_1_0 -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/b752/hdl/generic_baseblocks_v2_1_vl_rfs.v" \

vlog -work axi_register_slice_v2_1_15 -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/3ed1/hdl/axi_register_slice_v2_1_vl_rfs.v" \

vlog -work fifo_generator_v13_2_1 -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/5c35/simulation/fifo_generator_vlog_beh.v" \

vcom -work fifo_generator_v13_2_1 -64 -93 \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/5c35/hdl/fifo_generator_v13_2_rfs.vhd" \

vlog -work fifo_generator_v13_2_1 -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/5c35/hdl/fifo_generator_v13_2_rfs.v" \

vlog -work axi_data_fifo_v2_1_14 -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/9909/hdl/axi_data_fifo_v2_1_vl_rfs.v" \

vlog -work axi_crossbar_v2_1_16 -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/c631/hdl/axi_crossbar_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../bd/Histo1/ip/Histo1_xbar_1/sim/Histo1_xbar_1.v" \

vlog -work blk_mem_gen_v8_3_6 -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/2751/simulation/blk_mem_gen_v8_3.v" \

vcom -work axi_bram_ctrl_v4_0_13 -64 -93 \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/20fd/hdl/axi_bram_ctrl_v4_0_rfs.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/Histo1/ip/Histo1_axi_bram_ctrl_0_1/sim/Histo1_axi_bram_ctrl_0_1.vhd" \

vlog -work blk_mem_gen_v8_4_1 -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/67d8/simulation/blk_mem_gen_v8_4.v" \

vlog -work xil_defaultlib -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../bd/Histo1/ip/Histo1_blk_mem_gen_0_2/sim/Histo1_blk_mem_gen_0_2.v" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/Histo1/ipshared/13cb/src/Top.vhd" \
"../../../bd/Histo1/ip/Histo1_SBRAM_wen_mgr_0_0/sim/Histo1_SBRAM_wen_mgr_0_0.vhd" \

vlog -work xil_defaultlib -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../bd/Histo1/ip/Histo1_xlconstant_1_0_3/sim/Histo1_xlconstant_1_0.v" \
"../../../../../../histo_w_comblock/histogram_rewriten/histogram_rewriten.srcs/sources_1/bd/Histo1/ip/Histo1_addr_accond_0_2/sim/Histo1_addr_accond_0.v" \
"../../../bd/Histo1/ip/Histo1_fifo_generator_0_1/sim/Histo1_fifo_generator_0_1.v" \

vcom -work axi_lite_ipif_v3_0_4 -64 -93 \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/cced/hdl/axi_lite_ipif_v3_0_vh_rfs.vhd" \

vcom -work lib_pkg_v1_0_2 -64 -93 \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/0513/hdl/lib_pkg_v1_0_rfs.vhd" \

vcom -work lib_fifo_v1_0_10 -64 -93 \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/f10a/hdl/lib_fifo_v1_0_rfs.vhd" \

vcom -work axi_fifo_mm_s_v4_1_12 -64 -93 \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/68a8/hdl/axi_fifo_mm_s_v4_1_rfs.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/Histo1/ip/Histo1_axi_fifo_mm_s_0_1/sim/Histo1_axi_fifo_mm_s_0_1.vhd" \

vcom -work interrupt_control_v3_1_4 -64 -93 \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/8e66/hdl/interrupt_control_v3_1_vh_rfs.vhd" \

vcom -work axi_gpio_v2_0_17 -64 -93 \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/c450/hdl/axi_gpio_v2_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/Histo1/ip/Histo1_axi_gpio_0_0/sim/Histo1_axi_gpio_0_0.vhd" \

vlog -work util_vector_logic_v2_0_1 -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/2137/hdl/util_vector_logic_v2_0_vl_rfs.v" \

vlog -work xil_defaultlib -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../bd/Histo1/ip/Histo1_util_vector_logic_0_1/sim/Histo1_util_vector_logic_0_1.v" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/Histo1/ipshared/e787/sampler.vhd" \
"../../../bd/Histo1/ip/Histo1_fifo_sampler_0_0/sim/Histo1_fifo_sampler_0_0.vhd" \

vlog -work axi_protocol_converter_v2_1_15 -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ff69/hdl/axi_protocol_converter_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib -64 "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/4868" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/ec67/hdl" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/02c8/hdl/verilog" "+incdir+../../../../project_1.srcs/sources_1/bd/Histo1/ipshared/1313/hdl" "+incdir+C:/Xilinx/Vivado/2017.4/data/xilinx_vip/include" \
"../../../bd/Histo1/ip/Histo1_auto_pc_2/sim/Histo1_auto_pc_2.v" \
"../../../bd/Histo1/ip/Histo1_auto_pc_1/sim/Histo1_auto_pc_1.v" \
"../../../bd/Histo1/ip/Histo1_auto_pc_0/sim/Histo1_auto_pc_0.v" \
"../../../bd/Histo1/ip/Histo1_auto_pc_3/sim/Histo1_auto_pc_3.v" \

vlog -work xil_defaultlib \
"glbl.v"

