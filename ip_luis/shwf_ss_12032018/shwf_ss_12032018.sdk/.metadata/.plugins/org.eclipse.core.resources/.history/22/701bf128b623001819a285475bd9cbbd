/******************************************************************************
*
* Copyright (C) 2016 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

#include <stdio.h>
#include <string.h>

#include "lwip/sockets.h"
#include "netif/xadapter.h"
#include "lwipopts.h"
#include "xil_printf.h"
#include "FreeRTOS.h"
#include "task.h"

#include "globaldef.h"

#define THREAD_STACKSIZE 1024

u16_t echo_port = 7;


char *pch;

void print_echo_app_header()
{
    xil_printf("%20s %6d %s\r\n", "echo server",
                        echo_port,
                        "$ telnet <board_ip> 7");

}

/* thread spawned for each connection */
void process_echo_request(void *p)
{
	int sd = (int)p;
	int RECV_BUF_SIZE = 2048;
	int TDPR_DEPTH=65536;
	char recv_buf[RECV_BUF_SIZE];
	int n, nwrote;

	u32 fifo_depth=1025;
	u32 start_count = 1000;
	u32 sample_number = 1025;

	int read_val1[sample_number];
	int counts, dec;

	int fifo_empty;

	int state;
	char sc;

	u32 trans_buf[MEMORY_BINS];


	while (1) {
		/* read a max of RECV_BUF_SIZE bytes from socket */
		if ((n = read(sd, recv_buf, RECV_BUF_SIZE)) < 0) {
			xil_printf("%s: error reading from socket %d, closing socket\r\n", __FUNCTION__, sd);
			break;
		}
		pch= strtok(recv_buf,",");
		/* break if the recved message = "quit" */

		if (strncmp(pch, "gen_histo", 9)==0){
			xil_printf("Cleaning memory \r\n");
			clean_TDPR();
			pch=strtok(NULL, ",");
			counts=atoi(pch);
//			xil_printf("Reset Fifo\n\r");
//			Xil_Out32(read_fifo_reset_addr,0xA5);//Reset FIFO
//			Xil_Out32(read_fifo_reset_addr,0x00);
//			xil_printf("Done Reseting Fifo \n\r");
			if (counts>0){
				xil_printf("Setting count number to %d \r\n",counts);
				Histo_SetCounts(counts);
				xil_printf("Initializing Count \r\n");
				Histo_init_count();
				xil_printf("Generating histogram \r\n");
			}
			else{
				xil_printf("Count must be > 0, Closing socket %d\r\n", sd);
				break;
			}

		/* break if client closed connection */
		}
		else if (strncmp(pch, "init_fifo", 9)==0){
			xil_printf("Initialize Fifo: ");
			Xil_Out32(Histo_Enable_reg, 1); //Histogram reset condition to clear fifo
			Xil_Out32(read_fifo_intr_addr, 		0x0C0001FC); //IER //interrupt enable register
			Xil_Out32(read_fifo_dect_reg_addr,	0x00000002); //RDR //receive data register

			Xil_Out32(fifo_count_start_addr, start_count);
			Xil_Out32(fifo_sample_number_addr, sample_number);

			xil_printf("Done\n\r");
		}
		else if (strncmp(pch, "get_fifo", 8)==0){
			xil_printf("For cicle values \n\r");
			fifo_empty=Xil_In32(read_fifo_empty);
			xil_printf("Fifo is empty: %d\r\n",fifo_empty);
			if(fifo_empty!=1){
				for (int var = 0; var < sample_number-1; ++var) {
						read_val1[var] = Xil_In32(read_fifo_data_addr);
//						printf("read fifo = %x \r\n", read_val1[var]);		// %u for unsigned number print
					}
				xil_printf("Sending FIFO values \n\r");
			}
			if ((nwrote = write(sd, read_val1, 4*(sample_number-1))) < 0) {
				xil_printf("%s: ERROR responding to client echo request. received = %d, written = %d\r\n",
					__FUNCTION__, n, nwrote);
				xil_printf("Closing socket %d\r\n", sd);
				break;
			}
			xil_printf("Done Sending Fifo values \n\r");

		}
		else if (strncmp(pch, "dec_factor", 10)==0){
			pch=strtok(NULL, ",");
			dec=atoi(pch);
			xil_printf("Setting Decimator Factor to: %d ",dec);
			Xil_Out32(ADC_Decim_factor,dec);
			xil_printf("Done\n\r");
		}
		else if (strncmp(pch, "get_histo", 9)==0){
			xil_printf("Getting histogram data \n\r");
			u32 cuenta=Xil_In32(histo_curr_clk_count);
			xil_printf("Current Count %d\r\n", cuenta);
			int status=Histo_is_done();
			printf("Status %x \n\r",status);
			if((state=Histo_rfm(trans_buf))==XST_FAILURE){
				xil_printf("Full histogram not ready for reading \n\r");
				sc=48;

				if ((nwrote = write(sd, &sc, 1)) < 0) {
					xil_printf("%s: ERROR responding to client echo request. received = %d, written = %d\r\n",
						__FUNCTION__, n, nwrote);
					xil_printf("Closing socket %d\r\n", sd);
					break;
				}
				if ((nwrote = write(sd, &cuenta, 4)) < 0) {
					xil_printf("%s: ERROR responding to client echo request. received = %d, written = %d\r\n",
						__FUNCTION__, n, nwrote);
					xil_printf("Closing socket %d\r\n", sd);
					break;
				}
			}
			else{
				sc=49;
				if ((nwrote = write(sd, &sc, 1)) < 0) {
					xil_printf("%s: ERROR responding to client echo request. received = %d, written = %d\r\n",
						__FUNCTION__, n, nwrote);
					xil_printf("Closing socket %d\r\n", sd);
					break;
				}
				xil_printf("Writing histogram data \n\r");
				if ((nwrote = write(sd, trans_buf, MEMORY_BINS)) < 0) {
					xil_printf("%s: ERROR responding to client echo request. received = %d, written = %d\r\n",
						__FUNCTION__, n, nwrote);
					xil_printf("Closing socket %d\r\n", sd);
				break;
				}
			}

		}
		else if (strncmp(pch, "cyc_histo", 9)==0){
//			xil_printf("Entro \n\r");
			Xil_Out32(Histo_Enable_reg, 4); //RST
			Xil_Out32(Histo_Enable_reg, 1); //Idle
			Xil_Out32(Histo_Enable_reg, 3); //count
			xil_printf("Generating histogram... ");
			int status=Histo_is_done();
			xil_printf("Done \n\r");
			xil_printf("Status %d \n\r",status);
			if((state=Histo_rfm(trans_buf))==XST_FAILURE){
				xil_printf("Full histogram not ready for reading \n\r");
			}
			else{
				xil_printf("Writing histogram data \n\r");
				if ((nwrote = write(sd, trans_buf, MEMORY_BINS)) < 0) {
					xil_printf("%s: ERROR responding to client echo request. received = %d, written = %d\r\n",
						__FUNCTION__, n, nwrote);
					xil_printf("Closing socket %d\r\n", sd);
				break;
				}
//				Xil_Out32(Histo_Enable_reg, 5); //Clean histogram and set value
			}

		}

		/* handle request */
//		if ((nwrote = write(sd, recv_buf, n)) < 0) {
//			xil_printf("%s: ERROR responding to client echo request. received = %d, written = %d\r\n",
//					__FUNCTION__, n, nwrote);
//			xil_printf("Closing socket %d\r\n", sd);
//			break;
//		}
	}

	/* close connection */
	close(sd);
	vTaskDelete(NULL);
}

void echo_application_thread()
{
	int sock, new_sd;
	struct sockaddr_in address, remote;
	int size;

	if ((sock = lwip_socket(AF_INET, SOCK_STREAM, 0)) < 0)
		return;

	address.sin_family = AF_INET;
	address.sin_port = htons(echo_port);
	address.sin_addr.s_addr = INADDR_ANY;

	if (lwip_bind(sock, (struct sockaddr *)&address, sizeof (address)) < 0)
		return;

	lwip_listen(sock, 0);

	size = sizeof(remote);

	while (1) {
		if ((new_sd = lwip_accept(sock, (struct sockaddr *)&remote, (socklen_t *)&size)) > 0) {
			sys_thread_new("echos", process_echo_request,
				(void*)new_sd,
				THREAD_STACKSIZE,
				DEFAULT_THREAD_PRIO);
		}
	}
}
