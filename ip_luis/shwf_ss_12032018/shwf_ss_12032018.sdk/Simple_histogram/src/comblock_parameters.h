/*
 * comblock_parameters.h
 *
 *  Created on: Feb 8, 2018
 *      Author: infolab
 */

#ifndef SRC_COMBLOCK_PARAMETERS_H_
#define SRC_COMBLOCK_PARAMETERS_H_

#include "xparameters.h"

#define comblock_reg_block_addr	XPAR_COMBLOCK_REG_BLOCK_0_S00_AXI_BASEADDR
#define	comblock_reg_block_out_addr	comblock_reg_block_addr
#define	comblock_reg_block_in_addr	comblock_reg_block_addr + 4*10


//Defining output register blocks address
#define comblock_reg_block_out_0 comblock_reg_block_out_addr+4*0
#define comblock_reg_block_out_1 comblock_reg_block_out_addr+4*1
#define comblock_reg_block_out_2 comblock_reg_block_out_addr+4*2
#define comblock_reg_block_out_3 comblock_reg_block_out_addr+4*3
#define comblock_reg_block_out_4 comblock_reg_block_out_addr+4*4
#define comblock_reg_block_out_5 comblock_reg_block_out_addr+4*5
#define comblock_reg_block_out_6 comblock_reg_block_out_addr+4*6
#define comblock_reg_block_out_7 comblock_reg_block_out_addr+4*7
#define comblock_reg_block_out_8 comblock_reg_block_out_addr+4*8
#define comblock_reg_block_out_9 comblock_reg_block_out_addr+4*9


//Defining input register blocks address
#define comblock_reg_block_in_0 comblock_reg_block_in_addr+4*0
#define comblock_reg_block_in_1 comblock_reg_block_in_addr+4*1
#define comblock_reg_block_in_2 comblock_reg_block_in_addr+4*2
#define comblock_reg_block_in_3 comblock_reg_block_in_addr+4*3
#define comblock_reg_block_in_4 comblock_reg_block_in_addr+4*4
#define comblock_reg_block_in_5 comblock_reg_block_in_addr+4*5
#define comblock_reg_block_in_6 comblock_reg_block_in_addr+4*6
#define comblock_reg_block_in_7 comblock_reg_block_in_addr+4*7
#define comblock_reg_block_in_8 comblock_reg_block_in_addr+4*8
#define comblock_reg_block_in_9 comblock_reg_block_in_addr+4*9


//Defining TDPR parameters

#define comblock_TDPR_addr	XPAR_COMBLOCK_AXI_BRAM_CTRL_0_S_AXI_BASEADDR
#define comblock_TDPR_highaddr XPAR_COMBLOCK_AXI_BRAM_CTRL_0_S_AXI_HIGHADDR




#endif /* SRC_COMBLOCK_PARAMETERS_H_ */
