--Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
--Date        : Fri Mar  9 15:41:40 2018
--Host        : HP6-MLAB-9 running 64-bit Service Pack 1  (build 7601)
--Command     : generate_target Histo1_wrapper.bd
--Design      : Histo1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Histo1_wrapper is
  port (
    CLK_IN1_D_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_IN1_D_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    DDR_0_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_0_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_0_cas_n : inout STD_LOGIC;
    DDR_0_ck_n : inout STD_LOGIC;
    DDR_0_ck_p : inout STD_LOGIC;
    DDR_0_cke : inout STD_LOGIC;
    DDR_0_cs_n : inout STD_LOGIC;
    DDR_0_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_0_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_0_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_0_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_0_odt : inout STD_LOGIC;
    DDR_0_ras_n : inout STD_LOGIC;
    DDR_0_reset_n : inout STD_LOGIC;
    DDR_0_we_n : inout STD_LOGIC;
    FIXED_IO_0_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_0_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_0_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_0_ps_clk : inout STD_LOGIC;
    FIXED_IO_0_ps_porb : inout STD_LOGIC;
    FIXED_IO_0_ps_srstb : inout STD_LOGIC;
    clk_to_adc_DS_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_to_adc_DS_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    data_from_adc_DS_N : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data_from_adc_DS_P : in STD_LOGIC_VECTOR ( 15 downto 0 );
    from_adc_or_DS_N : in STD_LOGIC_VECTOR ( 0 to 0 );
    from_adc_or_DS_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    histo_done : out STD_LOGIC;
    rst_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    sys_clk : in STD_LOGIC;
    x_from_adc_calrun_fmc : in STD_LOGIC;
    x_to_adc_cal_fmc : out STD_LOGIC;
    x_to_adc_caldly_nscs_fmc : out STD_LOGIC;
    x_to_adc_dclk_rst_fmc : out STD_LOGIC;
    x_to_adc_fsr_ece_fmc : out STD_LOGIC;
    x_to_adc_led_0 : out STD_LOGIC;
    x_to_adc_led_1 : out STD_LOGIC;
    x_to_adc_outedge_ddr_sdata_fmc : out STD_LOGIC;
    x_to_adc_outv_slck_fmc : out STD_LOGIC;
    x_to_adc_pd_fmc : out STD_LOGIC
  );
end Histo1_wrapper;

architecture STRUCTURE of Histo1_wrapper is
  component Histo1 is
  port (
    clk_to_adc_DS_N : out STD_LOGIC_VECTOR ( 0 to 0 );
    clk_to_adc_DS_P : out STD_LOGIC_VECTOR ( 0 to 0 );
    x_to_adc_cal_fmc : out STD_LOGIC;
    x_to_adc_caldly_nscs_fmc : out STD_LOGIC;
    x_to_adc_dclk_rst_fmc : out STD_LOGIC;
    x_to_adc_fsr_ece_fmc : out STD_LOGIC;
    x_to_adc_led_0 : out STD_LOGIC;
    x_to_adc_led_1 : out STD_LOGIC;
    x_to_adc_outedge_ddr_sdata_fmc : out STD_LOGIC;
    x_to_adc_outv_slck_fmc : out STD_LOGIC;
    x_to_adc_pd_fmc : out STD_LOGIC;
    data_from_adc_DS_N : in STD_LOGIC_VECTOR ( 15 downto 0 );
    data_from_adc_DS_P : in STD_LOGIC_VECTOR ( 15 downto 0 );
    CLK_IN1_D_clk_n : in STD_LOGIC_VECTOR ( 0 to 0 );
    CLK_IN1_D_clk_p : in STD_LOGIC_VECTOR ( 0 to 0 );
    sys_clk : in STD_LOGIC;
    from_adc_or_DS_N : in STD_LOGIC_VECTOR ( 0 to 0 );
    from_adc_or_DS_P : in STD_LOGIC_VECTOR ( 0 to 0 );
    x_from_adc_calrun_fmc : in STD_LOGIC;
    histo_done : out STD_LOGIC;
    rst_out : out STD_LOGIC_VECTOR ( 0 to 0 );
    DDR_0_cas_n : inout STD_LOGIC;
    DDR_0_cke : inout STD_LOGIC;
    DDR_0_ck_n : inout STD_LOGIC;
    DDR_0_ck_p : inout STD_LOGIC;
    DDR_0_cs_n : inout STD_LOGIC;
    DDR_0_reset_n : inout STD_LOGIC;
    DDR_0_odt : inout STD_LOGIC;
    DDR_0_ras_n : inout STD_LOGIC;
    DDR_0_we_n : inout STD_LOGIC;
    DDR_0_ba : inout STD_LOGIC_VECTOR ( 2 downto 0 );
    DDR_0_addr : inout STD_LOGIC_VECTOR ( 14 downto 0 );
    DDR_0_dm : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_0_dq : inout STD_LOGIC_VECTOR ( 31 downto 0 );
    DDR_0_dqs_n : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    DDR_0_dqs_p : inout STD_LOGIC_VECTOR ( 3 downto 0 );
    FIXED_IO_0_mio : inout STD_LOGIC_VECTOR ( 53 downto 0 );
    FIXED_IO_0_ddr_vrn : inout STD_LOGIC;
    FIXED_IO_0_ddr_vrp : inout STD_LOGIC;
    FIXED_IO_0_ps_srstb : inout STD_LOGIC;
    FIXED_IO_0_ps_clk : inout STD_LOGIC;
    FIXED_IO_0_ps_porb : inout STD_LOGIC
  );
  end component Histo1;
begin
Histo1_i: component Histo1
     port map (
      CLK_IN1_D_clk_n(0) => CLK_IN1_D_clk_n(0),
      CLK_IN1_D_clk_p(0) => CLK_IN1_D_clk_p(0),
      DDR_0_addr(14 downto 0) => DDR_0_addr(14 downto 0),
      DDR_0_ba(2 downto 0) => DDR_0_ba(2 downto 0),
      DDR_0_cas_n => DDR_0_cas_n,
      DDR_0_ck_n => DDR_0_ck_n,
      DDR_0_ck_p => DDR_0_ck_p,
      DDR_0_cke => DDR_0_cke,
      DDR_0_cs_n => DDR_0_cs_n,
      DDR_0_dm(3 downto 0) => DDR_0_dm(3 downto 0),
      DDR_0_dq(31 downto 0) => DDR_0_dq(31 downto 0),
      DDR_0_dqs_n(3 downto 0) => DDR_0_dqs_n(3 downto 0),
      DDR_0_dqs_p(3 downto 0) => DDR_0_dqs_p(3 downto 0),
      DDR_0_odt => DDR_0_odt,
      DDR_0_ras_n => DDR_0_ras_n,
      DDR_0_reset_n => DDR_0_reset_n,
      DDR_0_we_n => DDR_0_we_n,
      FIXED_IO_0_ddr_vrn => FIXED_IO_0_ddr_vrn,
      FIXED_IO_0_ddr_vrp => FIXED_IO_0_ddr_vrp,
      FIXED_IO_0_mio(53 downto 0) => FIXED_IO_0_mio(53 downto 0),
      FIXED_IO_0_ps_clk => FIXED_IO_0_ps_clk,
      FIXED_IO_0_ps_porb => FIXED_IO_0_ps_porb,
      FIXED_IO_0_ps_srstb => FIXED_IO_0_ps_srstb,
      clk_to_adc_DS_N(0) => clk_to_adc_DS_N(0),
      clk_to_adc_DS_P(0) => clk_to_adc_DS_P(0),
      data_from_adc_DS_N(15 downto 0) => data_from_adc_DS_N(15 downto 0),
      data_from_adc_DS_P(15 downto 0) => data_from_adc_DS_P(15 downto 0),
      from_adc_or_DS_N(0) => from_adc_or_DS_N(0),
      from_adc_or_DS_P(0) => from_adc_or_DS_P(0),
      histo_done => histo_done,
      rst_out(0) => rst_out(0),
      sys_clk => sys_clk,
      x_from_adc_calrun_fmc => x_from_adc_calrun_fmc,
      x_to_adc_cal_fmc => x_to_adc_cal_fmc,
      x_to_adc_caldly_nscs_fmc => x_to_adc_caldly_nscs_fmc,
      x_to_adc_dclk_rst_fmc => x_to_adc_dclk_rst_fmc,
      x_to_adc_fsr_ece_fmc => x_to_adc_fsr_ece_fmc,
      x_to_adc_led_0 => x_to_adc_led_0,
      x_to_adc_led_1 => x_to_adc_led_1,
      x_to_adc_outedge_ddr_sdata_fmc => x_to_adc_outedge_ddr_sdata_fmc,
      x_to_adc_outv_slck_fmc => x_to_adc_outv_slck_fmc,
      x_to_adc_pd_fmc => x_to_adc_pd_fmc
    );
end STRUCTURE;
