// Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
// Date        : Wed Feb  7 13:10:30 2018
// Host        : HP6-MLAB-9 running 64-bit Service Pack 1  (build 7601)
// Command     : write_verilog -force -mode synth_stub
//               c:/Users/infolab/Desktop/my_verilog/histo_w_comblock/project_1/project_1.srcs/sources_1/bd/Histo1/ip/Histo1_ADC_Decimator_0_0/Histo1_ADC_Decimator_0_0_stub.v
// Design      : Histo1_ADC_Decimator_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7z020clg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "ADC_Decimator,Vivado 2017.4" *)
module Histo1_ADC_Decimator_0_0(clk_in, resetn, Din, N, clk_out, Dout, 
  Decim_data_valid, D_sum)
/* synthesis syn_black_box black_box_pad_pin="clk_in,resetn,Din[15:0],N[15:0],clk_out,Dout[15:0],Decim_data_valid,D_sum[15:0]" */;
  input clk_in;
  input resetn;
  input [15:0]Din;
  input [15:0]N;
  output clk_out;
  output [15:0]Dout;
  output Decim_data_valid;
  output [15:0]D_sum;
endmodule
