-- Copyright 1986-2017 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2017.4 (win64) Build 2086221 Fri Dec 15 20:55:39 MST 2017
-- Date        : Wed Feb  7 13:10:30 2018
-- Host        : HP6-MLAB-9 running 64-bit Service Pack 1  (build 7601)
-- Command     : write_vhdl -force -mode synth_stub
--               c:/Users/infolab/Desktop/my_verilog/histo_w_comblock/project_1/project_1.srcs/sources_1/bd/Histo1/ip/Histo1_ADC_Decimator_0_0/Histo1_ADC_Decimator_0_0_stub.vhdl
-- Design      : Histo1_ADC_Decimator_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7z020clg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Histo1_ADC_Decimator_0_0 is
  Port ( 
    clk_in : in STD_LOGIC;
    resetn : in STD_LOGIC;
    Din : in STD_LOGIC_VECTOR ( 15 downto 0 );
    N : in STD_LOGIC_VECTOR ( 15 downto 0 );
    clk_out : out STD_LOGIC;
    Dout : out STD_LOGIC_VECTOR ( 15 downto 0 );
    Decim_data_valid : out STD_LOGIC;
    D_sum : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );

end Histo1_ADC_Decimator_0_0;

architecture stub of Histo1_ADC_Decimator_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk_in,resetn,Din[15:0],N[15:0],clk_out,Dout[15:0],Decim_data_valid,D_sum[15:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "ADC_Decimator,Vivado 2017.4";
begin
end;
