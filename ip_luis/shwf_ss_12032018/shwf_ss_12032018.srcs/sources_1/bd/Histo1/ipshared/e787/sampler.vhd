----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/09/2018 10:21:11 AM
-- Design Name: 
-- Module Name: sampler - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sampler is
    Generic( count_bits : integer:=32;
             fifo_size : integer:= 1024;
             we_bytes : integer :=4    
    );
  Port (
    clk : in STD_LOGIC;
    rst : in STD_LOGIC;
    
    curr_count : in UNSIGNED(count_bits-1 downto 0);
    scount : in UNSIGNED(count_bits -1 downto 0);
    nsamples : in UNSIGNED(count_bits -1 downto 0);
    FIFO_FULL : in STD_LOGIC;
    RAM_WE : in STD_LOGIC_VECTOR(we_bytes-1 downto 0);
    
    FIFO_WE : out STD_LOGIC;
    FIFO_CLR : out STD_LOGIC
  );
end sampler;

architecture Behavioral of sampler is

signal scount_signal : unsigned(count_bits -1 downto 0);
signal max_count :unsigned(count_bits -1 downto 0);

begin

process(clk,rst, curr_count, scount, nsamples, FIFO_FULL)
begin
    if rst='1' then
        FIFO_WE<='0';
        FIFO_CLR<='0';
        scount_signal<=scount;
        max_count<=scount+nsamples;
    elsif rising_edge(clk) then
        FIFO_CLR<='1';
        IF FIFO_FULL='1' then
            FIFO_WE<='0';
        ELSIF ((curr_count>=scount_signal) and (curr_count<max_count)) then
            FIFO_WE<='1' AND RAM_WE(0);
        ELSE
            FIFO_WE<='0';
        end if;
    end if;


end process;


end Behavioral;
