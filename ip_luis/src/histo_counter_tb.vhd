-- Testbench automatically generated online
-- at http://vhdl.lapinoo.net
-- Generation date : 9.2.2018 09:43:20 GMT

library ieee;
use ieee.std_logic_1164.all;

entity tb_histo_counter is
        Generic(count_res : integer :=32);
end tb_histo_counter;

architecture tb of tb_histo_counter is

    component histo_counter
        port (clk  : in std_logic;
              rst  : in std_logic;
              num  : in std_logic_vector (count_res-1 downto 0);
              en   : in std_logic;
              cen  : out std_logic;
              done : out std_logic);
    end component;

    signal clk  : std_logic;
    signal rst  : std_logic;
    signal num  : std_logic_vector (count_res-1 downto 0);
    signal en   : std_logic;
    signal cen  : std_logic;
    signal done : std_logic;

    constant TbPeriod : time := 3.3 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : histo_counter
    port map (clk  => clk,
              rst  => rst,
              num  => num,
              en   => en,
              cen  => cen,
              done => done);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that clk is really your main clock signal
    clk <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        num <=x"000000FF";
        en <= '0';

        -- Reset generation
        -- EDIT: Check that rst is really your reset signal
        rst <= '1';
        wait for 100 ns;
        rst <= '0';
        wait for 100 ns;
        
        en<='1';
        
        wait for 40*TbPeriod;
        
        en<='0';
        num <=x"F00000FF";
        wait for 40*TbPeriod;
        
        en<='1';

        wait for 250*TbPeriod;
        
        num <=x"0000000F";
        rst <= '1';
        wait for 100 ns;
        rst <= '0';
        wait for 100 ns;
--        -- EDIT Add stimuli here
--        wait for 100 * TbPeriod;

--        -- Stop the clock and hence terminate the simulation
--        TbSimEnded <= '1';
        wait;
    end process;

end tb;

