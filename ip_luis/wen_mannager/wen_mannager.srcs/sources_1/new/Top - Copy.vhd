----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/02/2018 02:48:38 AM
-- Design Name: 
-- Module Name: Top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Top is
    Generic( addr_bits : integer :=16;
             en_cond : std_logic:='1');
    Port ( clk : in std_logic;
           en : in std_logic;
    
           curr_addr : in STD_LOGIC_VECTOR (addr_bits-1 downto 0);
           last_addr : in STD_LOGIC_VECTOR (addr_bits-1 downto 0);
           ena : out STD_LOGIC;
           enb : out STD_LOGIC;
           wea : out STD_LOGIC;
           web : out STD_LOGIC);
end Top;

architecture Behavioral of Top is

begin
process(clk, en)
begin
    if en /= en_cond then
        ena <='0';
        enb <='0';
        wea <= '0';
        web <= '0';
    elsif falling_edge(clk) then
        if curr_addr=last_addr then
            ena <='1';
            enb <='0';
            wea <= '1';
            web <= '0';
        else
            ena <='1';
            enb <='1';
            wea <= '0';
            web <= '1';        
        end if;
    end if;
end process;

end Behavioral;
