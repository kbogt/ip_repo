----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 02/02/2018 02:48:38 AM
-- Design Name: 
-- Module Name: Top - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Top is
    Generic( addr_bits : integer :=12;
             latency : integer :=2;
             data_bits : integer :=16;
             en_cond : std_logic:='1');
    Port ( clk : in std_logic;
           rst : in std_logic;
           en : in std_logic;
    
           in_addr : in STD_LOGIC_VECTOR (addr_bits-1 downto 0);
--           last_addr : in STD_LOGIC_VECTOR (addr_bits-1 downto 0);

           adda : out std_logic_vector (addr_bits -1 downto 0);
           addb : out std_logic_vector (addr_bits -1 downto 0);
           
           douta : in std_logic_vector (data_bits-1 downto 0);
           doutb : in std_logic_vector (data_bits-1 downto 0);
           
           dina : out std_logic_vector (data_bits-1 downto 0);
           dinb : out std_logic_vector (data_bits-1 downto 0);
                      
           out_addr : out std_logic_vector (addr_bits -1 downto 0);
           count : out std_logic_vector(data_bits -1 downto 0);
           rdy : out std_logic;
                    
           ena : out STD_LOGIC;
           enb : out STD_LOGIC;
           wea : out STD_LOGIC;
           web : out STD_LOGIC);
end Top;

architecture Behavioral of Top is
signal out_cond : std_logic;
signal rd_count : integer range 0 to latency;

type addr_buffer is array (latency downto 0) of std_logic_vector(addr_bits -1 downto 0);
signal adbuf : addr_buffer;


type states is (not_enable, normal, colision, avoidance);
signal curr_state, next_state : states;

signal douta_signal : std_logic_vector(data_bits -1 downto 0);
begin


process(clk, rst, en)
begin
--    if en /= en_cond then
   if rst='0' then
          curr_state<=not_enable;
          adbuf <= (others=>(others=>'0'));
          rd_count<=0;
          out_cond<='0';
          douta_signal<=(others=>'0');
    elsif rising_edge(clk) then
        douta_signal<=douta;
        if en=not en_cond then 
            rd_count<=0;
            out_cond<='0';
        elsif rd_count<latency then
            rd_count<=rd_count+1;
        else
            out_cond<='1';
        end if;
         
         if en= not en_cond then
             adbuf <= (others=>(others=>'0'));
         else
             adbuf(0)<=in_addr;
         end if;
         
         for i in 1 to latency loop
            adbuf(i)<=adbuf(i-1);          
         end loop;
         curr_state<=next_state;
    end if;
end process;





process(curr_state, out_cond)
begin
case curr_state is 
        when not_enable =>
            ena <='0';
            enb <='0';
            wea <= '0';
            web <= '0';

            rdy<='0';

        when normal =>
            ena <='1';
            enb <='1';
            wea <= '0';
            
                       
            if out_cond='1' then
                rdy<='1';
                web <= '1';
            else
                rdy<='0';
                web <= '0';
            end if;
            
        when colision=> 
            ena <='1';
            enb <='1';
            wea <= '0';
            web <= '1';
          
           
            if out_cond='1' then
                rdy<='1';
                web <= '1';
            else
                rdy<='0';
                web <= '0';
            end if;

        when avoidance=>
            ena <='1';
            enb <='1';
            wea <= '0';
            web <= '0';   

            rdy<='1';


        when others => 
            ena <='0';
            enb <='0';
            wea <= '0';
            web <= '0';
            
            if out_cond='1' then
                rdy<='1';
                web <= '1';
            else
                rdy<='0';
                web <= '0';
            end if;
    end case;

end process;


process(curr_state, adbuf, en)
begin
    case curr_state is 
        when not_enable =>
            count<=std_logic_vector(unsigned(douta_signal));
            dinb<=std_logic_vector(unsigned(douta_signal));
            
            if en=en_cond then
                next_state<=normal;
            else
                next_state<=not_enable;
            end if;
        when normal =>
            count<=std_logic_vector(unsigned(douta_signal)+1);
            dinb<=std_logic_vector(unsigned(douta_signal)+1);
            
            if en = not en_cond then
                next_state<=not_enable;
            elsif adbuf(0)=adbuf(1) then
                next_state<=colision;
            else    
                next_state<=normal;
            end if;        
            
        when colision=> 
            count<=std_logic_vector(unsigned(douta_signal)+1);
            dinb<=std_logic_vector(unsigned(douta_signal)+2);
            
            next_state<=avoidance;
        when avoidance=>
            count<=std_logic_vector(unsigned(douta_signal)+2);
            dinb<=(others=>'0');
            
            if en = not en_cond then
                next_state<=not_enable;
            elsif adbuf(0)=adbuf(1) then
                next_state<=colision;
            else    
                next_state<=normal;
            end if;    
        when others => 
            next_state<=not_enable;

    end case;
end process;



dina<=std_logic_vector(unsigned(douta_signal)+1);

out_addr<=(others=>'0') when curr_state=not_enable else adbuf(latency);

adda<=adbuf(0);
addb<=adbuf(latency);



end Behavioral;
