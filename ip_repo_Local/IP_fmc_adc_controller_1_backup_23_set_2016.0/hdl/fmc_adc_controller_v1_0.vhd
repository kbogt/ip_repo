library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fmc_adc_controller_v1_0 is
	generic (
		-- Users to add parameters here

		-- User parameters ends
		-- Do not modify the parameters beyond this line


		-- Parameters of Axi Slave Bus Interface S00_AXI
		C_S00_AXI_DATA_WIDTH	: integer	:= 32;
		C_S00_AXI_ADDR_WIDTH	: integer	:= 6;

		-- Parameters of Axi Master Bus Interface M00_AXIS
		C_M00_AXIS_TDATA_WIDTH	: integer	:= 32;
		C_M00_AXIS_START_COUNT	: integer	:= 32

	);
	port (
		-- Users to add ports here
        data_from_adc : in std_logic_vector(15 downto 0); --usar GENERIC 

        x_to_adc_cal_fmc         : out std_logic ;   --                               
        x_to_adc_caldly_nscs_fmc : out std_logic ;  --                         
        x_to_adc_fsr_ece_fmc     : out std_logic ;  --                                  
        x_to_adc_outv_slck_fmc   : out std_logic ;  --                                
        x_to_adc_outedge_ddr_sdata_fmc : out std_logic ;  --                        
        x_to_adc_dclk_rst_fmc : out std_logic ;   --                                 
        x_to_adc_pd_fmc : out std_logic ;  -- 
        x_to_adc_led_0    : out std_logic ;
        x_to_adc_led_1    : out std_logic ;                                         
                                                            
        x_from_adc_calrun_fmc: in std_logic;    --                          
        x_from_adc_or: in std_logic ;          -- adc (differential) lvds signal  		
		-- User ports ends
		-- Do not modify the ports beyond this line


		-- Ports of Axi Slave Bus Interface S00_AXI
		s00_axi_aclk	: in std_logic;
		s00_axi_aresetn	: in std_logic;
		s00_axi_awaddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_awprot	: in std_logic_vector(2 downto 0);
		s00_axi_awvalid	: in std_logic;
		s00_axi_awready	: out std_logic;
		s00_axi_wdata	: in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_wstrb	: in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
		s00_axi_wvalid	: in std_logic;
		s00_axi_wready	: out std_logic;
		s00_axi_bresp	: out std_logic_vector(1 downto 0);
		s00_axi_bvalid	: out std_logic;
		s00_axi_bready	: in std_logic;
		s00_axi_araddr	: in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
		s00_axi_arprot	: in std_logic_vector(2 downto 0);
		s00_axi_arvalid	: in std_logic;
		s00_axi_arready	: out std_logic;
		s00_axi_rdata	: out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
		s00_axi_rresp	: out std_logic_vector(1 downto 0);
		s00_axi_rvalid	: out std_logic;
		s00_axi_rready	: in std_logic;

		-- Ports of Axi Master Bus Interface M00_AXIS
		m00_axis_aclk	: in std_logic;
		m00_axis_aresetn	: in std_logic;
		m00_axis_tvalid	: out std_logic;
		m00_axis_tdata	: out std_logic_vector(C_M00_AXIS_TDATA_WIDTH-1 downto 0);
		m00_axis_tstrb	: out std_logic_vector((C_M00_AXIS_TDATA_WIDTH/8)-1 downto 0);
		m00_axis_tlast	: out std_logic;
		m00_axis_tready	: in std_logic

	);
end fmc_adc_controller_v1_0;
--
architecture arch_imp of fmc_adc_controller_v1_0 is

--
component fmc_adc_demultiplexer is 
		generic (d_width: natural:=16);  --demultiplexed data width = 16 bits = 2 x 8-bits data samples   --generic declaration 
    Port ( data_in      : in  STD_LOGIC_VECTOR (d_width-1 downto 0);
           data_out     : out  STD_LOGIC_VECTOR (2*d_width-1 downto 0); -- 32 bits = 4 x 8-bits data samples (de-de-multiplexed) 
           clk_from_adc : in  STD_LOGIC;
           fifo_clock_enable : out  STD_LOGIC;
           reset : in  STD_LOGIC);
end component fmc_adc_demultiplexer;

	-- component declaration
	component fmc_adc_controller_v1_0_S00_AXI is
		generic (
		C_S_AXI_DATA_WIDTH	: integer	:= 32;
		C_S_AXI_ADDR_WIDTH	: integer	:= 6
		);
		port (		
        to_adc_cal_fmc         : out std_logic ;   --                               
        to_adc_caldly_nscs_fmc : out std_logic ;  --                         
        to_adc_fsr_ece_fmc     : out std_logic ;  --                                  
        to_adc_outv_slck_fmc   : out std_logic ;  --                                
        to_adc_outedge_ddr_sdata_fmc : out std_logic ;  --                        
        to_adc_dclk_rst_fmc : out std_logic ;   --                                 
        to_adc_pd_fmc : out std_logic ;  --  
        to_adc_led_0    : out std_logic ;
        to_adc_led_1    : out std_logic ;                                       
                                                            
        from_adc_calrun_fmc: in std_logic;    --                          
        from_adc_or: in std_logic ;          -- adc (differential) lvds signal 
    		
		S_AXI_ACLK	: in std_logic;
		S_AXI_ARESETN	: in std_logic;
		S_AXI_AWADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_AWPROT	: in std_logic_vector(2 downto 0);
		S_AXI_AWVALID	: in std_logic;
		S_AXI_AWREADY	: out std_logic;
		S_AXI_WDATA	: in std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_WSTRB	: in std_logic_vector((C_S_AXI_DATA_WIDTH/8)-1 downto 0);
		S_AXI_WVALID	: in std_logic;
		S_AXI_WREADY	: out std_logic;
		S_AXI_BRESP	: out std_logic_vector(1 downto 0);
		S_AXI_BVALID	: out std_logic;
		S_AXI_BREADY	: in std_logic;
		S_AXI_ARADDR	: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		S_AXI_ARPROT	: in std_logic_vector(2 downto 0);
		S_AXI_ARVALID	: in std_logic;
		S_AXI_ARREADY	: out std_logic;
		S_AXI_RDATA	: out std_logic_vector(C_S_AXI_DATA_WIDTH-1 downto 0);
		S_AXI_RRESP	: out std_logic_vector(1 downto 0);
		S_AXI_RVALID	: out std_logic;
		S_AXI_RREADY	: in std_logic
		);
	end component fmc_adc_controller_v1_0_S00_AXI;

	component fmc_adc_controller_v1_0_M00_AXIS is
		generic (
		C_M_AXIS_TDATA_WIDTH	: integer	:= 32;
		C_M_START_COUNT	: integer	:= 32
		);
		port (
		input_M_AXIS_TVALID    : in std_logic; --this signal goes directly to output M_AXIS_TVALID
        input_M_AXIS_TDATA    : in std_logic_vector(C_M_AXIS_TDATA_WIDTH-1 downto 0);   --this signal data goes directly to output M_AXIS_TDATA 
		M_AXIS_ACLK	: in std_logic;
		M_AXIS_ARESETN	: in std_logic;
		M_AXIS_TVALID	: out std_logic;
		M_AXIS_TDATA	: out std_logic_vector(C_M_AXIS_TDATA_WIDTH-1 downto 0);
		M_AXIS_TSTRB	: out std_logic_vector((C_M_AXIS_TDATA_WIDTH/8)-1 downto 0);
		M_AXIS_TLAST	: out std_logic;
		M_AXIS_TREADY	: in std_logic
		);
	end component fmc_adc_controller_v1_0_M00_AXIS;

-- Signal declarations 

signal input_M_AXIS_TVALID_sig : std_logic;
signal input_M_AXIS_TDATA_sig  : std_logic_vector(31 downto 0);
begin



-- Instantiation of Axi Bus Interface M00_AXIS
demultiplexer : fmc_adc_demultiplexer
    port map (
	  data_in       =>  data_from_adc,
      data_out      =>  input_M_AXIS_TDATA_sig, 
      clk_from_adc  =>  m00_axis_aclk,
      fifo_clock_enable => input_M_AXIS_TVALID_sig,	
      reset         =>  	m00_axis_aresetn);


-- Instantiation of Axi Bus Interface S00_AXI
fmc_adc_controller_v1_0_S00_AXI_inst : fmc_adc_controller_v1_0_S00_AXI
	generic map (
		C_S_AXI_DATA_WIDTH	=> C_S00_AXI_DATA_WIDTH,
		C_S_AXI_ADDR_WIDTH	=> C_S00_AXI_ADDR_WIDTH
	)
	port map (
	
	to_adc_cal_fmc          => x_to_adc_cal_fmc,
    to_adc_caldly_nscs_fmc  => x_to_adc_caldly_nscs_fmc,
    to_adc_fsr_ece_fmc      => x_to_adc_fsr_ece_fmc,
    to_adc_outv_slck_fmc    => x_to_adc_outv_slck_fmc,
    to_adc_outedge_ddr_sdata_fmc  => x_to_adc_outedge_ddr_sdata_fmc,
    to_adc_dclk_rst_fmc     => x_to_adc_dclk_rst_fmc,
    to_adc_pd_fmc           => x_to_adc_pd_fmc,
	to_adc_led_0            => x_to_adc_led_0,
    to_adc_led_1            => x_to_adc_led_1,   
	
    from_adc_calrun_fmc  =>  x_from_adc_calrun_fmc,  --                          
    from_adc_or          =>   x_from_adc_or,   -- adc (differential) lvds signal  

		S_AXI_ACLK	=> s00_axi_aclk,
		S_AXI_ARESETN	=> s00_axi_aresetn,
		S_AXI_AWADDR	=> s00_axi_awaddr,
		S_AXI_AWPROT	=> s00_axi_awprot,
		S_AXI_AWVALID	=> s00_axi_awvalid,
		S_AXI_AWREADY	=> s00_axi_awready,
		S_AXI_WDATA	=> s00_axi_wdata,
		S_AXI_WSTRB	=> s00_axi_wstrb,
		S_AXI_WVALID	=> s00_axi_wvalid,
		S_AXI_WREADY	=> s00_axi_wready,
		S_AXI_BRESP	=> s00_axi_bresp,
		S_AXI_BVALID	=> s00_axi_bvalid,
		S_AXI_BREADY	=> s00_axi_bready,
		S_AXI_ARADDR	=> s00_axi_araddr,
		S_AXI_ARPROT	=> s00_axi_arprot,
		S_AXI_ARVALID	=> s00_axi_arvalid,
		S_AXI_ARREADY	=> s00_axi_arready,
		S_AXI_RDATA	=> s00_axi_rdata,
		S_AXI_RRESP	=> s00_axi_rresp,
		S_AXI_RVALID	=> s00_axi_rvalid,
		S_AXI_RREADY	=> s00_axi_rready
	);

-- Instantiation of Axi Bus Interface M00_AXIS
fmc_adc_controller_v1_0_M00_AXIS_inst : fmc_adc_controller_v1_0_M00_AXIS
	generic map (
		C_M_AXIS_TDATA_WIDTH	=> C_M00_AXIS_TDATA_WIDTH,
		C_M_START_COUNT	=> C_M00_AXIS_START_COUNT
	)
	port map (
	    input_M_AXIS_TVALID => input_M_AXIS_TVALID_sig, 
        input_M_AXIS_TDATA => input_M_AXIS_TDATA_sig,   
		M_AXIS_ACLK	=> m00_axis_aclk,
		M_AXIS_ARESETN	=> m00_axis_aresetn,
		M_AXIS_TVALID	=> m00_axis_tvalid,
		M_AXIS_TDATA	=> m00_axis_tdata,
		M_AXIS_TSTRB	=> m00_axis_tstrb,
		M_AXIS_TLAST	=> m00_axis_tlast,
		M_AXIS_TREADY	=> m00_axis_tready
	);


	-- Add user logic here

	-- User logic ends

end arch_imp;
