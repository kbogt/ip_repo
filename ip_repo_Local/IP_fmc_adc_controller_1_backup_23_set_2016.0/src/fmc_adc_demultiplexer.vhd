------------------------------------------------------------------
--      ICTP-INFN MLAB
------------------------------------------------------------------
-- Designer:            Andres Cicuttin, ICTP-INFN
-- Create Date:         Aug 31, 2016
-- Design Name:         FMC-ADC-500MHZ
-- Module Name:         fmc_top.vhd
-- Project Name:        ThickGEMS HVPS Controler
-- Target Devices:      Zynq 
-- FMC Platfporm:       ZedBoard
--
-- Tool versions:       Vivado 
 
-- Description:         VHDL Top Descrpition of FMC-ADC-500MHZ.
-- Dependencies: 
--
-- Revision:            Aug 31, 2016: 0.0  Initial version
-- Revision 0.01 - File Created
-- Additional Comments: 

--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use IEEE.std_logic_signed.all;

Library UNISIM;
use UNISIM.vcomponents.all;

Library UNIMACRO;
use UNIMACRO.vcomponents.all;

entity fmc_adc_demultiplexer is 
		generic (d_width: natural:=16);  --demultiplexed data width = 16 bits = 2 x 8-bits data samples   --generic declaration 
    Port ( data_in      : in  STD_LOGIC_VECTOR (d_width-1 downto 0);
           data_out     : out  STD_LOGIC_VECTOR (2*d_width-1 downto 0); -- 32 bits = 4 x 8-bits data samples (de-de-multiplexed) 
           clk_from_adc : in  STD_LOGIC;
           fifo_clock_enable : out  STD_LOGIC;
           reset : in  STD_LOGIC);
end fmc_adc_demultiplexer;


architecture Behavioral of fmc_adc_demultiplexer is

-- Signals declaration
      
    --Signals used for the second demultiplexing process
    signal data_int_16b_1 : STD_LOGIC_VECTOR (d_width-1 downto 0); -- 16-bits temporary register
    signal data_int_16b_2 : STD_LOGIC_VECTOR (d_width-1 downto 0); -- 16-bits temporary register
    signal data_int_32b   : STD_LOGIC_VECTOR (2*d_width-1 downto 0); -- 32-bits temporary register = 4 x 8-bits data samples (de-de-multiplexed) 
    signal data_to_fifo_clock_enable: STD_LOGIC; -- Active High
    	

begin

Second_Demultiplexing_Process: process (clk_from_adc,reset)  --the first demultiplexing is done by the ADC 
   begin
    if (clk_from_adc'event and clk_from_adc = '1') then
        if (reset = '0') then     -- Synchronous reset
		
		data_int_16b_1 <= (others => '0');       
		data_int_16b_2 <= (others => '0');       
		data_int_32b   <= (others => '0');       
        data_to_fifo_clock_enable <= '1';    
			  
         else
            data_int_16b_1 <= data_in;             -- Shifting data
			data_int_16b_2 <= data_int_16b_1;      -- Shifting data

			if data_to_fifo_clock_enable = '1' then
			     data_int_32b <= data_int_16b_2 & data_int_16b_1 ; -- demultiplexing again   -- CHECK the order!!! --better the same like the ADC
			end if;
				
		data_to_fifo_clock_enable <= not data_to_fifo_clock_enable;	
        
        end if;        
    end if;
   end process;
   data_out <= data_int_32b;
   fifo_clock_enable <= data_to_fifo_clock_enable;
end Behavioral;