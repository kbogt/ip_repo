----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/07/2017 05:57:18 PM
-- Design Name: 
-- Module Name: ADC500MHz_Controller_v1_0 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ADC500MHz_Controller_v1_0 is
    Generic(din_width : integer := 16);             --demultiplexed data width = 16 bits = 2 x 8-bits data samples   --generic declaration 
    Port ( ADC_clk          :  in STD_LOGIC;                        -- clock from adc
           ADC_reset        :  in STD_LOGIC;                        -- common reset
           ADC_Din          :  in STD_LOGIC_VECTOR (din_width - 1 downto 0);    -- data in from adc
           
           ADC_Dout          :  out STD_LOGIC_VECTOR (2*din_width - 1 downto 0);    -- demultiplexed adc data out (4 samples), 32 bits = 4 x 8-bits data samples (de-de-multiplexed) 
           data_valid       :  out STD_LOGIC;                                       -- valid signal for demultiplexed adc data
           
           Ctrl_reg0_in     : in STD_LOGIC_VECTOR (8 downto 0);        -- controls the ADC configurations
           Ctrl_reg1_out    : out STD_LOGIC_VECTOR (1 downto 0);       -- feedback signals from the adc attached to this register
           
           to_adc_cal_fmc   :  out STD_LOGIC;
           to_adc_caldly_nscs_fmc   :  out STD_LOGIC;
           to_adc_fsr_ece_fmc       :  out STD_LOGIC;
           to_adc_outv_slck_fmc     :  out STD_LOGIC;
           to_adc_outedge_ddr_sdata_fmc :  out STD_LOGIC;
           to_adc_dclk_rst_fmc      :  out STD_LOGIC;
           to_adc_pd_fmc    :  out STD_LOGIC;
           
           to_adc_led_0     :  out STD_LOGIC;
           to_adc_led_1     :  out STD_LOGIC;
    
           from_adc_calrun_fmc  :  in STD_LOGIC;
           from_adc_or          :  in STD_LOGIC      -- adc (differential) lvds signal
        );
end ADC500MHz_Controller_v1_0;

architecture Behavioral of ADC500MHz_Controller_v1_0 is

    -- Signals declaration
      
    --Signals used for the second demultiplexing process
    signal data_int_16b_1 : STD_LOGIC_VECTOR (din_width-1 downto 0); -- 16-bits temporary register
    signal data_int_16b_2 : STD_LOGIC_VECTOR (din_width-1 downto 0); -- 16-bits temporary register
    signal data_int_32b   : STD_LOGIC_VECTOR (2*din_width-1 downto 0); -- 32-bits temporary register = 4 x 8-bits data samples (de-de-multiplexed) 
    signal data_to_fifo_clock_enable: STD_LOGIC; -- Active High
    
    signal temp_Ctrl_reg1 : STD_LOGIC_VECTOR (1 downto 0);
    
begin

-- Register assignments
    to_adc_cal_fmc               <= Ctrl_reg0_in(0)  ;   --   
    to_adc_caldly_nscs_fmc       <= Ctrl_reg0_in(1);  -- 
    to_adc_fsr_ece_fmc           <= Ctrl_reg0_in(2);
    to_adc_outv_slck_fmc         <= Ctrl_reg0_in(3);
    to_adc_outedge_ddr_sdata_fmc <= Ctrl_reg0_in(4) ;
    to_adc_dclk_rst_fmc          <= Ctrl_reg0_in(5) ;
    to_adc_pd_fmc                <= Ctrl_reg0_in(6);
    
    to_adc_led_0                 <= Ctrl_reg0_in(7);
    to_adc_led_1                 <= Ctrl_reg0_in(8);
    
    temp_Ctrl_reg1(0)            <= from_adc_calrun_fmc;
    temp_Ctrl_reg1(1)            <= from_adc_or;      -- adc (differential) lvds signal
    
    Ctrl_reg1_out <= temp_Ctrl_reg1(1) & temp_Ctrl_reg1(0);
    
    
Second_Demultiplexing_Process: process (ADC_clk,ADC_reset, ADC_din)  --the first demultiplexing is done by the ADC 
       begin
        if (rising_edge(ADC_clk)) then
            if (ADC_reset = '0') then     -- Synchronous reset
            
            data_int_16b_1 <= (others => '0');       
            data_int_16b_2 <= (others => '0');       
            data_int_32b   <= (others => '0');       
            data_to_fifo_clock_enable <= '1';    
                  
             else
                data_int_16b_1 <= ADC_din;             -- Shifting data
                data_int_16b_2 <= data_int_16b_1;      -- Shifting data
    
                if data_to_fifo_clock_enable = '1' then
                     data_int_32b <= data_int_16b_2 & data_int_16b_1 ; -- demultiplexing again   -- CHECK the order!!! --better the same like the ADC
                end if;
                    
            data_to_fifo_clock_enable <= not data_to_fifo_clock_enable;    
            
            end if;        
        end if;
       end process;
       ADC_Dout <= data_int_32b;
       data_valid <= data_to_fifo_clock_enable;
    

end Behavioral;
