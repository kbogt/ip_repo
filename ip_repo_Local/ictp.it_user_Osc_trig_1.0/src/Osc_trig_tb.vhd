library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use IEEE.STD_LOGIC_unsigned.ALL;

entity Osc_trig_tb is
end Osc_trig_tb;

architecture Behavioral of Osc_trig_tb is

-- component declaration fro the unit under test(UUT)
	component Osc_trig
		Port ( clk : in STD_LOGIC;
			   resetn : in STD_LOGIC;
			   ack_arm : in STD_LOGIC;
			   Din : in STD_LOGIC_VECTOR (15 downto 0);
			   din_valid : in STD_LOGIC;
			   osc_fifo_we : out STD_LOGIC;
			   osc_fifo_re : out STD_LOGIC;
			   com_fifo_we : out STD_LOGIC;
			   trig_fpga : out STD_LOGIC;
			   Dout : out STD_LOGIC_VECTOR (15 downto 0);
			   trig_edge : in STD_LOGIC;
			   trig_auto : in STD_LOGIC;            -- to dump about 10 traces per second without trigger
			   trig_level : in STD_LOGIC_VECTOR (15 downto 0);
			   trig_delay : in unsigned(15 downto 0)
			   );
	end component;

--inputs
	signal 	clk 		: STD_LOGIC := '0';
	signal 	resetn 		: STD_LOGIC := '0';
	signal 	ack_arm 	: STD_LOGIC	:= '0';
	signal 	Din 		: STD_LOGIC_VECTOR (15 downto 0) := (others => '0');
	signal	din_valid 	: STD_LOGIC := '1';
	signal  trig_edge 	: STD_LOGIC := '1';
	signal  trig_auto 	: STD_LOGIC := '0';          
	signal  trig_level 	: STD_LOGIC_VECTOR (15 downto 0) := x"000F";
	signal  trig_delay 	: unsigned (15 downto 0) := (others => '0');

--outputs
	signal  osc_fifo_we : STD_LOGIC;
	signal  osc_fifo_re : STD_LOGIC;
	signal  com_fifo_we : STD_LOGIC;
	signal  trig_fpga 	: STD_LOGIC;
	signal  Dout 		: STD_LOGIC_VECTOR (15 downto 0);
	
-- clock period definition
	constant	clk_period : time := 1ns;

begin

-- instantiate the unit under test(UUT)
	uut: Osc_trig
		Port map( clk 		=> clk,
			   resetn 		=> resetn,
			   ack_arm 		=> ack_arm,
			   Din 			=> Din,
			   din_valid 	=> din_valid,
			   osc_fifo_we 	=> osc_fifo_we,
			   osc_fifo_re 	=> osc_fifo_re,
			   com_fifo_we 	=> com_fifo_we,
			   trig_fpga 	=> trig_fpga,
			   Dout 		=> Dout,
			   trig_edge 	=> trig_edge,
			   trig_auto 	=> trig_auto,           -- to dump about 10 traces per second without trigger
			   trig_level 	=> trig_level,
			   trig_delay 	=> trig_delay
			   );
	
-- clock process definition
	clk_process: process
	begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
	end process;
	
-- data generator
	Data_process: process
    begin
        Din <= Din + 1;
        wait for clk_period;
    end process;

-- stimulus process
	uut_process: process
	begin
		wait for 100ns;
		resetn <= '1';
		
		-- insert stimulus here
		wait;
	end process;

end;