----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/21/2017 06:28:50 PM
-- Design Name: 
-- Module Name: Operation_mode_decoder - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Operation_mode_decoder is
    Port ( op_mode_in      : in STD_LOGIC_VECTOR (2 downto 0);
           FIFO_wr_en_in   : in STD_LOGIC;
           sirio_rst_restor  : in STD_LOGIC;
           FIR_data     : in STD_LOGIC_VECTOR (15 downto 0);
           ADC_data_in     : in STD_LOGIC_VECTOR (11 downto 0);
           FIR_Deriv_data          : in STD_LOGIC_VECTOR (15 downto 0);
           FIFO_wr_en_valid           : out STD_LOGIC;
           FIFO_DATA_IN             : out STD_LOGIC_VECTOR (15 downto 0);
           hist_dout    : in STD_LOGIC_VECTOR (15 downto 0);
           hist_tvalid  : in STD_LOGIC
           );
end Operation_mode_decoder;

architecture Behavioral of Operation_mode_decoder is
    signal op_mode_temp : STD_LOGIC_VECTOR (2 downto 0);
    signal FIFO_wr_en_mux_temp : STD_LOGIC;
    signal FIFO_DATA_IN_mux_temp : STD_LOGIC_VECTOR (15 downto 0);
begin
-- Definition of Operation modes
op_mode_temp <= op_mode_in;
FIFO_wr_en_valid <= FIFO_wr_en_mux_temp;
FIFO_DATA_IN <= FIFO_DATA_IN_mux_temp;

OPERATION_MODE_DECODING: process (op_mode_temp, sirio_rst_restor, FIFO_wr_en_in, ADC_data_in, FIR_data, FIR_Deriv_data)
   begin
      case (op_mode_temp) is
         when "000" =>          	-- Normal: Amplitudes Acquisition 
                FIFO_wr_en_mux_temp <= FIFO_wr_en_in AND NOT sirio_rst_restor;
			    FIFO_DATA_IN_mux_temp <= FIR_data; 

         when "001" =>				-- ADC debbuging: Continuous ADC Data Acquisition 
                FIFO_wr_en_mux_temp <= '1';
 				FIFO_DATA_IN_mux_temp <= "0000"& ADC_data_in;
					
         when "010" =>				-- FIR debbuging: Continuous output FIR Acquisition 
                FIFO_wr_en_mux_temp <= '1';
				FIFO_DATA_IN_mux_temp <= FIR_data;
			
         when "100" =>
                FIFO_wr_en_mux_temp <= '1';
				FIFO_DATA_IN_mux_temp <= FIR_Deriv_data; 
				
	     when "111" =>
	            FIFO_wr_en_mux_temp <= hist_tvalid;
	            FIFO_DATA_IN_mux_temp <= hist_dout;
					
         when others =>
               FIFO_wr_en_mux_temp <= FIFO_wr_en_in AND NOT sirio_rst_restor;
               FIFO_DATA_IN_mux_temp <= FIR_data;					
      end case;      
   end process;

end Behavioral;
