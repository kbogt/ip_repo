----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06/21/2017 06:28:50 PM
-- Design Name: 
-- Module Name: Photon_counters - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.std_logic_signed.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Photon_counters is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           photon_arrival : in STD_LOGIC;
           long_rejection_flag : in STD_LOGIC;
           EndOfRun : in STD_LOGIC;
           reg_total_photon_counter : out  STD_LOGIC_VECTOR (31 downto 0); --total detected photon in run
           reg_dead_time_main_FIR: out  STD_LOGIC_VECTOR (31 downto 0);
           reg_sirio_resets_counter: out  STD_LOGIC_VECTOR (31 downto 0)
           );
end Photon_counters;

architecture Behavioral of Photon_counters is

    signal total_photon_counter : STD_LOGIC_VECTOR (31 downto 0);
    signal dead_time_main_FIR   : STD_LOGIC_VECTOR (31 downto 0);
    signal sirio_resets_counter : STD_LOGIC_VECTOR (31 downto 0);
    
begin
--Updating Counters
--Total detected photons
SYNC_total_photon_counter : process (clk, reset, photon_arrival)
   begin
      if (clk'event and clk = '1') then
         if (reset = '0') then
          total_photon_counter <= (others => '0');
         elsif (photon_arrival = '1') then
          total_photon_counter <= total_photon_counter + 1;
         end if; 
		end if;
end process;

--Total dead time due to main FIR output (in system clock cycles) (The same as the one calculated by the logic_rejection_block ("dead_time_counter"))
SYNC_dead_time_main_FIR : process (clk,reset)
   begin
      if (clk'event and clk = '1') then
         if (reset = '0') then
          dead_time_main_FIR <=(others => '0');
         elsif (long_rejection_flag = '1') then
          dead_time_main_FIR <= dead_time_main_FIR + 1;
         end if; 
		end if;
end process;	


-- Saving value of counters (before reset!) (Assuming FIFO doesn't become FULL)
SYNC_Saving_Counters : process (clk,EndOfRun)
   begin
      if (clk'event and clk = '1') then
         if (EndOfRun ='1' ) then   -- EndOfRun must be put to '1' then to '0' (to save counters), and immediately after send a reset to the Macroblock
          reg_total_photon_counter <= total_photon_counter ; --Acquired photons must be read from FIFO
			 reg_dead_time_main_FIR   <= dead_time_main_FIR ;
			 reg_sirio_resets_counter <= sirio_resets_counter ; 
         end if; 
		end if;
end process;

end Behavioral;
