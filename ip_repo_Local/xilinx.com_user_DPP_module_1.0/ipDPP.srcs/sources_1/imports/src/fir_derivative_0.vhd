----------------------------------------------------------------------------------
-- Company: MLAB (ICTP-INFN)
-- Engineers: A. Cicuttin, M.L. Crespo
-- 
-- Create Date:    10:38:36 08/27/2015 
-- Design Name: 
-- Module Name:    fir_derivative_0 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
-- 		This module takes the synchronous input data stream  "data_in" and generates 
--      	its "average derivative" at the same rate by mean of a six taps FIR with 
--       hardcoded coefficients (-3,-5,-9,9,5,3). 
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--			A  more efficient implementation
--			could be implemented with power of two coefficients (e.g. -2,-4,-8,-16,16,8,4,2) or similar
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
use IEEE.std_logic_signed.all;

entity fir_derivative_0 is 

		generic (d_width: natural:=12;  --data width   			--generic declaration 
		         c_width: natural:=16); --coefficient width  
		
    Port ( data_in : in  STD_LOGIC_VECTOR (d_width-1 downto 0);
           data_out : out  STD_LOGIC_VECTOR (15 downto 0); -- one bit is gained with derivative
           clk : in  STD_LOGIC;
           reset : in  STD_LOGIC;
           cf_1: in std_logic_vector(4 downto 0);
           cf_2: in std_logic_vector(4 downto 0);
           cf_3: in std_logic_vector(4 downto 0));
 
end fir_derivative_0;


architecture Behavioral of fir_derivative_0 is

    signal din: std_logic_vector(d_width + 5 downto 0);
	 signal doutint:std_logic_vector(d_width + 5 + 5 downto 0); --5 due to mult by 5 bits. 5 due to five additons

	 signal din_1, din_2, din_3 , din_4 , din_5 , din_6 : std_logic_vector(d_width + 5 downto 0);   

	 signal d1,  d2, d3, d4, d5, d6 : std_logic_vector((d_width +5 + 5) downto 0);   

--    constant cf_1: std_logic_vector(4 downto 0) := "00011";  --(3) -- Coefficients for fast FIR derivative
--	 constant cf_2: std_logic_vector(4 downto 0) := "00101";  --(5)
--	 constant cf_3: std_logic_vector(4 downto 0) := "01001";  --(9)


begin

din <= ("000000" & data_in);
data_out <= doutint(d_width + 5 + 5 downto 7); --Only used the Least Significnat Segment


FIR_Derivative_Process: process (clk,reset)
   begin
      if (clk'event and clk= '1') then
         if (reset = '0') then  
 
				din_1 <= (others => '0');       -- Shifting in data
				din_2 <= (others => '0');
				din_3 <= (others => '0');
				din_4 <= (others => '0');
				din_5 <= (others => '0');
				din_6 <= (others => '0');
			  
         else
            
				din_1 <= din;        -- Shifting data
				din_2 <= din_1;
				din_3 <= din_2;
				din_4 <= din_3;
				din_5 <= din_4;
				din_6 <= din_5;
				
            d1 <= cf_1 * din_1;  -- Multiplication coef * data
            d2 <= cf_2 * din_2;
				d3 <= cf_3 * din_3;
				d4 <= cf_3 * din_4;
				d5 <= cf_2 * din_5;
				d6 <= cf_1 * din_6;
				
				doutint <= (d1 + d2 + d3) - ( d4 + d5 + d6) ; -- Derivative
			   end if;        
      end if;
   end process;


end Behavioral;

